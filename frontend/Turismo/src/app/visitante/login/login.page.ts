import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ReadusService } from 'src/service/visitante/readus/readus.service';
import { LoadingController, AlertController, NavController } from '@ionic/angular';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { Storage } from '@ionic/storage';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { User } from 'src/models/user/user';
import { Languaje } from 'src/models/languaje/languaje';
import { EspanolService } from 'src/service/comun/lenguaje/espanol/espanol.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  labels=new Languaje();
  idioma="espanol";
  login=new User();
  passtype="password";
  passicon="eye";

  constructor(private router:Router,private readUS:ReadusService,
    public presenters:PresentersService,private storage: Storage,
    public parameters:ParametersService,public navController:NavController,public espano:EspanolService) { 
  }
  ngOnInit() {
    this.parameters.setIdioma("espanol");
    this.idioma="espanol";
    this.labels=this.espano.espanol();
    this.storage.get('TokenRef').then((tokenref) => {
      this.storage.get('TokenVer').then((tokenver) => {
        this.storage.get('Usuario').then((user) => {
          console.log("Ref",tokenref);
          console.log("Ver",tokenver);
          console.log("Us",user);
          if(user=="User"){
            this.storage.get('Rol').then((rol) => {   
              console.log("Rol",rol);       
            });
          }          
        });
      });
  });
  }
  goReceiver(){
    this.navController.navigateRoot(['/tabs']);
  }
  SignIn(){
    let info:any;
    this.presenters.presentLoading(this.labels.espere);
    this.readUS.SigInUs(this.login).subscribe(
      data => {
        info= Object.assign(data);
        console.log('exito');
        this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentAlert(this.labels.noExisteUser,this.labels.passNoMatchMsg);
        }
        else{
          console.log('info', info); 
          this.saveToken(info["Token-Refresh"],info["Token-Verificacion"],info["roles"],info['idioma']);
          this.navController.navigateRoot(['/tabsus']);
        }
        }, (error: any)=> {
          this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        }
    );
  } 
  saveToken(tokenref:string,tokenver:string,roles:number[],idioma:string){
    this.parameters.setIdioma(idioma);
    if(idioma=="espanol"){
      this.parameters.setLabels(this.labels);
    }
    else{
      this.labels=this.espano.ingles();
      this.parameters.setLabels(this.labels);
    }
    this.parameters.setToken(tokenver);
    this.parameters.setTokenref(tokenref);
    if(roles!=null){
      if(roles.length>1){
        this.storage.set('idioma', idioma);
        this.storage.set('TokenRef', tokenref);
        this.storage.set('TokenVer', tokenver);
        this.storage.set('Rol', "Admin");
        this.parameters.setRol("Admin");
      }
      else{
        this.storage.set('idioma', idioma);
        this.storage.set('TokenRef', tokenref);
        this.storage.set('TokenVer', tokenver);
        this.storage.set('Rol', "User");
        this.parameters.setRol("User");
      }
    }

  }
  seepass(){    
    if(this.passtype=='password')
    {
      this.passtype='text';
      this.passicon='eye-off';
    }
    else{
      this.passtype='password';
      this.passicon='eye';
    }
  }
  skip(){
    this.parameters.setLabels(this.labels);
    this.parameters.setRol("Off");
    this.navController.navigateRoot(['/tabsus']);
  }
  onChange($event){
    console.log("CAMBIO");
    console.log($event.target.value);
    if($event.target.value=='ingles'){
      this.labels=this.espano.ingles();
      this.idioma=$event.target.value;
      this.parameters.setIdioma($event.target.value);
    this.parameters.setLabels(this.labels);
    }
    else{
      this.labels=this.espano.espanol();
      this.idioma=$event.target.value;
      this.parameters.setIdioma($event.target.value);
      this.parameters.setLabels(this.labels);
    }
  }
  restorepass(){
    this.parameters.userType="V";
    this.navController.navigateRoot(['/restorepassword']);
  }
}
