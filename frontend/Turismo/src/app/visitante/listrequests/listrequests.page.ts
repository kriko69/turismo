import { Component, OnInit } from '@angular/core';
import { ReadentService } from 'src/service/empresa/readent/readent.service';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Ent } from 'src/models/ent/ent';
import { NavController } from '@ionic/angular';
import { Languaje } from 'src/models/languaje/languaje';

@Component({
  selector: 'app-listrequests',
  templateUrl: './listrequests.page.html',
  styleUrls: ['./listrequests.page.scss'],
})
export class ListrequestsPage implements OnInit {
  labels=new Languaje();
  requests:Ent[];
  requests2:Ent[];
  linkimage:string;
  busqueda:any='';
  buscar:boolean=false;

  constructor(private readENT:ReadentService,public presenters:PresentersService,
    public parameters:ParametersService,public navController:NavController) { }

  ngOnInit() {
    this.labels=this.parameters.getLabels();
    this.ListReq();
  }
  ionViewDidEnter(){
    this.labels=this.parameters.getLabels();
  }
  ListReq(){
    this.labels=this.parameters.getLabels();
    this.linkimage=this.parameters.ImagenesempresaUrl;
    let info:any;
    console.log("request");
    this.presenters.presentLoading(this.labels.espere);
    this.readENT.ListRequest(this.parameters.getToken()).subscribe(
      data => {
        info= Object.assign(data);
        console.log(info); 
        this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        }else{
          this.requests=info.data;
          this.requests2=info.data;
        }
        }, (error: any)=> {
          this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        }
    );
  } 
  GoReq(req:Ent){
    this.parameters.setEnt(req);
    this.navController.navigateRoot(['/requestinfo']);
  }
  setFilteredItems() {
    console.log("Activities",this.requests);
    
    this.requests = this.filterItems(this.busqueda);
  }
  filterItems(searchTerm){
  return this.requests2.filter((item) => {
      return (item.nombre).toLowerCase().includes(searchTerm.toLowerCase());
  });
  }
  search(){
    this.buscar=!this.buscar;
    if(!this.buscar){
      this.busqueda="";
      this.setFilteredItems();
    }
  }
}
