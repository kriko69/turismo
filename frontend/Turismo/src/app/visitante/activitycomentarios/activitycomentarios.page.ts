import { Component, OnInit } from '@angular/core';
import { Comentarios } from 'src/models/comentarios/comentarios';
import { Languaje } from 'src/models/languaje/languaje';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ReadusService } from 'src/service/visitante/readus/readus.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-activitycomentarios',
  templateUrl: './activitycomentarios.page.html',
  styleUrls: ['./activitycomentarios.page.scss'],
})
export class ActivitycomentariosPage implements OnInit {
  labels=new Languaje();
  coments:Comentarios[];
  coments2:Comentarios[];
  act_id:number;
  busqueda:any='';
  linkimage:string;
  user:string;
  buscar:boolean=false;

  constructor(public parameters:ParametersService,public presenters:PresentersService,
    private readUs:ReadusService, public navController:NavController) { }

  ngOnInit() {
    this.act_id=this.parameters.getActId();
    this.labels=this.parameters.getLabels();
    this.linkimage=this.parameters.ImagenesusuarioUrl;
    this.ListComments();
  }
  
  back(){
    this.navController.navigateBack(['/comprar']);
  }
  setFilteredItems() {
    console.log("Activities",this.coments);
    
    this.coments = this.filterItems(this.busqueda);
  }
  filterItems(searchTerm){
  return this.coments2.filter((item) => {
      return (item.nombre).toLowerCase().includes(searchTerm.toLowerCase());
  });
  }
  search(){
    this.buscar=!this.buscar;
    if(!this.buscar){
      this.busqueda="";
      this.setFilteredItems();
    }
  }
  ListComments(){
    this.labels=this.parameters.getLabels();
    let info:any;
    console.log("request");
    this.presenters.presentLoading(this.labels.espere);
    this.readUs.ListComments(this.act_id).subscribe(
      data => {
        info= Object.assign(data);
        console.log(info); 
        this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
          this.navController.navigateBack(['/comprar']);
        }else{
          this.coments=info.data;
          this.coments2=info.data;
        }
        }, (error: any)=> {
          this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        this.navController.navigateBack(['/comprar']);
        }
    );
  } 
}
