import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ActivitycomentariosPage } from './activitycomentarios.page';

const routes: Routes = [
  {
    path: '',
    component: ActivitycomentariosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ActivitycomentariosPage]
})
export class ActivitycomentariosPageModule {}
