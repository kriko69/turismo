import { Component, OnInit } from '@angular/core';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Languaje } from 'src/models/languaje/languaje';

@Component({
  selector: 'app-tabsus',
  templateUrl: './tabsus.page.html',
  styleUrls: ['./tabsus.page.scss'],
})
export class TabsusPage implements OnInit {
  labels=new Languaje();
  rol:string="";
  constructor(public parameters:ParametersService) { 
    this.rol=parameters.getRol();
  }

  ngOnInit() {
    this.labels=this.parameters.getLabels();
    this.rol=this.parameters.getRol();
  }
  ionTabsDidChange(){
    this.labels=this.parameters.getLabels();
  }
  ionTabsWillChange(){
    this.labels=this.parameters.getLabels();
  }
}
