import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsusPage } from './tabsus.page';

const routes: Routes = [
  {
    path: '',
    component: TabsusPage,
    children: [
      {
        path: 'activities',
        children: [
          {
            path: '',
            loadChildren: '../activities/activities.module#ActivitiesPageModule' 
          }
        ]
      },
      {
        path: 'listrequests',
        children: [
          {
            path: '',
            loadChildren: '../listrequests/listrequests.module#ListrequestsPageModule' 
          }
        ]
      },
      {
        path: 'optionsus',
        children: [
          {
            path: '',
            loadChildren: '../optionsus/optionsus.module#OptionsusPageModule' 
          }
        ]
      },
      { 
        path: 'listcompras',
        children:[
          {
            path:'',
            loadChildren: '../listcompras/listcompras.module#ListcomprasPageModule' 
          }
        ] 
      }
      ,
      {
        path: '',
        redirectTo:'/tabsus/activities',
        pathMatch:'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsusPage]
})
export class TabsusPageModule {}
