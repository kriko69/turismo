import { Component, OnInit } from '@angular/core';
import { Act } from 'src/models/act/act';
import { Compra } from 'src/models/compra/compra';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { NavController } from '@ionic/angular';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ReadusService } from 'src/service/visitante/readus/readus.service';
import { CreateusService } from 'src/service/visitante/createus/createus.service';
import { Languaje } from 'src/models/languaje/languaje';

@Component({
  selector: 'app-comprar',
  templateUrl: './comprar.page.html',
  styleUrls: ['./comprar.page.scss'],
})
export class ComprarPage implements OnInit {
  labels=new Languaje();
  activity=new Act();
  compra=new Compra();
  fotoUsuario:string;
  verify:boolean=false;
  rol:string;
  date: string = new Date().toISOString();
  date2: any = new Date();
  date3:string;
  auxiliardatestring:string;
  idioma:string='espanol';

  constructor(public parameters:ParametersService,public navController:NavController,
    public presenters:PresentersService,private readUS:ReadusService,private createUs:CreateusService) { }

  ngOnInit() {
    this.idioma=this.parameters.getIdioma();
    this.labels=this.parameters.getLabels();
    this.activity=this.parameters.getAct();
    this.fotoUsuario=this.parameters.ImagenesactividadUrl+this.activity.imagen;
    this.rol=this.parameters.getRol();
    if(this.activity.dias!=null && this.activity.dias!="N/A" && this.idioma=='espanol'){
    this.activity.dias=this.activity.dias.replace("L", "Lunes");
    this.activity.dias=this.activity.dias.replace("Ma", "Martes");
    this.activity.dias=this.activity.dias.replace("Mi", "Miercoles");
    this.activity.dias=this.activity.dias.replace("J", "Jueves");
    this.activity.dias=this.activity.dias.replace("V", "Viernes");
    this.activity.dias=this.activity.dias.replace("S", "Sabado");
    this.activity.dias=this.activity.dias.replace("D", "Domingo");
    }
    else if(this.activity.dias!=null && this.activity.dias!="N/A" && this.idioma!='espanol'){
      this.activity.dias=this.activity.dias.replace("L", "Monday");
      this.activity.dias=this.activity.dias.replace("Ma", "Tuesday");
      this.activity.dias=this.activity.dias.replace("Mi", "Wednesday");
      this.activity.dias=this.activity.dias.replace("J", "Thursday");
      this.activity.dias=this.activity.dias.replace("V", "Friday");
      this.activity.dias=this.activity.dias.replace("S", "Saturday");
      this.activity.dias=this.activity.dias.replace("D", "Sunday");
      }
      if(this.date2.getDate()<10){
        this.date3=((this.date2.getYear()+1901)+'-'+(this.date2.getMonth()+1)+'-0'+this.date2.getDate());  
      }
      else{
        this.date3=((this.date2.getYear()+1901)+'-'+(this.date2.getMonth()+1)+'-'+this.date2.getDate());  
      }
    console.log("date3",this.date3);
  }
  back(){
    this.navController.navigateRoot(['/tabsus']);
  }
  totalprice(){
    this.compra.price=this.activity.precio*this.compra.cantity;
    this.verify=false;
  }
  submit(){
    this.auxiliardatestring=this.compra.date;
    console.log("fesha",this.compra.date);
    
    let dateausx1=this.compra.date.split("T")[0];
    console.log("fesha",dateausx1);
    this.compra.date=dateausx1;
        let dateausx=dateausx1.split("-");
        let per=+dateausx[1]-1;
        this.date2=new Date(+dateausx[0],+dateausx[1]-1,+dateausx[2]);
/*
        this.date2.setYear(dateausx[0]);
        this.date2.setMonth(per);
        this.date2.setDate(dateausx[2]);*/
        let dia=this.date2.getUTCDay();
        

        console.log("date2",this.date2);
        console.log("dia",dia);
        let daysactivity:string[]=this.activity.dias.split("-");
        let flag=false;
        switch(dia) { 
          case 4: { //jueves
             if(this.contains(daysactivity,'Jueves') || this.contains(daysactivity,'Thursday')){
                flag=true;
             }
             break; 
          } 
          case 5: {              
             if(this.contains(daysactivity,'Viernes') || this.contains(daysactivity,'Friday')){
              flag=true;
           }
             break; 
          } 
          case 6: {             
            if(this.contains(daysactivity,'Sabado') || this.contains(daysactivity,'Saturday')){
              flag=true;
           }
            break; 
          } 
          case 0: {             
            if(this.contains(daysactivity,'Domingo') || this.contains(daysactivity,'Sunday')){
              flag=true;
           }
            break; 
          } 
          case 1: {             
            if(this.contains(daysactivity,'Lunes') || this.contains(daysactivity,'Monday')){
              flag=true;
           }
            break; 
          } 
          case 2: {             
            if(this.contains(daysactivity,'Martes') || this.contains(daysactivity,'Tuesday')){
              flag=true;
           }
            break; 
          }       
          case 3: {             
            if(this.contains(daysactivity,'Miercoles') || this.contains(daysactivity,'Wednesday')){
              flag=true;
           }
            break; 
          }       
          default: { 
                flag=false;
             break; 
          } 
       } 
    
if(flag){
    let info:any;
    this.presenters.presentLoading(this.labels.espere);
    this.readUS.VerifyBuy(this.compra,this.activity.actividad_id).subscribe(
      data => {
        info= Object.assign(data);
        console.log('info',info);
         this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentAlert(this.labels.noDispTitl,this.labels.noCuposMsg);
          this.verify=false;
          this.compra.date=this.auxiliardatestring;
        }
        else{
          this.verify=true;
        }
        }, (error: any)=> {
           this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        this.back();
        }
      );}
    else
    {
      console.log('flag'+dia, flag);
      this.presenters.presentAlert(this.labels.noDispTitl,this.labels.noCuposMsg);
      this.compra.date=this.auxiliardatestring;
    }
  } 
  buy(){
    if(this.rol!='Off'){
      if(this.compra.cantity!=0){
        let info:any;
        this.presenters.presentLoading(this.labels.espere);
        this.compra.date=this.compra.date.split("T")[0];
        this.createUs.CreateBuy(this.compra,this.activity.actividad_id,this.parameters.getToken()).subscribe(
          data => {
            console.log('data', data);
            info= Object.assign(data);
             this.presenters.quitLoading();
            console.log('exito',info);
            if(info.estado=="exito"){
              this.presenters.presentAlert(this.labels.exitoTitl,this.labels.compraExitoMsg);
            this.navController.navigateRoot(['/tabsus']);
            }else{
              this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
              this.navController.navigateRoot(['/tabsus']);            
            }
            }, (error: any)=> {
            console.log('error', error);
             this.presenters.quitLoading();
            this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
            }
        );
      }
      else{
        this.presenters.presentAlert(this.labels.noPersonsTitl,this.labels.noPersonsMsg);
      }
    }
    else{
      this.presenters.presentAlert(this.labels.noRegistredTitl,this.labels.noRegistredMsg);
      this.navController.navigateRoot(['/login']);
    }
  }
  contains(days:string[],day:string){
    for(let i=0;i<days.length;i++){
      if(days[i]==day){
        return true;
      }
    }
    return false;
  }
  coments(){
    this.parameters.setActId(this.activity.actividad_id);
    this.navController.navigateForward(['/activitycomentarios']);
  }
  entPerfil(){
    this.parameters.setEmpId(this.activity.empresa_id);
    this.parameters.setPaginaAnt('/comprar');
    this.navController.navigateForward(['/perfilempresa']);
  }
}
