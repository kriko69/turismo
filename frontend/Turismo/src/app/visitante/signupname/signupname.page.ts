import { Component, OnInit } from '@angular/core';
import { User } from 'src/models/user/user';
import { NavController } from '@ionic/angular';
import { EspanolService } from 'src/service/comun/lenguaje/espanol/espanol.service';
import { Languaje } from 'src/models/languaje/languaje';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';

@Component({
  selector: 'app-signupname',
  templateUrl: './signupname.page.html',
  styleUrls: ['./signupname.page.scss'],
})
export class SignupnamePage implements OnInit {
   
  labels=new Languaje();
  user=new User();
  constructor(public navController:NavController,
    public espanol:EspanolService,public parameters:ParametersService) { }

  ngOnInit() {
    this.labels=this.espanol.espanol();
    this.user.idioma="espanol";
  }
  gonext(){
    this.parameters.setUser(this.user);
    this.navController.navigateRoot(['/signupinfo']);
  }
  onChange($event){
    console.log("CAMBIO");
    console.log($event.target.value);
    if($event.target.value=='ingles'){
      this.labels=this.espanol.ingles();
      this.user.idioma=$event.target.value;
    this.parameters.setLabels(this.labels);
    }
    else{
      this.labels=this.espanol.espanol();
      this.user.idioma=$event.target.value;
      this.parameters.setLabels(this.labels);
    }
  }
}
