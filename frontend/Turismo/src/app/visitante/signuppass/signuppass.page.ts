import { Component, OnInit } from '@angular/core';
import { CreateusService } from 'src/service/visitante/createus/createus.service';
import { NavController } from '@ionic/angular';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { User } from 'src/models/user/user';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Languaje } from 'src/models/languaje/languaje';

@Component({
  selector: 'app-signuppass',
  templateUrl: './signuppass.page.html',
  styleUrls: ['./signuppass.page.scss'],
})
export class SignuppassPage implements OnInit {
  user=new User();

  passtype:string[]=["password","password"];
  passicon:string[]=["eye","eye"];

  fotoUsuario:string;
  base64Image: string='';
  labels=new Languaje();

  constructor(public parameters:ParametersService,private camera:Camera,
    public presenters:PresentersService,private createUS:CreateusService
    ,public navController:NavController) { }

  ngOnInit() {
    this.labels=this.parameters.getLabels();
    this.user=this.parameters.getUser();
    this.fotoUsuario=this.parameters.defaultimageuser;
  }
  SignUp(){
    if(this.user.pass1!=this.user.pass2){
      this.presenters.presentAlert(this.labels.passNoMatchTitl,this.labels.passNoMatchMsg);
    }
    else{
      if(this.fotoUsuario!=this.parameters.defaultimageuser){
        this.user.imagen=this.base64Image;
      }
      else{
        this.user.imagen="";
      }
      let info:any;
      this.presenters.presentLoading(this.labels.espere);
      this.createUS.CreateUser(this.user).subscribe(
        data => {
          console.log('data', data);
          info= Object.assign(data);
          this.presenters.quitLoading();
          if(info['estado']=='exito')
          {
            this.presenters.presentAlert(this.labels.exitoTitl,this.labels.usrCreateExito);
            this.navController.navigateRoot(['/login']);
          }
          else{
            this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
          }
          console.log('exito');
          }, (error: any)=> {
          console.log('error', error);
          this.presenters.quitLoading();
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
          }
      );
    }
  }
  seepass(pos:number){    
    if(this.passtype[pos]=='password')
    {
      this.passtype[pos]='text';
      this.passicon[pos]='eye-off';
    }
    else{
      this.passtype[pos]='password';
      this.passicon[pos]='eye';
    }
  }
  openCamara(){
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation:true
    }
    
    this.camera.getPicture(options).then((imageData)=> {
      this.base64Image = 'data:image/jpeg;base64,'+ imageData;
      this.fotoUsuario = this.base64Image;
    },(err)=>{
      console.log('Error en la foto tomada')
    });
  }
  openGallery(){
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation:true
    }

    this.camera.getPicture(options).then((imageData)=> {
    this.base64Image = 'data:image/jpeg;base64,'+ imageData;
    this.fotoUsuario = this.base64Image;
  },(err)=>{
    console.log('Error en la foto tomada')
  });
  }
}
