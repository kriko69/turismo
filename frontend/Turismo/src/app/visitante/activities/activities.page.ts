import { Component, OnInit } from '@angular/core';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { ReadusService } from 'src/service/visitante/readus/readus.service';
import { Act } from 'src/models/act/act';
import { NavController } from '@ionic/angular';
import { Languaje } from 'src/models/languaje/languaje';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.page.html',
  styleUrls: ['./activities.page.scss'],
})
export class ActivitiesPage implements OnInit {
  labels=new Languaje();
  activities:Act[];
  activities2:Act[];
  busqueda:any='';
  linkimage:string;
  idioma:string='espanol';
  buscar:boolean=false;

  constructor(private readUs:ReadusService,public presenters:PresentersService,
    public parameters:ParametersService,public navController:NavController) { }

  ngOnInit() {
    this.idioma=this.parameters.getIdioma();
    this.labels=this.parameters.getLabels();
    this.ListActivities();
    this.linkimage=this.parameters.ImagenesactividadUrl;
  }
  ionViewDidEnter(){
    this.idioma=this.parameters.getIdioma();
    this.labels=this.parameters.getLabels();
  }
  ListActivities(){
    this.idioma=this.parameters.getIdioma();
    this.labels=this.parameters.getLabels();
    let info:any;
    console.log("activities");
    this.presenters.presentLoading(this.labels.espere);
    this.readUs.ListAct(/*this.parameters.getToken()*/).subscribe(
      data => {
        info= Object.assign(data);    
         this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        }else{
          this.activities=info.data;
          this.activities2=info.data;
          console.log(this.activities);    
        }
        }, (error: any)=> {
           this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        }
    );
  } 
  CreateActivity(){
    this.navController.navigateRoot(['/createactivity']);
  }
  Buys(act:Act){
    act
    console.log("nombre emp",act.nombre_empresa);
    
    this.parameters.setAct(act);
    this.navController.navigateRoot(['/comprar']);
  }
  setFilteredItems() {
    console.log("Activities",this.activities);
    
    this.activities = this.filterItems(this.busqueda);
  }
  filterItems(searchTerm){
    if(this.idioma=='espanol'){
      return this.activities2.filter((item) => {
          return (item.nombre_esp).toLowerCase().includes(searchTerm.toLowerCase());
      });
    }
    else{
  return this.activities2.filter((item) => {
    return (item.nombre_eng).toLowerCase().includes(searchTerm.toLowerCase());
  });
    }
  }
  search(){
    this.buscar=!this.buscar;
    if(!this.buscar){
      this.busqueda="";
      this.setFilteredItems();
    }
  }
}
