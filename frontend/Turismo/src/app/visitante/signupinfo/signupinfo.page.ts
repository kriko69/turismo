import { Component, OnInit } from '@angular/core';
import { User } from 'src/models/user/user';
import { NavController } from '@ionic/angular';
import { Languaje } from 'src/models/languaje/languaje';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';

@Component({
  selector: 'app-signupinfo',
  templateUrl: './signupinfo.page.html',
  styleUrls: ['./signupinfo.page.scss'],
})
export class SignupinfoPage implements OnInit {

  user=new User();
  labels=new Languaje();
  constructor(public navController:NavController,public parameters:ParametersService) { }

  ngOnInit() {
    this.labels=this.parameters.getLabels();
    this.user=this.parameters.getUser();
  }
  gonext(){
    this.user.fecha_nacimiento=this.user.fecha_nacimiento.split("T")[0];
    this.parameters.setUser(this.user);
    this.navController.navigateRoot(['/signuppass']);
  }

}
