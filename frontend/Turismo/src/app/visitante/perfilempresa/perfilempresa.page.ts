import { Component, OnInit } from '@angular/core';
import { Languaje } from 'src/models/languaje/languaje';
import { Ent } from 'src/models/ent/ent';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { ModifyentService } from 'src/service/empresa/modifyent/modifyent.service';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ReadusService } from 'src/service/visitante/readus/readus.service';

@Component({
  selector: 'app-perfilempresa',
  templateUrl: './perfilempresa.page.html',
  styleUrls: ['./perfilempresa.page.scss'],
})
export class PerfilempresaPage implements OnInit {

  empresa_id:number;
  labels=new Languaje();
  ent:Ent=new Ent();
  linkimage:string;
  stars=['star-outline','star-outline','star-outline','star-outline','star-outline'];

  constructor(public parameters:ParametersService, public readUs:ReadusService,
    public presenters:PresentersService, public router:Router
    ,public navController:NavController) { }

  ngOnInit() {
    this.labels=this.parameters.getLabels();
    this.empresa_id=this.parameters.getEmpId();
    this.getEnt();
    this.rate(this.ent.calificacion);
  }

  getEnt(){
    console.log("yes");
    let info:any;
    this.presenters.presentLoading(this.labels.espere);
    this.readUs.EntProfile(this.parameters.getToken(),this.empresa_id).subscribe(
      data => {
        info= Object.assign(data);
        console.log(info);
        this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
          this.navController.navigateBack([this.parameters.getPaginaAnt()]);
        }
        else{
          console.log('info', info); 
          this.ent=info.data;
          this.linkimage=this.parameters.ImagenesempresaUrl+this.ent.imagen;
          this.rate(this.ent.calificacion);
        }
        }, (error: any)=> {
          console.log('error', error); 
          this.presenters.quitLoading();
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
          this.navController.navigateBack([this.parameters.getPaginaAnt()]);
        }
    );
  } 

  rate(star:number){
    for(let i=0;i<this.stars.length;i++){
      this.stars[i]='star-outline';
    }
    for(let i=0;i<star;i++){
      this.stars[i]='star';
    }
  }
  back(){
    this.navController.navigateBack([this.parameters.getPaginaAnt()]);
  }
}
