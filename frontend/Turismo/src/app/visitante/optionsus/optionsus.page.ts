import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { Languaje } from 'src/models/languaje/languaje';
import { EspanolService } from 'src/service/comun/lenguaje/espanol/espanol.service';
import { ModifyusService } from 'src/service/visitante/modifyus/modifyus.service';

@Component({
  selector: 'app-optionsus',
  templateUrl: './optionsus.page.html',
  styleUrls: ['./optionsus.page.scss'],
})
export class OptionsusPage implements OnInit {
  rol:string="";
  labels=new Languaje();
  idioma:string;

  constructor(private router:Router,private storage: Storage,
    public presenters:PresentersService,public espanol:EspanolService,
    public parameters:ParametersService,public navController:NavController,
    public modifyUs:ModifyusService) { }

  ngOnInit() {
    this.labels=this.parameters.getLabels();
    this.rol=this.parameters.getRol();
    if(this.parameters.getIdioma()=='espanol'){
      this.idioma="Español";
    }
    else{
      this.idioma="English";
    }
  }
  SignOut(){
    this.parameters.SignOut();
  }
  SignIn(){
    this.navController.navigateRoot(['/login']);
  }
  Profile(){
    this.navController.navigateRoot(['/profile']);
  }
  Pass(){
    this.navController.navigateRoot(['/changepassus']);
  }
  onChange($event){
    console.log("CAMBIO");
    console.log($event.target.value);
    if($event.target.value=='ingles'){
      this.idioma="English";
      this.labels=this.espanol.ingles();
      this.parameters.setLabels(this.labels);
      this.parameters.setIdioma($event.target.value);
      if(this.parameters.getRol()!='Off')
      {
        let info:any;
        this.presenters.presentLoading(this.labels.espere);
        this.modifyUs.ModifyLanguaje(this.parameters.getToken(),$event.target.value).subscribe(
          data => {
            info= Object.assign(data);
            console.log(info);
            this.presenters.quitLoading();
            if(info.estado=="error"){
              this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
              this.navController.navigateRoot(['/login']);
            }
            }, (error: any)=> {
              console.log('error', error); 
              this.presenters.quitLoading();
              this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
              this.navController.navigateRoot(['/login']);
            }
          );
      }
    }
    else{
      this.idioma="Español";
      this.labels=this.espanol.espanol();
      this.parameters.setLabels(this.labels);
      this.parameters.setIdioma($event.target.value);
      if(this.parameters.getRol()!='Off')
      {
        let info:any;
        this.presenters.presentLoading(this.labels.espere);
        this.modifyUs.ModifyLanguaje(this.parameters.getToken(),$event.target.value).subscribe(
          data => {
            info= Object.assign(data);
            console.log(info);
            this.presenters.quitLoading();
            if(info.estado=="error"){
              this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
              this.navController.navigateRoot(['/login']);
            }
            }, (error: any)=> {
              console.log('error', error); 
              this.presenters.quitLoading();
              this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
              this.navController.navigateRoot(['/login']);
            }
          );
      }
    }
  }

}
