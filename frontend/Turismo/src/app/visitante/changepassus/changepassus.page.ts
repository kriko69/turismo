import { Component, OnInit } from '@angular/core';
import { Changepass } from 'src/models/changepass/changepass';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ModifyusService } from 'src/service/visitante/modifyus/modifyus.service';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Languaje } from 'src/models/languaje/languaje';

@Component({
  selector: 'app-changepassus',
  templateUrl: './changepassus.page.html',
  styleUrls: ['./changepassus.page.scss'],
})
export class ChangepassusPage implements OnInit {
  labels=new Languaje();

  constructor(public presenters:PresentersService,public modUs:ModifyusService,
              public parameters:ParametersService,public router:Router
              ,public navController:NavController) { }

  pass=new Changepass();

  passtype:string[]=["password","password","password"];
  passicon:string[]=["eye","eye","eye"];

  ngOnInit() {
    this.labels=this.parameters.getLabels();
  }
  Confirm(){
    if(this.pass.newpass1!=this.pass.newpass2){
      this.presenters.presentAlert(this.labels.passNoMatchTitl,this.labels.passNoMatchMsg);
    }
    else{
      let info:any;
      this.presenters.presentLoading(this.labels.espere);
      this.modUs.ModifyPass(this.parameters.getToken(),this.pass).subscribe(
        data => {
          console.log('data', data);
          info= Object.assign(data);
           this.presenters.quitLoading();
          if(info.estado=="exito"){
            this.presenters.presentAlert(this.labels.exitoTitl,this.labels.msgPassCambio);
            this.navController.navigateRoot(['/tabsus']);
          }
          else{
            this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
            this.navController.navigateRoot(['/tabsus']);
          }
          console.log('exito');
          }, (error: any)=> {
          console.log('error', error);
           this.presenters.quitLoading();
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
          this.navController.navigateRoot(['/tabsus']);
          }
      );
    }
  }
  seepass(pos:number){    
    if(this.passtype[pos]=='password')
    {
      this.passtype[pos]='text';
      this.passicon[pos]='eye-off';
    }
    else{
      this.passtype[pos]='password';
      this.passicon[pos]='eye';
    }
  }
  back(){
    this.navController.navigateRoot(['/tabsus']);
  }
}
