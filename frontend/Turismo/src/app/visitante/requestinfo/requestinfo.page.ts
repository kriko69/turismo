import { Component, OnInit } from '@angular/core';
import { Ent } from 'src/models/ent/ent';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { ModifyentService } from 'src/service/empresa/modifyent/modifyent.service';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Languaje } from 'src/models/languaje/languaje';

@Component({
  selector: 'app-requestinfo',
  templateUrl: './requestinfo.page.html',
  styleUrls: ['./requestinfo.page.scss'],
})
export class RequestinfoPage implements OnInit {
  labels=new Languaje();
  ent:Ent=new Ent();
  linkimage:string;

  constructor(public parameters:ParametersService, public modEnt:ModifyentService,
              public presenters:PresentersService, public router:Router
              ,public navController:NavController) { 
    this.ent=parameters.getEnt();
  }

  ngOnInit() { 
    this.labels=this.parameters.getLabels();
    this.ent=this.parameters.getEnt();
    this.linkimage=this.parameters.ImagenesempresaUrl+this.ent.imagen;
  }
  ChangeState(state:number){
    let token=this.parameters.getToken();
    let info:any;
    this.presenters.presentLoading(this.labels.espere);
    this.modEnt.Afiliation(state,this.ent.empresa_id,token).subscribe(
      data => {
        info= Object.assign(data);
        console.log('exito');
        this.presenters.quitLoading();
        console.log('info', info); 
        if(info["estado"]=="exito"){
          this.presenters.presentAlert(this.labels.exitoTitl,this.labels.respEmail);
          this.navController.navigateRoot(['/tabsus']);
        }
        else{
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
          this.navController.navigateRoot(['/tabsus']);
        }
        }, (error: any)=> {
          this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        this.navController.navigateRoot(['/tabsus']);
        }
    );
  }
  back(){
    this.navController.navigateRoot(['/tabsus']);
  }
}
