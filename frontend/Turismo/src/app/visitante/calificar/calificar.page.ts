import { Component, OnInit } from '@angular/core';
import { Userbuys } from 'src/models/userbuys/userbuys';
import { Languaje } from 'src/models/languaje/languaje';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { NavController, AlertController } from '@ionic/angular';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ReadusService } from 'src/service/visitante/readus/readus.service';
import { CreateusService } from 'src/service/visitante/createus/createus.service';

@Component({
  selector: 'app-calificar',
  templateUrl: './calificar.page.html',
  styleUrls: ['./calificar.page.scss'],
})
export class CalificarPage implements OnInit {
  [x: string]: any;
  labels=new Languaje();
  buy=new Userbuys();
  fotoAct:string;
  idioma:string;
  resolveRec:any=[];
  calificacion:number;
  comentario:string="";
  calificado=false;

  stars=['star-outline','star-outline','star-outline','star-outline','star-outline'];

  constructor(public parameters:ParametersService,public navController:NavController,
    public presenters:PresentersService,private readUSer:ReadusService
    ,private createUs:CreateusService,public alertCtrl:AlertController)
 { }

  ngOnInit() {
    this.idioma=this.parameters.getIdioma();
    this.labels=this.parameters.getLabels();
    this.buy.actividad_id=this.parameters.getActId();
    this.getBuy();
  }
  back(){
    this.navController.navigateRoot(['/tabsus']);
  }
  rate(star:number){
    if(!this.calificado){
      for(let i=0;i<this.stars.length;i++){
        this.stars[i]='star-outline';
      }
      for(let i=0;i<star;i++){
        this.stars[i]='star';
      }
      this.calificacion=star;
    }
  }
  sendComment(){
      let info:any;
      this.presenters.presentLoading(this.labels.espere);
      console.log("actividad_id",this.buy.actividad_id);
      
      this.createUs.createCalification(this.parameters.getToken(),this.buy.actividad_id,this.calificacion,this.comentario).subscribe(
        data => {
          console.log('data', data);
          info= Object.assign(data);
           this.presenters.quitLoading();
          if(info.estado=="exito"){
            this.presenters.presentAlert(this.labels.exitoTitl,this.labels.succescomentario);
            this.navController.navigateRoot(['/tabsus']);
          }
          else{
            this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
            this.navController.navigateRoot(['/tabsus']);
          }
          console.log('exito');
          }, (error: any)=> {
          console.log('error', error);
           this.presenters.quitLoading();
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
          this.navController.navigateRoot(['/tabsus']);
          }
      );
  }
  entPerfil(){
    this.parameters.setEmpId(this.buy.empresa_id);
    this.parameters.setPaginaAnt('/calificar');
    this.navController.navigateForward(['/perfilempresa']);
  }
  getBuy(){
    let info:any;
    console.log("request");
    this.presenters.presentLoading(this.labels.espere);
    this.readUSer.ListMycomentAct(this.parameters.getToken(),this.buy.actividad_id).subscribe(
      data => {
        info= Object.assign(data);
        console.log(info); 
        this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
          this.navController.navigateBack(['/tabsus']);
        }else{
          this.buy=info.data;
          this.fotoAct=this.parameters.ImagenesactividadUrl+this.buy.imagen;
          if(this.buy.calificacion_usuario>0){
            this.rate(this.buy.calificacion_usuario);
            this.comentario=this.buy.comentario_usuario;
            this.calificado=true;
          }
        }
        }, (error: any)=> {
          this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        this.navController.navigateBack(['/tabsus']);
        }
    );
  } 
}
