import { Component, OnInit } from '@angular/core';
import { Userbuys } from 'src/models/userbuys/userbuys';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ReadusService } from 'src/service/visitante/readus/readus.service';
import { Languaje } from 'src/models/languaje/languaje';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-listcompras',
  templateUrl: './listcompras.page.html',
  styleUrls: ['./listcompras.page.scss'],
})
export class ListcomprasPage implements OnInit {
  labels=new Languaje();
  buys:Userbuys[];
  buys2:Userbuys[];
  busqueda:any='';
  linkimage:string;
  user:string;
  idioma:string='espanol';
  buscar:boolean=false;

  constructor(public parameters:ParametersService,public presenters:PresentersService,
    private readUs:ReadusService, public navController:NavController) { }

  ngOnInit() {
    this.idioma=this.parameters.getIdioma();
    this.labels=this.parameters.getLabels();
    this.user=this.parameters.getRol();
      this.ListBuy();
      this.linkimage=this.parameters.ImagenesactividadUrl;
  }
  ionViewDidEnter(){
    this.idioma=this.parameters.getIdioma();
    this.labels=this.parameters.getLabels();
  }
  ListBuy(){
    this.idioma=this.parameters.getIdioma();
    this.labels=this.parameters.getLabels();
    if(this.user!='Off'){
    let info:any;
    console.log("request");
    this.presenters.presentLoading(this.labels.espere);
    this.readUs.ListBuys(this.parameters.getToken()).subscribe(
      data => {
        info= Object.assign(data);
        console.log(info); 
        this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        }else{
          this.buys=info.data;
          this.buys2=info.data;
        }
        }, (error: any)=> {
          this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        }
    );
  }
  else{
    this.presenters.presentAlert(this.labels.noRegistredTitl,this.labels.noRegistredMsg);
  }
}
setFilteredItems() {
  console.log("Activities",this.buys);
  
  this.buys = this.filterItems(this.busqueda);
}
filterItems(searchTerm){
  if(this.idioma=='espanol'){
    return this.buys2.filter((item) => {
        return (item.nombre_esp).toLowerCase().includes(searchTerm.toLowerCase());
    });
  }
  else{
return this.buys2.filter((item) => {
  return (item.nombre_eng).toLowerCase().includes(searchTerm.toLowerCase());
});
  }
}
search(){
  this.buscar=!this.buscar;
  if(!this.buscar){
    this.busqueda="";
    this.setFilteredItems();
  }
}
calificar(buy:Userbuys){
  this.parameters.setActId(buy.actividad_id);
  this.navController.navigateRoot(['/calificar']);
}

}
