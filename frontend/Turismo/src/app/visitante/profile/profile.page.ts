import { Component, OnInit } from '@angular/core';
import { ReadusService } from 'src/service/visitante/readus/readus.service';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { User } from 'src/models/user/user';
import { ModifyusService } from 'src/service/visitante/modifyus/modifyus.service';
import { NavController } from '@ionic/angular';

import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Languaje } from 'src/models/languaje/languaje';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  labels=new Languaje();
  user=new User();
  edit:boolean=false;
  
  fotoUsuario:string;
  base64Image: string='';
  val: boolean=false;

  constructor(private camera: Camera,private readUS:ReadusService,
    public presenters:PresentersService,public parameters:ParametersService,
    private mdUser:ModifyusService,public navController:NavController) { }

  ngOnInit() {
    this.labels=this.parameters.getLabels();
    this.getUser();
  }
  Edit(){
    this.edit=true;
  }
  getUser(){
    console.log("yes");
    let info:any;
    this.presenters.presentLoading(this.labels.espere);
    this.readUS.Profile(this.parameters.getToken()).subscribe(
      data => {
        info= Object.assign(data);
        console.log(info);
        this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
          this.navController.navigateRoot(['/tabsus']);
        }
        else{
          console.log('info', info); 
          this.user=info.data;
          this.fotoUsuario=this.parameters.ImagenesusuarioUrl+this.user.imagen;
        }
        }, (error: any)=> {
          console.log('error', error); 
          this.presenters.quitLoading();
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
          this.navController.navigateRoot(['/tabsus']);
        }
    );
  } 
  editProfile(){
    if(this.user.pass1!=this.user.pass2){
      this.presenters.presentAlert(this.labels.passNoMatchTitl,this.labels.passNoMatchMsg);
    }
    else{
      if(this.base64Image!=""){
        this.user.imagen=this.base64Image;
      }
      else{
        this.user.imagen="";
      }
    this.user.fecha_nacimiento=this.user.fecha_nacimiento.split("T")[0];
    console.log(this.user.fecha_nacimiento);
    
    let info:any;
    this.presenters.presentLoading(this.labels.espere);
    this.mdUser.ModifyProfile(this.parameters.getToken(),this.user).subscribe(
      data => {
        info= Object.assign(data);
        console.log(info);
        this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
          this.navController.navigateRoot(['/tabsus']);
        }
        else{
          console.log('info', info); 
          this.presenters.presentAlert(this.labels.exitoTitl,this.labels.exitoUserEdit);
          this.navController.navigateRoot(['/tabsus']);
        }
        }, (error: any)=> {
          console.log('error', error); 
          this.presenters.quitLoading();
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
          this.navController.navigateRoot(['/tabsus']);
        }
      );
    }
  }
  back(){
    this.edit=false;
    this.navController.navigateRoot(['/tabsus']);
  }
  openCamara(){
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation:true
    }
    
    this.camera.getPicture(options).then((imageData)=> {
      this.base64Image = 'data:image/jpeg;base64,'+ imageData;
      this.fotoUsuario = this.base64Image;
      this.val=true;
    },(err)=>{
      console.log('Error en la foto tomada')
    });
  }
  openGallery(){
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation:true
    }

    this.camera.getPicture(options).then((imageData)=> {
    this.base64Image = 'data:image/jpeg;base64,'+ imageData;
    this.fotoUsuario = this.base64Image;
    this.val=true;
  },(err)=>{
    console.log('Error en la foto tomada')
  });
  }
}