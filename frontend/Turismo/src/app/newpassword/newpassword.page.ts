import { Component, OnInit } from '@angular/core';
import { Languaje } from 'src/models/languaje/languaje';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ModifyusService } from 'src/service/visitante/modifyus/modifyus.service';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Changepass } from 'src/models/changepass/changepass';
import { ModifyentService } from 'src/service/empresa/modifyent/modifyent.service';

@Component({
  selector: 'app-newpassword',
  templateUrl: './newpassword.page.html',
  styleUrls: ['./newpassword.page.scss'],
})
export class NewpasswordPage implements OnInit {
  labels=new Languaje();
  pass=new Changepass();
  passtype:string[]=["password","password"];
  passicon:string[]=["eye","eye"];
  genericId:number;
  userType:string;

  constructor(public presenters:PresentersService,public modEnt:ModifyentService,
              public parameters:ParametersService,public router:Router
              ,public navController:NavController) { }

  ngOnInit() {
    this.labels=this.parameters.getLabels();
    this.genericId=this.parameters.genericId;
    this.userType=this.parameters.userType;
  }
  seepass(pos:number){    
    if(this.passtype[pos]=='password')
    {
      this.passtype[pos]='text';
      this.passicon[pos]='eye-off';
    }
    else{
      this.passtype[pos]='password';
      this.passicon[pos]='eye';
    }
  }
  back(){
    this.navController.navigateRoot(['/login']);
  }
  Confirm(){
    if(this.pass.newpass1!=this.pass.newpass2){
      this.presenters.presentAlert(this.labels.passNoMatchTitl,this.labels.passNoMatchMsg);
    }
    else{
      let info:any;
      this.presenters.presentLoading(this.labels.espere);
      this.modEnt.RestorePass(this.genericId,this.userType,this.pass.newpass1).subscribe(
        data => {
          console.log('data', data);
          info= Object.assign(data);
           this.presenters.quitLoading();
          if(info.estado=="exito"){
            this.presenters.presentAlert(this.labels.exitoTitl,this.labels.msgPassCambio);
            this.navController.navigateRoot(['/login']);
          }
          else{
            this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
            this.navController.navigateRoot(['/login']);
          }
          console.log('exito');
          }, (error: any)=> {
          console.log('error', error);
           this.presenters.quitLoading();
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
          this.navController.navigateRoot(['/login']);
          }
      );
    }
  }
}
