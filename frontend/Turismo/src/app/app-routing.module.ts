import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
 { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: './visitante/login/login.module#LoginPageModule' },
  { path: 'signupname', loadChildren: './visitante/signupname/signupname.module#SignupnamePageModule' },
  { path: 'signupinfo', loadChildren: './visitante/signupinfo/signupinfo.module#SignupinfoPageModule' },
  { path: 'signuppass', loadChildren: './visitante/signuppass/signuppass.module#SignuppassPageModule' },
  { path: 'signupenterpriseinfo', loadChildren: './empresa/signupenterpriseinfo/signupenterpriseinfo.module#SignupenterpriseinfoPageModule' },
  { path: 'signupenterprisecontact', loadChildren: './empresa/signupenterprisecontact/signupenterprisecontact.module#SignupenterprisecontactPageModule' },
  { path: 'signupenterprisepass', loadChildren: './empresa/signupenterprisepass/signupenterprisepass.module#SignupenterprisepassPageModule' },
  { path: 'loginenterprise', loadChildren: './empresa/loginenterprise/loginenterprise.module#LoginenterprisePageModule' },
  { path: 'tabs', loadChildren: './empresa/tabs/tabs.module#TabsPageModule' },
  { path: 'tabsus', loadChildren: './visitante/tabsus/tabsus.module#TabsusPageModule' },
  { path: 'requestinfo', loadChildren: './visitante/requestinfo/requestinfo.module#RequestinfoPageModule' },
  { path: 'profileent', loadChildren: './empresa/profileent/profileent.module#ProfileentPageModule' },
  { path: 'changepassus', loadChildren: './visitante/changepassus/changepassus.module#ChangepassusPageModule' },
  { path: 'changepassent', loadChildren: './empresa/changepassent/changepassent.module#ChangepassentPageModule' },
  { path: 'createactivity',loadChildren: './empresa/createactivity/createactivity.module#CreateactivityPageModule'},
  { path: 'modifyactivity',loadChildren: './empresa/modifyactivity/modifyactivity.module#ModifyactivityPageModule'},
  { path: 'reservations',loadChildren: './empresa/reservations/reservations.module#ReservationsPageModule'},
  { path: 'profile',loadChildren: './visitante/profile/profile.module#ProfilePageModule'},
  { path: 'comprar', loadChildren: './visitante/comprar/comprar.module#ComprarPageModule' },  { path: 'calificar', loadChildren: './visitante/calificar/calificar.module#CalificarPageModule' },
  { path: 'perfilempresa', loadChildren: './visitante/perfilempresa/perfilempresa.module#PerfilempresaPageModule' },
  { path: 'activitycomentarios', loadChildren: './visitante/activitycomentarios/activitycomentarios.module#ActivitycomentariosPageModule' },
  { path: 'buzon', loadChildren: './empresa/buzon/buzon.module#BuzonPageModule' },
  { path: 'restorepassword', loadChildren: './restorepassword/restorepassword.module#RestorepasswordPageModule' },
  { path: 'newpassword', loadChildren: './newpassword/newpassword.module#NewpasswordPageModule' },


];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
