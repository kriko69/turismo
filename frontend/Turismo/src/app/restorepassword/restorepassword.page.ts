import { Component, OnInit } from '@angular/core';
import { Languaje } from 'src/models/languaje/languaje';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { CreateusService } from 'src/service/visitante/createus/createus.service';
import { CreateentService } from 'src/service/empresa/createent/createent.service';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ReadentService } from 'src/service/empresa/readent/readent.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-restorepassword',
  templateUrl: './restorepassword.page.html',
  styleUrls: ['./restorepassword.page.scss'],
})
export class RestorepasswordPage implements OnInit {
  labels=new Languaje();
  user="";
  email="";
  usertype="";
  verificado=false;
  codigo="";
  id=0;

  constructor(public parameters:ParametersService, public createUs:CreateusService, 
    public createEnt:CreateentService, public presenters:PresentersService, 
    public readEnt:ReadentService,public navController:NavController) {
    this.labels=parameters.getLabels();
    this.usertype=parameters.userType;
   }
  ngOnInit() {
  }
  SendEmail(){
    let info:any;
    this.presenters.presentLoading(this.labels.espere);
    this.createEnt.GetCodePass(this.email).subscribe(
      data => {
        info= Object.assign(data);    
         this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        }else{
          this.verificado=true;             
        }
        }, (error: any)=> {
           this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        }
    );
  }
  SendUser(){
    let info:any;
    this.presenters.presentLoading(this.labels.espere);
    this.createUs.GetCodePass(this.user).subscribe(
      data => {
        info= Object.assign(data);    
         this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        }else{
          this.verificado=true;            
        }
        }, (error: any)=> {
           this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        }
    );
  }
  SendCode(){
    let info:any;
    this.presenters.presentLoading(this.labels.espere);
    this.readEnt.VerifyPassCode(this.codigo).subscribe(
      data => {
        info= Object.assign(data);    
        console.log("info",info);
         this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        }else{
          this.id=info.data.id;      
          this.parameters.genericId=this.id;
          this.navController.navigateRoot(['/newpassword']);      
        }
        }, (error: any)=> {
           this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentAlert(this.labels.error,this.labels.msgerror);
        }
    );
  }
  back(){
    this.navController.navigateRoot(['/login']);
  }
}
