import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ReadentService } from 'src/service/empresa/readent/readent.service';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { DeleteentService } from 'src/service/empresa/deleteent/deleteent.service';
import { NavController } from '@ionic/angular';
import { Notificacion } from 'src/models/notificacion/notificacion';

@Component({
  selector: 'app-buzon',
  templateUrl: './buzon.page.html',
  styleUrls: ['./buzon.page.scss'],
})
export class BuzonPage implements OnInit {
  notificaciones:Notificacion[]=[];
  notificaciones2:Notificacion[]=[];

  calif:Notificacion[];
  comp:Notificacion[];

  mensajeCalif:string=" comento la actividad ";
  mensajeComp:string=" compro cupos en la actividad ";
  tipoCalif:string="Calificacion";
  tipoComp:string="Compra";
  busqueda:any='';
  linkimage:string;
  buscar:boolean=false;

  constructor(private router:Router,private readENT:ReadentService,
    public presenters:PresentersService,
    public parameters:ParametersService,private delENT:DeleteentService
    ,public navController:NavController) { }

  ngOnInit() {
    this.ListNotification();
    this.linkimage=this.parameters.ImagenesusuarioUrl;
  }

  setFilteredItems() {
    console.log("Activities",this.notificaciones);
    
    this.notificaciones = this.filterItems(this.busqueda);
  }

  filterItems(searchTerm){
      return this.notificaciones2.filter((item) => {
          return (item.nombre_esp).toLowerCase().includes(searchTerm.toLowerCase());
      });
  }

  search(){
    this.buscar=!this.buscar;
    if(!this.buscar){
      this.busqueda="";
      this.setFilteredItems();
    }
  }

  ListNotification(){
    let info:any;
    this.presenters.presentLoading("Espere por favor...");
    this.readENT.ReadBuzon(this.parameters.getToken()).subscribe(
      data => {
        info= Object.assign(data);
        console.log(info);
        this.presenters.quitLoading();
        if(info.estado=="exito"){
          console.log("exito",info);
          this.calif=info.calificaciones_comentarios;
          this.comp=info.compras;
          this.formatCalif();
          console.log("calif",this.calif);
          console.log("comp",this.comp);
        }
        else{
          this.presenters.presentErrorAlert();
        }
        }, (error: any)=> {
          this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentErrorAlert();
        }
    );
  }

  formatCalif(){
    if(this.calif!=null){
      for(let i=0;i<this.calif.length;i++){
        this.calif[i].tipo=this.tipoCalif;
        this.calif[i].mensaje=this.calif[i].usuario+this.mensajeCalif+this.calif[i].nombre_esp+".";
      }
    }
    this.formatComp();
  }

  formatComp(){
    if(this.comp!=null){
      for(let i=0;i<this.comp.length;i++){
        this.comp[i].tipo=this.tipoComp;
        this.comp[i].mensaje=this.comp[i].usuario+this.mensajeComp+this.comp[i].nombre_esp+".";
      }
    }
    this.joinCalifComp();
  }

  joinCalifComp(){    
    if(this.comp!=null){
      console.log("entra");
      if(this.calif!=null){
        this.notificaciones=this.calif.concat(this.comp);

        this.notificaciones.sort(function (a, b) {
          if (a.fecha > b.fecha) {
            return -1;
          }
          if (a.fecha < b.fecha) {
            return 1;
          }
          return 0;
        });

      }
      else{
        this.notificaciones=this.comp;
      }
    }
    else if(this.calif!=null){
      this.notificaciones=this.calif;
    }
    this.notificaciones2=this.notificaciones;
    console.log("notificaciones",this.notificaciones);    
  }
}
