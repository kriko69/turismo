import { Component, OnInit } from '@angular/core';
import { Ent } from 'src/models/ent/ent';
import { NavController } from '@ionic/angular';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';

@Component({
  selector: 'app-signupenterpriseinfo',
  templateUrl: './signupenterpriseinfo.page.html',
  styleUrls: ['./signupenterpriseinfo.page.scss'],
})
export class SignupenterpriseinfoPage implements OnInit {

  ent=new Ent();
  constructor(private parameters:ParametersService,public navController:NavController) { }

  ngOnInit() {
  }

  gonext(){
    this.parameters.setEnt(this.ent);
    this.navController.navigateRoot(['/signupenterprisecontact']);
  }
}
