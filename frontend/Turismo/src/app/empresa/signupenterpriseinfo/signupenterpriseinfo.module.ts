import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SignupenterpriseinfoPage } from './signupenterpriseinfo.page';

const routes: Routes = [
  {
    path: '',
    component: SignupenterpriseinfoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SignupenterpriseinfoPage]
})
export class SignupenterpriseinfoPageModule {}
