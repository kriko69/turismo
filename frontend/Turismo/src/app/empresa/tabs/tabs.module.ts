import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'listactivities',
        children: [
          {
            path: '',
            loadChildren: '../listactivities/listactivities.module#ListactivitiesPageModule' 
          }
        ]
      },
      {
        path: 'options',
        children: [
          {
            path: '',
            loadChildren: '../options/options.module#OptionsPageModule' 
          }
        ]
      },
      {
        path: 'buzon',
        children: [
          {
            path: '',
            loadChildren: '../buzon/buzon.module#BuzonPageModule' 
          }
        ]
      },
      {
        path: '',
        redirectTo:'/tabs/listactivities',
        pathMatch:'full'
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
