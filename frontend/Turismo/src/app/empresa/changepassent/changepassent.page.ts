import { Component, OnInit } from '@angular/core';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ModifyentService } from 'src/service/empresa/modifyent/modifyent.service';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Router } from '@angular/router';
import { Changepass } from 'src/models/changepass/changepass';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-changepassent',
  templateUrl: './changepassent.page.html',
  styleUrls: ['./changepassent.page.scss'],
})
export class ChangepassentPage implements OnInit {
constructor(public presenters:PresentersService,public modEnt:ModifyentService,
              public parameters:ParametersService,public router:Router,
              public navController:NavController) { }

  pass=new Changepass();
  titleerr="Passwords no coinciden.";
  messageerr="Verifique los datos ingresados e intente de nuevo por favor.";
  titleex="Exito.";
  messageex="Password Cambiado con Exito.";

  passtype:string[]=["password","password","password"];
  passicon:string[]=["eye","eye","eye"];

  ngOnInit() {
  }
  Confirm(){
    if(this.pass.newpass1!=this.pass.newpass2){
      this.presenters.presentAlert(this.titleerr,this.messageerr);
    }
    else{
      let info:any;
      this.presenters.presentLoading("Espere por favor...");
      this.modEnt.ModifyPass(this.parameters.getToken(),this.pass).subscribe(
        data => {
          console.log('data', data);
          info= Object.assign(data);
          this.presenters.quitLoading();
          if(info.estado=="exito"){
            this.presenters.presentAlert(this.titleex,this.messageex);
            //this.navController.navigateRoot(['/tabs']);
            this.navController.navigateRoot(['/tabs']);

          }
          else{
            this.presenters.presentErrorAlert();
            //this.navController.navigateRoot(['/tabs']);
            this.navController.navigateRoot(['/tabs']);

          }
          console.log('exito');
          }, (error: any)=> {
          console.log('error', error);
          this.presenters.quitLoading();
          this.presenters.presentErrorAlert();
          //this.navController.navigateRoot(['/tabs']);
          this.navController.navigateRoot(['/tabs']);

          }
      );
    }
  }
  seepass(pos:number){    
    if(this.passtype[pos]=='password')
    {
      this.passtype[pos]='text';
      this.passicon[pos]='eye-off';
    }
    else{
      this.passtype[pos]='password';
      this.passicon[pos]='eye';
    }
  }
  back(){
    this.navController.navigateRoot(['/tabs']);
  }
}
