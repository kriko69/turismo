import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ReadentService } from 'src/service/empresa/readent/readent.service';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Act } from 'src/models/act/act';
import { DeleteentService } from 'src/service/empresa/deleteent/deleteent.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-listactivities',
  templateUrl: './listactivities.page.html',
  styleUrls: ['./listactivities.page.scss'],
})
export class ListactivitiesPage implements OnInit {
  activities:Act[];
  busqueda:any='';
  activities2:Act[];
  titleex="Exito.";
  messageex="Actividad Borrada con Exito.";
  linkimage:string;
  buscar:boolean=false;

  constructor(private router:Router,private readENT:ReadentService,
    public presenters:PresentersService,
    public parameters:ParametersService,private delENT:DeleteentService
    ,public navController:NavController) { }

  ngOnInit() {
    this.ListActivities();
    this.linkimage=this.parameters.ImagenesactividadUrl;
  }
  ListActivities(){
    let info:any;
    this.presenters.presentLoading("Espere por favor...");
    this.readENT.ListAct(this.parameters.getToken()).subscribe(
      data => {
        info= Object.assign(data);
        console.log(info);
        this.presenters.quitLoading();
        if(info.estado=="exito"){
          this.activities=info.data;
          this.activities2=this.activities;
          console.log("exito",this.activities);
        }
        else{
          this.presenters.presentErrorAlert();
        }
        }, (error: any)=> {
          this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentErrorAlert();
        }
    );
  } 
  CreateActivity(){
    this.navController.navigateRoot(['/createactivity']);
  }
  Reservations(act:Act){
    this.parameters.setAct(act);
    this.navController.navigateRoot(['/reservations']);

  }
  deleteAct(act:Act){
    let info:any;
    this.presenters.presentLoading("Espere por favor...");
    this.delENT.DeleteAct(act.actividad_id,this.parameters.getToken()).subscribe(
      data => {
        console.log('data', data);
        info= Object.assign(data);
        this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentErrorAlert();
        }
        else{
          this.presenters.presentAlert(this.titleex,this.messageex);
          this.ListActivities();
        }
        }, (error: any)=> {
        console.log('error', error);
        this.presenters.quitLoading();
        this.presenters.presentErrorAlert();
        }
    );
}
  mofidyAct(act:Act){
    this.parameters.setAct(act);
    this.navController.navigateRoot(['/modifyactivity']);

  }
  setFilteredItems() {
    console.log("Activities",this.activities);
    
    this.activities = this.filterItems(this.busqueda);
  }
  filterItems(searchTerm){
      return this.activities2.filter((item) => {
          return (item.nombre_esp).toLowerCase().includes(searchTerm.toLowerCase());
      });
  }
  search(){
    this.buscar=!this.buscar;
    if(!this.buscar){
      this.busqueda="";
      this.setFilteredItems();
    }
  }
}
