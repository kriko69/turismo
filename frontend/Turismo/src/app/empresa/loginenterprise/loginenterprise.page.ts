import { Component, OnInit } from '@angular/core';
import { ReadentService } from 'src/service/empresa/readent/readent.service';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Ent } from 'src/models/ent/ent';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { Languaje } from 'src/models/languaje/languaje';
import { EspanolService } from 'src/service/comun/lenguaje/espanol/espanol.service';

@Component({
  selector: 'app-loginenterprise',
  templateUrl: './loginenterprise.page.html',
  styleUrls: ['./loginenterprise.page.scss'],
})
export class LoginenterprisePage implements OnInit {
  login=new Ent();
  title="Empresa no Existe";
  message="Verifique los datos ingresados e intente de nuevo por favor.";
  passtype="password";
  passicon="eye";

  constructor(private readENT:ReadentService,
    public presenters:PresentersService,
    public parameters:ParametersService,private storage: Storage
    ,public navController:NavController, public espano:EspanolService) {

  }

  ngOnInit() {
  }
  SignIn(){
    let info:any;
    this.presenters.presentLoading("Espere por favor...");
    this.readENT.SigInEnt(this.login).subscribe(
      data => {
        info= Object.assign(data);
        console.log('exito',info);
        this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentAlert(this.title,this.message);
        }
        else{
          this.saveToken(info["Token-Refresh"],info["Token-Verificacion"]);
         this.navController.navigateRoot(['/tabs']);
        }
        }, (error: any)=> {
          this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentErrorAlert();
        }
    );
  } 
  seepass(){    
    if(this.passtype=='password')
    {
      this.passtype='text';
      this.passicon='eye-off';
    }
    else{
      this.passtype='password';
      this.passicon='eye';
    }
  }
  saveToken(tokenref:string,tokenver:string){
    this.parameters.setToken(tokenver);
    this.parameters.setTokenref(tokenref);
        this.storage.set('TokenRef', tokenref);
        this.storage.set('TokenVer', tokenver);
        this.storage.set('Usuario', "Enterprise");
  }
  restorepass(){
    let languaje=new Languaje();
    languaje=this.espano.espanol();
    this.parameters.setLabels(languaje);
    this.parameters.userType="E";
    this.navController.navigateRoot(['/restorepassword']);
  }
}
