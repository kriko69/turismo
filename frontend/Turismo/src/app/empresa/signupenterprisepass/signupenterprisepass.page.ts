import { Component, OnInit } from '@angular/core';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { LoadingController, NavController } from '@ionic/angular';
import { CreateentService } from 'src/service/empresa/createent/createent.service';
import { Ent } from 'src/models/ent/ent';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-signupenterprisepass',
  templateUrl: './signupenterprisepass.page.html',
  styleUrls: ['./signupenterprisepass.page.scss'],
})
export class SignupenterprisepassPage implements OnInit {

  ent=new Ent();
  titleerr="Passwords no coinciden.";
  messageerr="Verifique los datos ingresados e intente de nuevo por favor.";
  titleex="Exito.";
  messageex="Solicitud enviada con Exito.";

  passtype:string[]=["password","password"];
  passicon:string[]=["eye","eye"];  
  
  fotoUsuario:string;
  base64Image: string='';

  constructor(public camera:Camera,public parameters:ParametersService,
    public presenters:PresentersService,private createENT:CreateentService
    ,public navController:NavController) { }

  ngOnInit() {
    this.ent=this.parameters.getEnt();
    this.fotoUsuario=this.parameters.defaultimageent;
  }
  SignUp(){
    if(this.ent.pass1!=this.ent.pass2){
      this.presenters.presentAlert(this.titleerr,this.messageerr);
    }
    else{
      if(this.fotoUsuario!=this.parameters.defaultimageuser){
      this.ent.imagen=this.base64Image;
    }
    else{
      this.ent.imagen="";
    }
      let info:any;
      this.presenters.presentLoading("Espere por favor...");
      this.createENT.CreateEnt(this.ent).subscribe(
        data => {
          console.log('data', data);
          info= Object.assign(data);
          this.presenters.quitLoading();
          if(info['estado']=='exito')
          {
            this.presenters.presentAlert(this.titleex,this.messageex);
            this.navController.navigateRoot(['/loginenterprise']);
          }
          else{
            this.presenters.presentErrorAlert();
          }
          }, (error: any)=> {
          console.log('error', error);
          this.presenters.presentErrorAlert();
          this.presenters.quitLoading();
          }
      );
    }
  }
  seepass(pos:number){    
    if(this.passtype[pos]=='password')
    {
      this.passtype[pos]='text';
      this.passicon[pos]='eye-off';
    }
    else{
      this.passtype[pos]='password';
      this.passicon[pos]='eye';
    }
  }
  openCamara(){
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation:true
    }
    
    this.camera.getPicture(options).then((imageData)=> {
      this.base64Image = 'data:image/jpeg;base64,'+ imageData;
      this.fotoUsuario = this.base64Image;
    },(err)=>{
      console.log('Error en la foto tomada')
    });
  }
  openGallery(){
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation:true
    }

    this.camera.getPicture(options).then((imageData)=> {
    this.base64Image = 'data:image/jpeg;base64,'+ imageData;
    this.fotoUsuario = this.base64Image;
  },(err)=>{
    console.log('Error en la foto tomada')
  });
  }
}