import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SignupenterprisepassPage } from './signupenterprisepass.page';

const routes: Routes = [
  {
    path: '',
    component: SignupenterprisepassPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SignupenterprisepassPage]
})
export class SignupenterprisepassPageModule {}
