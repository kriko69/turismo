import { Component, OnInit } from '@angular/core';
import { Ent } from 'src/models/ent/ent';
import { NavController } from '@ionic/angular';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';

@Component({
  selector: 'app-signupenterprisecontact',
  templateUrl: './signupenterprisecontact.page.html',
  styleUrls: ['./signupenterprisecontact.page.scss'],
})
export class SignupenterprisecontactPage implements OnInit {

  ent=new Ent();
  constructor(private parameters:ParametersService,public navController:NavController) { }

  ngOnInit() {
    this.ent=this.parameters.getEnt();
  }

  gonext(){
    this.parameters.setEnt(this.ent);
    this.navController.navigateRoot(['/signupenterprisepass']);
  }
}
