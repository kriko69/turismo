import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SignupenterprisecontactPage } from './signupenterprisecontact.page';

const routes: Routes = [
  {
    path: '',
    component: SignupenterprisecontactPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SignupenterprisecontactPage]
})
export class SignupenterprisecontactPageModule {}
