import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { CreateentService } from 'src/service/empresa/createent/createent.service';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Act } from 'src/models/act/act';
import { NavController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-createactivity',
  templateUrl: './createactivity.page.html',
  styleUrls: ['./createactivity.page.scss'],
})
export class CreateactivityPage implements OnInit {
  activity=new Act();
  titleex="Exito.";
  messageex="Actividad creada con Exito.";
  
  titleer="Error.";
  messageer="Si la opcion de repetir esta activa, seleccione al menos un dia.";

  fotoUsuario:string;
  base64Image: string='';
  val: boolean=false;

  days = [
    { val: 'L', isChecked: false, label:'Lunes' },
    { val: 'Ma', isChecked: false, label:'Martes' },
    { val: 'Mi', isChecked: false, label:'Miercoles' },
    { val: 'J', isChecked: false, label:'Jueves' },
    { val: 'V', isChecked: false, label:'Viernes' },
    { val: 'S', isChecked: false, label:'Sabado' },
    { val: 'D', isChecked: false, label:'Domingo' }
  ];

  constructor(
    private camera:Camera,private router:Router,public presenters:PresentersService,
              public createENT:CreateentService,public parameters:ParametersService,
              public navController:NavController) { }

  ngOnInit() {
    console.log(this.parameters.getToken());
    this.fotoUsuario=this.parameters.defaultimageact;
    
  }
  submit(){
    console.log("Actividad",this.activity);
    if(this.validateData()){
      this.getData();
      let info:any;
      this.presenters.presentLoading("Espere por favor...");
      this.createENT.CreateAct(this.activity,this.parameters.getToken()).subscribe(
        data => {
          console.log('data', data);
          info= Object.assign(data);
          this.presenters.quitLoading();
          console.log('exito',info);
          if(info.estado=="exito"){
            this.presenters.presentAlert(this.titleex,this.messageex);
            //this.navController.navigateRoot(['/tabs']);
          this.navController.navigateRoot(['/tabs']);
          }else{
            this.presenters.presentErrorAlert();
            this.navController.navigateRoot(['/tabs']);            
          }
          }, (error: any)=> {
          console.log('error', error);
          this.presenters.quitLoading();
          this.presenters.presentErrorAlert();
          this.navController.navigateRoot(['/tabs']);
          }
      );
    }
    else{
      this.presenters.presentAlert(this.titleer,this.messageer);
      console.log("fracaso");
    }
  }
  back(){
    this.navController.navigateRoot(['/tabs']); 
  }
  openCamara(){
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation:true
    }
    
    this.camera.getPicture(options).then((imageData)=> {
      this.base64Image = 'data:image/jpeg;base64,'+ imageData;
      this.fotoUsuario = this.base64Image;
      this.val=true;
    },(err)=>{
      console.log('Error en la foto tomada')
    });
  }
  openGallery(){
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation:true
    }

    this.camera.getPicture(options).then((imageData)=> {
    this.base64Image = 'data:image/jpeg;base64,'+ imageData;
    this.fotoUsuario = this.base64Image;
    this.val=true;
  },(err)=>{
    console.log('Error en la foto tomada')
  });
  }
  getDays(){
    this.activity.dias="";
    let days="";
    for(let i=0;i<this.days.length;i++){
      if(this.days[i].isChecked){
      if(days!=""){
        days=days+"-"+this.days[i].val;
      }
      else
      days=days+this.days[i].val;
      }
    }
    this.activity.dias=days;        
  }
  validateData(){
    this.getDays();
    if(this.activity.se_repite && this.activity.dias!="")
    {
      return true;
    }
    if(!this.activity.se_repite && this.activity.dias=="")
    {
      this.activity.dias="N/A";
      return true;
    }
    if(!this.activity.se_repite && this.activity.dias!="")
    {
      this.activity.dias="N/A";
      return true;
    }
      return false;
  }
  getData(){
    if(this.fotoUsuario!=this.parameters.defaultimageuser){
      this.activity.imagen=this.base64Image;
    }
    else{
      this.activity.imagen="";
    }
    this.activity.fecha_inicio=this.activity.fecha_inicio.split("T")[0];
    this.activity.horario=this.activity.horario.split("T")[1].split(".")[0];
  }
}
