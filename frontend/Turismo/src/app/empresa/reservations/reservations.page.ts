import { Component, OnInit } from '@angular/core';
import { Reserv } from 'src/models/reserv/reserv';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { ReadusService } from 'src/service/visitante/readus/readus.service';
import { Act } from 'src/models/act/act';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.page.html',
  styleUrls: ['./reservations.page.scss'],
})
export class ReservationsPage implements OnInit {
  reserv:Reserv[];
  activity= new Act;
  fotoUsuario:string;
  linkimage:string;

  constructor(private readUs:ReadusService,public presenters:PresentersService,
    public parameters:ParametersService,public navController:NavController) { }

  ngOnInit() {
    this.linkimage=this.parameters.ImagenesusuarioUrl;
    this.ListReserv();
  }
  ListReserv(){
    this.activity=this.parameters.getAct();
    let info:any;
    this.presenters.presentLoading("Espere por favor...");
    this.readUs.Reservations(this.parameters.getToken(),this.activity.actividad_id).subscribe(
      data => {
        info= Object.assign(data);
        console.log(info);
        this.presenters.quitLoading();
        if(info.estado=="exito"){
          this.reserv=info.data;
          console.log("exito",this.reserv);
        }
        else{
          this.presenters.presentErrorAlert();
          this.navController.navigateRoot(['/tabs']);
        }
        }, (error: any)=> {
          this.presenters.quitLoading();
        console.log('error', error);
        this.presenters.presentErrorAlert();
        }
    );
  }
  back(){
    this.navController.navigateRoot(['/tabs']);
  }
}
