import { Component, OnInit } from '@angular/core';
import { Ent } from 'src/models/ent/ent';
import { ReadentService } from 'src/service/empresa/readent/readent.service';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { ModifyentService } from 'src/service/empresa/modifyent/modifyent.service';
import { NavController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-profileent',
  templateUrl: './profileent.page.html',
  styleUrls: ['./profileent.page.scss'],
})
export class ProfileentPage implements OnInit {
  ent=new Ent();
  edit:boolean=false;
  titleex="Exito.";
  messageex="Empresa Editada con Exito.";
  titleerr="Passwords no coinciden.";
  messageerr="Verifique los datos ingresados e intente de nuevo por favor.";

  fotoUsuario:string;
  base64Image: string='';
  val: boolean=false;
  constructor(private camera: Camera,private readEnt:ReadentService,
    public presenters:PresentersService,public parameters:ParametersService,
    private mdEnt:ModifyentService,public navController:NavController) { }

  ngOnInit() {
    this.getEnt();
  }
  Edit(){
    this.edit=true;
  }
  getEnt(){
    console.log("yes");
    let info:any;
    this.presenters.presentLoading("Espere por favor...");
    this.readEnt.Profile(this.parameters.getToken()).subscribe(
      data => {
        info= Object.assign(data);
        console.log(info);
        this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentErrorAlert();
          this.navController.navigateRoot(['/tabs']);
        }
        else{
          console.log('info', info); 
          this.ent=info.data;
          this.fotoUsuario=this.parameters.ImagenesempresaUrl+this.ent.imagen;
        }
        }, (error: any)=> {
          console.log('error', error); 
          this.presenters.quitLoading();
          this.presenters.presentErrorAlert();
          this.navController.navigateRoot(['/tabs']);
        }
      );
  } 
  editProfile(){
    if(this.ent.pass1!=this.ent.pass2){
      this.presenters.presentAlert(this.titleerr,this.messageerr);
    }
    else{
      if(this.base64Image!=""){
        this.ent.imagen=this.base64Image;
      }
      else{
        this.ent.imagen="";
      }
    let info:any;
    this.presenters.presentLoading("Espere por favor...");
    this.mdEnt.ModifyProfile(this.parameters.getToken(),this.ent).subscribe(
      data => {
        info= Object.assign(data);
        console.log(info);
        this.presenters.quitLoading();
        if(info.estado=="error"){
          this.presenters.presentErrorAlert();
          this.navController.navigateRoot(['/tabs']);
        }
        else{
          console.log('info', info); 
          this.presenters.presentAlert(this.titleex,this.messageex);
          this.navController.navigateRoot(['/tabs']);
        }
        }, (error: any)=> {
          console.log('error', error); 
          this.presenters.quitLoading();
          this.presenters.presentErrorAlert();
          this.navController.navigateRoot(['/tabs']);
        }
      );
    }
  }
  back(){
    this.navController.navigateRoot(['/tabs']);
  }
  openCamara(){
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation:true
    }
    
    this.camera.getPicture(options).then((imageData)=> {
      this.base64Image = 'data:image/jpeg;base64,'+ imageData;
      this.fotoUsuario = this.base64Image;
      this.val=true;
    },(err)=>{
      console.log('Error en la foto tomada')
    });
  }
  openGallery(){
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation:true
    }

    this.camera.getPicture(options).then((imageData)=> {
    this.base64Image = 'data:image/jpeg;base64,'+ imageData;
    this.fotoUsuario = this.base64Image;
    this.val=true;
  },(err)=>{
    console.log('Error en la foto tomada')
  });
  }
}
