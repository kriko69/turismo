import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-options',
  templateUrl: './options.page.html',
  styleUrls: ['./options.page.scss'],
})
export class OptionsPage implements OnInit {

  constructor(private router:Router,private storage: Storage,
    public presenters:PresentersService,
    public parameters:ParametersService,public navController:NavController) { }

  ngOnInit() {
  }
  SignOut(){
    this.parameters.SignOut();
  }
  Profile(){
    this.navController.navigateRoot(['/profileent']);
  }
  Pass(){
    this.navController.navigateRoot(['/changepassent']);
  }
}
