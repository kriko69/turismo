import { Component, OnInit } from '@angular/core';
import { Act } from 'src/models/act/act';
import { Router } from '@angular/router';
import { PresentersService } from 'src/service/comun/presenters/presenters.service';
import { CreateentService } from 'src/service/empresa/createent/createent.service';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { ModifyentService } from 'src/service/empresa/modifyent/modifyent.service';
import { NavController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-modifyactivity',
  templateUrl: './modifyactivity.page.html',
  styleUrls: ['./modifyactivity.page.scss'],
})
export class ModifyactivityPage implements OnInit {
  activity=new Act();
  titleex="Exito.";
  messageex="Actividad editada con Exito.";

  titleer="Error.";
  messageer="Si la opcion de repetir esta activa, seleccione al menos un dia.";
  
  fotoUsuario:string;
  base64Image: string='';
  val: boolean=false;
  days = [
    { val: 'L', isChecked: false, label:'Lunes' },
    { val: 'Ma', isChecked: false, label:'Martes' },
    { val: 'Mi', isChecked: false, label:'Miercoles' },
    { val: 'J', isChecked: false, label:'Jueves' },
    { val: 'V', isChecked: false, label:'Viernes' },
    { val: 'S', isChecked: false, label:'Sabado' },
    { val: 'D', isChecked: false, label:'Domingo' }
  ];

  constructor(private camera:Camera,private router:Router,public presenters:PresentersService,
              public modifyENT:ModifyentService,public parameters:ParametersService
              ,public navController:NavController) { 
                this.activity=parameters.getAct();
              }

  ngOnInit() {
   this.activity=this.parameters.getAct();
   this.fotoUsuario=this.parameters.ImagenesactividadUrl+this.activity.imagen;
   this.activity.se_repite=!this.activity.se_repite;
   if(this.activity.se_repite){
    let daysarray=this.activity.dias.split('-');
    for(let i=0;i<daysarray.length;i++){
      switch(daysarray[i]) { 
        case 'L': { 
           this.days[0].isChecked=true;
           break; 
        } 
        case 'Ma': { 
          this.days[1].isChecked=true;
           break; 
        } 
        case 'Mi': { 
          this.days[2].isChecked=true;
          break; 
        } 
        case 'J': { 
          this.days[3].isChecked=true;
          break; 
        } 
        case 'V': { 
          this.days[4].isChecked=true;
          break; 
        } 
        case 'S': { 
          this.days[5].isChecked=true;
          break; 
        }       
        case 'D': { 
          this.days[6].isChecked=true;
          break; 
        }       
        default: { 
           break; 
        } 
     } 
    }
    }
  }
  submit(){
    console.log("Actividad",this.activity);
    if(this.validateData()){
      if(this.activity.dias==null || this.activity.dias==""){
        this.activity.dias="N/A";
      }
      this.getData();
      let info:any;
      this.presenters.presentLoading("Espere por favor...");
      this.modifyENT.ModifyAct(this.activity,this.parameters.getToken()).subscribe(
        data => {
          console.log('data', data);
          info= Object.assign(data);
          this.presenters.quitLoading();
          if(info.estado=="exito"){
            console.log('exito',info);
            this.presenters.presentAlert(this.titleex,this.messageex);
            this.navController.navigateRoot(['/tabs']);
          }
          else{
            this.presenters.presentErrorAlert();
            this.navController.navigateRoot(['/tabs']);
          }
          }, (error: any)=> {
          console.log('error', error);
          this.presenters.quitLoading();
          this.presenters.presentErrorAlert();
          this.navController.navigateRoot(['/tabs']);
          }
      );
    }
    else{
      this.presenters.presentAlert(this.titleer,this.messageer);
      console.log("fracaso");
    }
  }
  back(){
    this.navController.navigateRoot(['/tabs']);
  }
  openCamara(){
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation:true
    }
    
    this.camera.getPicture(options).then((imageData)=> {
      this.base64Image = 'data:image/jpeg;base64,'+ imageData;
      this.fotoUsuario = this.base64Image;
      this.val=true;
    },(err)=>{
      console.log('Error en la foto tomada')
    });
  }
  openGallery(){
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation:true
    }

    this.camera.getPicture(options).then((imageData)=> {
    this.base64Image = 'data:image/jpeg;base64,'+ imageData;
    this.fotoUsuario = this.base64Image;
    this.val=true;
  },(err)=>{
    console.log('Error en la foto tomada')
  });
  }
  getDays(){
    this.activity.dias="";
    let days="";
    for(let i=0;i<this.days.length;i++){
      if(this.days[i].isChecked){
      if(days!=""){
        days=days+"-"+this.days[i].val;
      }
      else
      days=days+this.days[i].val;
      }
    }
    this.activity.dias=days;        
  }
  validateData(){
    this.getDays();
    if(this.activity.se_repite && this.activity.dias!="")
    {
      return true;
    }
    if(this.activity.se_repite==false && this.activity.dias=="")
    {
      return true;
    }
    if(this.activity.se_repite==false && this.activity.dias!="")
    {
      this.activity.dias="";
      return true;
    }
      return false;
  }
  getData(){
    if(this.fotoUsuario!=this.parameters.defaultimageuser){
      this.activity.imagen=this.base64Image;
    }
    else{
      this.activity.imagen="";
    }/*
    this.activity.fecha_inicio=this.activity.fecha_inicio.split("T")[0];
    this.activity.horario=this.activity.horario.split("T")[1].split(".")[0];*/
  }
}
