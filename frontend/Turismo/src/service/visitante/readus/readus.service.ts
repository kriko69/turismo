import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Http,Headers} from '@angular/http';
import {map} from 'rxjs/operators';
import { User } from 'src/models/user/user';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Compra } from 'src/models/compra/compra';

@Injectable({
  providedIn: 'root'
})
export class ReadusService {

  constructor(private http2: Http,public http:HttpClient,public parameter:ParametersService){}

  SigInUs(us:User){
    let data={
      "usuario":us.usuario,
      "password":us.pass1
    };

    let header : any = new HttpHeaders({'Content-Type': 'application/json'}),
    opsi   : any = JSON.stringify(data);
    console.log('opsi',opsi);

    return this.http.post(this.parameter.ServidorUrl+'login', opsi, header);
  }
  ListAct(/*token:string*/){
    //let header : any = new Headers();
    //header.append("Authorization",token);
    //return this.http2.get(this.parameter.ServidorUrl+'actividades',{ headers:header}).pipe(map((res=>res.json())));
    return this.http2.get(this.parameter.ServidorUrl+'actividades/sin/registro').pipe(map((res=>res.json())));
  }
  Profile(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.parameter.ServidorUrl+'perfil',{ headers:header}).pipe(map((res=>res.json())));
  }
  Reservations(token:string,id:number)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    
    return this.http2.get(this.parameter.ServidorUrl+'actividad/'+id+'/reservas',{ headers:header}).pipe(map((res=>res.json())));
  }
  VerifyBuy(compra:Compra,act:number)
  {
    let data={
      "fecha":compra.date,
      "cantidad_visitantes":compra.cantity
    };
    let header : any = new HttpHeaders({'Content-Type': 'application/json'}),
    opsi   : any = JSON.stringify(data);
    console.log('opsi',opsi);
    return this.http.post(this.parameter.ServidorUrl+'actividad/'+act+'/verificar/reserva', opsi, header);
  }
  ListBuys(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    
    return this.http2.get(this.parameter.ServidorUrl+'usuario/reservas',{ headers:header}).pipe(map((res=>res.json())));
  }
  EntProfile(token:string,id:number)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    
    return this.http2.get(this.parameter.ServidorUrl+'get_empresa/'+id,{ headers:header}).pipe(map((res=>res.json())));
  }
  ListComments(id:number)
  {
    return this.http2.get(this.parameter.ServidorUrl+'actividad/'+id+'/comentarios').pipe(map((res=>res.json())));
  }
  ListMycomentAct(token:string,id:number)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    
    return this.http2.get(this.parameter.ServidorUrl+'usuario/reservas/'+id,{ headers:header}).pipe(map((res=>res.json())));
  }
}
