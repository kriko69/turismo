import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Http,Headers} from '@angular/http';
import {map} from 'rxjs/operators';
import { User } from 'src/models/user/user';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Changepass } from 'src/models/changepass/changepass';

@Injectable({
  providedIn: 'root'
})
export class ModifyusService {

  constructor(private http2: Http,public http:HttpClient,public parameter:ParametersService) { }
  ModifyProfile(token:string,user:User)
  {
    let data={
      "nombre":user.nombre,
      "apellidos":user.apellidos,
      "usuario":user.usuario,
      "correo":user.correo,
      "fecha_nacimiento":user.fecha_nacimiento,
      "telefono":user.telefono,
      "imagen":user.imagen
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.parameter.ServidorUrl+'perfil',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }
  ModifyPass(token:string,pass:Changepass)
  {
    let data={
      "actual_password":pass.oldpass,
      "nueva_password":pass.newpass1
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.parameter.ServidorUrl+'perfil/password',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }
  ModifyLanguaje(token:string,idioma:string)
  {
    let data={
      "idioma":idioma
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.parameter.ServidorUrl+'perfil/idioma',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }
}
