import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from 'src/models/user/user';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Compra } from 'src/models/compra/compra';
import {Http,Headers} from '@angular/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CreateusService {

  constructor(public http:HttpClient,public parameter:ParametersService,
    public http2:Http) { }
  
  CreateUser(user:User){
    let data={
      "nombre":user.nombre,
      "apellidos":user.apellidos,
      "usuario":user.usuario,
      "correo":user.correo,
      "fecha_nacimiento":user.fecha_nacimiento,
      "telefono":user.telefono,
      "password":user.pass1,
      "imagen":user.imagen,
      "idioma":user.idioma
    };

    let header : any = new HttpHeaders({'Content-Type': 'application/json'}),
    opsi   : any = JSON.stringify(data);
    console.log('opsi',opsi);

    return this.http.post(this.parameter.ServidorUrl+'registrar', opsi, header);
  }
  CreateBuy(compra:Compra,act:number,token:string){
    let data={
      "cantidad_reservas":compra.cantity,
	    "precio_total":compra.price,
	    "fecha":compra.date
  };
  
  let header : any = new Headers();
  header.append("Authorization",token);
  header.append("Content-Type","application/json");

  let opsi   : any = JSON.stringify(data);
  console.log('opsi',opsi);

  return this.http2.post(this.parameter.ServidorUrl+'actividad/'+act+'/reservar',opsi,{ headers:header}).pipe(map((res=>res.json())));
}
  createCalification(token:string,id:number,stars:number,comment:string){
    let data={
      "comentario":comment,
	    "calificacion":stars
  };
  
  let header : any = new Headers();
  header.append("Authorization",token);
  header.append("Content-Type","application/json");

  let opsi   : any = JSON.stringify(data);
  console.log('opsi',opsi);

  return this.http2.post(this.parameter.ServidorUrl+'actividad/'+id+'/comentar-calificar',opsi,{ headers:header}).pipe(map((res=>res.json())));
}
GetCodePass(usuario:string){
  let data={
    "usuario":usuario
};

let header : any = new Headers();
header.append("Content-Type","application/json");

let opsi   : any = JSON.stringify(data);
console.log('opsi',opsi);

return this.http2.post(this.parameter.ServidorUrl+'obtener/codigo/usuario',opsi,{ headers:header}).pipe(map((res=>res.json())));
}
}
