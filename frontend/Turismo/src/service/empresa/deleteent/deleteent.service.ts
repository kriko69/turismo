import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';
import {map} from 'rxjs/operators';
import { Act } from 'src/models/act/act';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';

@Injectable({
  providedIn: 'root'
})
export class DeleteentService {

  constructor(public http2:Http,public parameter:ParametersService) { }

  DeleteAct(id:number,token:string){  
  let header : any = new Headers();
  header.append("Authorization",token);

  return this.http2.delete(this.parameter.ServidorUrl+'actividad/'+id,{ headers:header}).pipe(map((res=>res.json())));
}
}
