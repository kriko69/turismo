import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Http,Headers} from '@angular/http';
import {map} from 'rxjs/operators';
import { User } from 'src/models/user/user';
import { Ent } from 'src/models/ent/ent';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';

@Injectable({
  providedIn: 'root'
})
export class ReadentService {

  constructor(public http:HttpClient,public http2:Http,public parameter:ParametersService) { }

  SigInEnt(ent:Ent){
    let data={
      "correo":ent.correo,
      "password":ent.pass1
    };
    let header : any = new HttpHeaders({'Content-Type': 'application/json'}),
    opsi   : any = JSON.stringify(data);
    console.log('opsi',opsi);
    return this.http.post(this.parameter.ServidorUrl+'login_empresa', opsi, header);
  }

  ListAct(token:string){
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.parameter.ServidorUrl+'actividades_empresa',{ headers:header}).pipe(map((res=>res.json())));
  }

  ListRequest(token:string){
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.parameter.ServidorUrl+'get_empresas',{ headers:header}).pipe(map((res=>res.json())));
  }
  Profile(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.parameter.ServidorUrl+'get_empresa',{ headers:header}).pipe(map((res=>res.json())));
  }

  Imagen(token:string){
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.parameter.ServidorUrl+'imagen/empresa',{ headers:header}).pipe(map((res=>res.json())));
  }

  ReadBuzon(token:string){
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.parameter.ServidorUrl+'buzon',{ headers:header}).pipe(map((res=>res.json())));
  }

  VerifyPassCode(codigo:string){
    let data={
      "codigo":codigo
    };
  let header : any = new Headers();
  header.append("Content-Type","application/json");
  let opsi   : any = JSON.stringify(data);
  console.log('opsi',opsi);
  return this.http2.post(this.parameter.ServidorUrl+'verficar/codigo',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }
}