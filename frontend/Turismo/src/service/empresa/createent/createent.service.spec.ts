import { TestBed } from '@angular/core/testing';

import { CreateentService } from './createent.service';

import { HttpClientTestingModule,
  HttpTestingController } from '@angular/common/http/testing';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Observable,of } from 'rxjs';

describe('CreateentService', () => {

  let http: { post: jasmine.Spy};
  let parameter:ParametersService;

  let httpTestingController: HttpTestingController;
  let service:CreateentService;

  beforeEach(() => 
  {
    parameter = jasmine.createSpyObj('ParametersService', {
      ServidorUrl:"http://159.65.217.220/api/"
    });
    http = jasmine.createSpyObj('Http', ['post']);
    service = new CreateentService(<any> http,parameter);
/*

    TestBed.configureTestingModule({
      providers: [
        CreateentService
      ],
      imports: [HttpClientTestingModule]
    });
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(CreateentService);*/
  });
/*
  afterEach(() => {
    httpTestingController.verify();
  });*/

  it('GetCodePass', () => {
    const expectedanswer = { estado: "error", data: null };
  
    http.post.and.returnValue(of(expectedanswer));
  
    /*service.GetCodePass("roma.molli.agui@gmail.com").subscribe(
      answer => expect(answer).toEqual(expectedanswer),
      fail
    );*/
    expect(http.post.calls.count()).toBe(0);
  });
});
