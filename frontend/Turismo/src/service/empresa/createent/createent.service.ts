import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';
import {map} from 'rxjs/operators';
import { Act } from 'src/models/act/act';
import { Ent } from 'src/models/ent/ent';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';

@Injectable({
  providedIn: 'root'
})
export class CreateentService {

  constructor(public http2:Http,public parameter:ParametersService) { }

  CreateEnt(ent:Ent){
    let data={
      "nombre":ent.nombre,
      "pais":ent.pais,
      "ciudad":ent.ciudad,
      "descripcion":ent.descripcion,
      "telefono":ent.telefono,
      "correo":ent.correo,
      "password":ent.pass1,
      "imagen":ent.imagen
    };

    let header : any = new Headers();
    header.append("Content-Type","application/json");

    let opsi   : any = JSON.stringify(data);
    console.log('opsi',opsi);


    return this.http2.post(this.parameter.ServidorUrl+'registrar_empresa',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }
  CreateAct(act:Act,token:string){
      let data={
        "nombre_esp":act.nombre_esp,
        "nombre_eng":act.nombre_eng,
        "ciudad":act.ciudad,
        "pais":act.pais,
        "horario":act.horario,
        "precio":act.precio,
        "descripcion_esp":act.descripcion_esp,
        "descripcion_eng":act.descripcion_eng,
        "imagen":act.imagen,
        "cantidad_visitantes":act.cantidad_visitantes,
        "dias":act.dias,
        "fecha_inicio":act.fecha_inicio,
        "lugar_encuentro":act.lugar_encuentro,
        "se_repite":!act.se_repite
    };
    
    let header : any = new Headers();
    header.append("Authorization",token);
    header.append("Content-Type","application/json");

    let opsi   : any = JSON.stringify(data);
    console.log('opsi',opsi);

    return this.http2.post(this.parameter.ServidorUrl+'actividad',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }

  GetCodePass(correo:string){
    let data={
      "correo":correo
  };
  
  let header : any = new Headers();
  header.append("Content-Type","application/json");

  let opsi   : any = JSON.stringify(data);
  console.log('opsi',opsi);

  return this.http2.post(this.parameter.ServidorUrl+'obtener/codigo/empresa',opsi,{ headers:header}).pipe(map((res=>res.json())));
}
}
