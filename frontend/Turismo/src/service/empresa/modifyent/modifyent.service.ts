import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';
import {map} from 'rxjs/operators';
import { Act } from 'src/models/act/act';
import { Ent } from 'src/models/ent/ent';
import { ParametersService } from 'src/service/comun/parameters/parameters.service';
import { Changepass } from 'src/models/changepass/changepass';

@Injectable({
  providedIn: 'root'
})
export class ModifyentService {
  
  constructor(public http2:Http,public parameter:ParametersService) { }

 Afiliation(answer:number,id:number,token:string){
 
  
  let header : any = new Headers();
  header.append("Authorization",token);
  console.log('token',token);let opsi   : any;
  if(answer==1)
      return this.http2.post(this.parameter.ServidorUrl+'empresa/'+id+'/confirmarSolicitud',opsi,{ headers:header}).pipe(map((res=>res.json())));
  else
      return this.http2.post(this.parameter.ServidorUrl+'empresa/'+id+'/rechazarSolicitud',opsi,{ headers:header}).pipe(map((res=>res.json())));
}
ModifyAct(act:Act,token:string){
  let data={
    "nombre_esp":act.nombre_esp,
    "nombre_eng":act.nombre_eng,
    "ciudad":act.ciudad,
    "pais":act.pais,
    "horario":act.horario,
    "precio":act.precio,
    "descripcion_esp":act.descripcion_esp,
    "descripcion_eng":act.descripcion_eng,
    "imagen":act.imagen,
    "cantidad_visitantes":act.cantidad_visitantes,
    "dias":act.dias,
    "fecha_inicio":act.fecha_inicio,
    "lugar_encuentro":act.lugar_encuentro,
    "se_repite":0
};
if(act.se_repite){
  data.se_repite=0;
}
else{
  data.se_repite=1;
}
let header : any = new Headers();
header.append("Authorization",token);
header.append("Content-Type","application/json");

let opsi   : any = JSON.stringify(data);
console.log('opsi',opsi);

return this.http2.put(this.parameter.ServidorUrl+'actividad/'+act.actividad_id,opsi,{ headers:header}).pipe(map((res=>res.json())));
}
ModifyProfile(token:string,ent:Ent)
  {
    let data={
      "nombre":ent.nombre,
      "pais":ent.pais,
      "ciudad":ent.ciudad,
      "descripcion":ent.descripcion,
      "telefono":ent.telefono,
      "correo":ent.correo,
      "imagen":ent.imagen
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.parameter.ServidorUrl+'actualizar_empresa',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }
ModifyPass(token:string,pass:Changepass)
  {
    let data={
      "oldpass":pass.oldpass,
      "newpass":pass.newpass1
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.parameter.ServidorUrl+'actualizar/password',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }
  RestorePass(id:number,tipo:string,pass:string)
  {
    let data={
      "id":id,
	    "tipo":tipo,
	    "password":pass
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Content-Type","application/json");
    console.log('opsi',opsi);
    return this.http2.post(this.parameter.ServidorUrl+'reset/password',opsi,{ headers:header}).pipe(map((res=>res.json())));
  }
}
