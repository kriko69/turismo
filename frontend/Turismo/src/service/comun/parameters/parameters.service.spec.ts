import { TestBed } from '@angular/core/testing';

import { ParametersService } from './parameters.service';
import { NavController } from '@ionic/angular';
import { Act } from 'src/models/act/act';
import { arch } from 'os';
import { Ent } from 'src/models/ent/ent';
import { User } from 'src/models/user/user';

describe('ParametersService', () => {
  let storageMock:Storage;
  let navCtrlMock:NavController;

  beforeEach(() => 
  {
    storageMock = jasmine.createSpyObj('Storage', {
      set: () => true,
      get: () => new Promise<any>((resolve, reject) => resolve('ABC123'))
    });
    navCtrlMock = jasmine.createSpyObj('NavController', {
      navigateRoot: () => true
    });
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        ParametersService,
        { provide: Storage, useValue: storageMock },
        { provide: NavController, useValue: navCtrlMock }
      ]
    });
  });

  it('Probar set, get ActId', () => {
    const service = new ParametersService(null,navCtrlMock);
    service.setActId(1);
    expect(service.getActId()).toEqual(1);
  });

  it('Probar set, get Act', () => {
    const service = new ParametersService(null,navCtrlMock);
    let act=new Act();
    act.actividad_id=1;
    act.ciudad="La Paz";
    act.empresa_id=2;
    service.setAct(act);
    expect(service.getAct()).toEqual(act);
  });

  it('Probar set, get User', () => {
    const service = new ParametersService(null,navCtrlMock);
    let user=new User();
    user.usuario_id=2;
    user.nombre="Test 1";
    user.apellidos="Test 2";
    service.setUser(user);
    expect(service.getUser()).toEqual(user);
  });
});
