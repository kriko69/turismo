import { Injectable } from '@angular/core';
import { User } from 'src/models/user/user';
import { Ent } from 'src/models/ent/ent';
import { Act } from 'src/models/act/act';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Languaje } from 'src/models/languaje/languaje';
import { Userbuys } from 'src/models/userbuys/userbuys';

@Injectable({
  providedIn: 'root'
})
export class ParametersService {
  public userType:string;
  public genericId:number;
  public paginaAnt:string;
  public empresa_id:number;
  public actividad_id:number;
  public buy:Userbuys;
  public rol:string;
  public tokenref:string;
  public token:string;
  public user:User;
  public ent:Ent;
  public act:Act;
  public idioma:string;
  public labels:Languaje;
  public ServidorUrl='http://167.172.238.126/api/';
  public ImagenesempresaUrl='http://167.172.238.126/imagenes/empresas/';
  public ImagenesusuarioUrl='http://167.172.238.126/imagenes/usuarios/';
  public ImagenesactividadUrl='http://167.172.238.126/imagenes/actividades/';
  public defaultimageuser='http://167.172.238.126/imagenes/usuarios/defaultuser.png';
  public defaultimageent='http://167.172.238.126/imagenes/empresas/defaultent.png';
  public defaultimageact='http://167.172.238.126/imagenes/actividades/defaultact.png';

  constructor(private storage: Storage,public navController:NavController) {
    this.rol="";
    this.token=""; 
    this.tokenref="";    
    this.user=new User();
    this.ent=new Ent();
    this.act= new Act();
    this.labels=new Languaje();
    this.idioma="";
    this.buy=new Userbuys();
    }
    public getPaginaAnt(){
      return this.paginaAnt;
    }
    public setPaginaAnt(npage:string){
      this.paginaAnt=npage;
    }
    public getActId(){
      return this.actividad_id;
    }
    public setActId(nId:number){
      this.actividad_id=nId;
    }
    public getEmpId(){
      return this.empresa_id;
    }
    public setEmpId(nId:number){
      this.empresa_id=nId;
    }
    public getBuy(){
      return this.buy;
    }
    public setBuy(nbuy:Userbuys){
      this.buy=nbuy;
    }
    public setIdioma(idioma:string){
      this.storage.set('idioma', idioma);
      this.idioma=idioma;
    }
    public getIdioma(){
      this.storage.get('idioma').then((idioma) => {
      //  console.log("idioma",idioma);
         this.idioma=idioma;
      });
      return this.idioma;
    }
    public setLabels(languaje:Languaje){
      this.storage.set('labels', languaje);
      this.labels=languaje;
    }
    public getLabels(){
      this.storage.get('labels').then((labels) => {
      //  console.log("labels",labels);
         this.labels=labels;
      });
      return this.labels;
    }
   public setToken(intoken:string){
    this.storage.set('TokenVer', intoken);
    this.token=intoken;
    }
   public getToken(){
    this.storage.get('TokenVer').then((tokenver) => {
    //  console.log("tok",tokenver);
      
        this.token=tokenver;
    });
    return this.token;
    }
    public setTokenref(intoken:string){
      this.storage.set('TokenRef', intoken);
      this.tokenref=intoken;
    }
   public getTokenref(){
    this.storage.get('TokenRef').then((tokenref) => {
        this.tokenref=tokenref;
    });
    return this.tokenref;
    }
    public setUser(inuser:User){
      this.user=inuser;
    }
   public getUser(){
    return this.user;
    }

    public setEnt(inent:Ent){
      this.ent=inent;
    }
   public getEnt(){
    return this.ent;
    }

    public setAct(inact:Act){
      this.act=inact;
    }
   public getAct(){
    return this.act;
    }
    public setRol(rol:string){
      this.storage.set('Rol',rol);
      this.rol=rol;
    }
    public getRol(){
      this.storage.get('Rol').then((rol) => {
        this.rol=rol;
      });
      return this.rol;    
    }
    public SignOut(){
      this.setToken("");
      this.setTokenref("");
      this.setRol("");
      this.storage.set('TokenRef', null);
      this.storage.set('TokenVer', null);
      this.storage.set('Rol', null);
      this.navController.navigateRoot(['/login']);
    }
}
