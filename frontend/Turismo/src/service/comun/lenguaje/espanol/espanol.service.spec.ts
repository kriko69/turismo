import { TestBed, inject } from '@angular/core/testing';

import { EspanolService } from './espanol.service';
import { ParametersService } from '../../parameters/parameters.service';
import { Languaje } from 'src/models/languaje/languaje';

describe('EspanolService', () => {
  let parameter:ParametersService;

  beforeEach(() => 
  {
    parameter = jasmine.createSpyObj('ParametersService', {
      setLabels: () => true
    });
    TestBed.configureTestingModule({
      providers: [
        EspanolService,
        { provide: ParametersService, useValue: parameter }
      ]
    });
  });

  it('Probar funcion cargar labels en espanol', () => {
    const service = new EspanolService(parameter);
    let labes= new Languaje();
    labes=service.espanol();
    expect(labes.espere).toEqual("Espere por favor...");
    expect(labes.actividades).toEqual("Actividades");
    expect(labes.nuevaEmp).toEqual("Registrar Empresa");
    expect(labes.perfil).toEqual("Perfil");
    expect(labes.noCompras).toEqual("Usted no tiene Compras.");
    expect(labes.buscarCompra).toEqual("Buscar Compra...");
  });

  it('Probar funcion cargar labels en ingles', () => {
    const service = new EspanolService(parameter);
    let labes= new Languaje();
    labes=service.ingles();
    expect(labes.espere).toEqual("Please wait...");
    expect(labes.actividades).toEqual("Activities");
    expect(labes.nuevaEmp).toEqual("Register Enterprise");
    expect(labes.perfil).toEqual("Profile");
    expect(labes.noCompras).toEqual("You have no purchases.");
    expect(labes.buscarCompra).toEqual("Search Buy...");
  });

});
