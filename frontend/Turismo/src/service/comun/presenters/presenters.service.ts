import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class PresentersService {

  loading:any;
  constructor(public alerta:AlertController,
    private loadingCtrl:LoadingController) { }

  async presentErrorAlert() {
    const alert = await this.alerta.create({
      header: 'Error',
      message: 'Ocurrio un error. Intente de nuevo por favor.',
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentAlert(title:string,message:string) {
    const alert = await this.alerta.create({
      header: title,
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentLoading(mensaje:string) {
    this.loading = await this.loadingCtrl.create({
      message: mensaje
    });
    return await this.loading.present();
  }

  async quitLoading() {
    return await this.loading.dismiss();
  }
}
