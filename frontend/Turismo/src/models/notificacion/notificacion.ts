export class Notificacion {

    tipo : string;
    mensaje : string;
    actividad_id : number;
    nombre_esp : string;
    usuario : string;
    calificacion : number;
    comentario : string;
    fecha : string;
    imagen : string;
    cantidad_reservas : number;
    precio_total : number;

    constructor(){
        this.tipo = "";
        this.mensaje = "";
        this.actividad_id = 0;
        this.nombre_esp = "";
        this.usuario = "";
        this.calificacion = 0;
        this.comentario = "";
        this.fecha = "";
        this.imagen = "";
        this.cantidad_reservas = 0;
        this.precio_total = 0;
    }

}
