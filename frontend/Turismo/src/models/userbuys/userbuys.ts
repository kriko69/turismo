export class Userbuys {
    nombre_esp:string;
    nombre_eng:string;
    cantidad_reservas:number;
    precio_total:number;
    fecha_actividad:string;
    imagen:string;
    
    calificacion_actividad:number;
    descripcion_eng:string;
    descripcion_esp:string;
    horario_actividad: string;
    lugar_encuentro:string;
    nombre_empresa: string;
    reserva_id:number;
    empresa_id:number;
    actividad_id:number;
    comentario:string;
    calificacion_usuario:number;
    comentario_usuario:string;
    constructor(){
        this.nombre_esp="";
        this.nombre_eng="";
        this.cantidad_reservas=0;
        this.precio_total=0;
        this.fecha_actividad="";
        this.imagen="";

        this.calificacion_actividad=0;
        this.descripcion_eng="";
        this.descripcion_esp="";
        this.horario_actividad="";
        this.lugar_encuentro="";
        this.nombre_empresa="";
        this.reserva_id=0;
        this.empresa_id=0;
        this.actividad_id=0;
        this.comentario="";
        this.calificacion_usuario=0;
        this.comentario_usuario="";
    }
}

