export class Reserv {
    nombre:string;
    apellidos:string;
    telefono:number;
    cantidad_reservas:number;
    precio_total:number;
    fecha:string;
    imagen:string;
    constructor(){
        this.nombre="";
        this.apellidos="";
        this.telefono=0;
        this.cantidad_reservas=0;
        this.precio_total=0;
        this.fecha="";
        this.imagen="";
    }
    
}
