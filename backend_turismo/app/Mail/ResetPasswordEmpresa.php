<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 04-11-19
 * Time: 05:52 PM
 */
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPasswordEmpresa extends Mailable
{
    use Queueable, SerializesModels;

    public $nombre,$codigo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject = 'Recuperacion de Contraseña.';
    public function __construct($nombre,$codigo)
    {
        $this->codigo=$codigo;
        $this->nombre=$nombre;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('correos.reset-password-empresa')->with('nombre',$this->nombre)->with('codigo',$this->codigo);
    }
}


