<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 04-11-19
 * Time: 05:20 PM
 */
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $usuario,$codigo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject = 'Recuperacion de Contraseña.';
    public function __construct($usuario,$codigo)
    {
        $this->codigo=$codigo;
        $this->usuario=$usuario;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('correos.reset-password')->with('usuario',$this->usuario)->with('codigo',$this->codigo);
    }
}

