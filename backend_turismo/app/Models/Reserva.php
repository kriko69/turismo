<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 16-09-19
 * Time: 06:32 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    protected $table = 'reservas';
    protected $primaryKey='reserva_id';
    protected $fillable = [
        'reserva_id','cantidad_reservas','precio_total','fecha','estado','usuario_id','actividad_id'
    ];

    protected $hidden = [

    ];


    public function usuario()
    {
        return $this->belongsTo(Usuario::class,'usuario_id');
    }

    public function actividad(){
        return $this->belongsTo(Actividad::class,'actividad_id');
    }
}
