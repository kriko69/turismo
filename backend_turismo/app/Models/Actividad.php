<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    protected $table = 'actividades';
    protected $primaryKey='actividad_id';
    protected $fillable = [
        'actividad_id','nombre_esp','nombre,eng','ciudad','pais','horario','precio','descripcion_esp','descripcion_eng',
        'imagen','empresa_id','cantidad_visitantes','lugar_encuentro','fecha_inicio','se_repite','dias','estado',
    ];

    public function empresa()
    {
        return $this->belongsTo(Empresa::class,'empresa_id');
    }

    public function reservas()
    {
        return $this->hasMany(Reserva::class,'actividad_id');
    }
    public function comentarios()
    {
        return $this->hasMany(Reserva::class,'actividad_id');
    }
    public function calificaciones()
    {
        return $this->hasMany(Reserva::class,'actividad_id');
    }
    public function cupos_dias()
    {
        return $this->hasMany(CupoDia::class,'actividad_id');
    }

}
