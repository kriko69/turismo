<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
    protected $table = 'calificaciones';
    protected $primaryKey='calificacion_id';
    protected $fillable = [
        'calificacion_id','calificacion','estado','create_at'
    ];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class,'usuario_id');
    }

    public function actividad(){
        return $this->belongsTo(Actividad::class,'actividad_id');
    }
}
