<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CupoDia extends Model
{
    protected $table = 'cupos_dias';
    protected $primaryKey='cupo_dia_id';
    protected $fillable = [
        'cupo_dia_id','actividad_id','cupos_disponibles','fecha_actividad','dia_actividad'
    ];

    public function actividad()
    {
        return $this->belongsTo(Actividad::class,'actividad_id');
    }


}
