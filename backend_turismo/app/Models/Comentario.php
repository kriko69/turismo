<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $table = 'comentarios';
    protected $primaryKey='comentario_id';
    protected $fillable = [
        'comentario_id','comentario','estado','create_at'
    ];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class,'usuario_id');
    }

    public function actividad(){
        return $this->belongsTo(Actividad::class,'actividad_id');
    }
}
