<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresas';
    protected $primaryKey='empresa_id';
    protected $fillable = [
        'empresa_id','nombre','pais','ciudad','descripcion','telefono','correo','password','imagen','idioma','afiliacion','estado'

    ];
}
