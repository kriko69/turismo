<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuarios';
    protected $primaryKey='usuario_id';
    protected $fillable = [
        'usuario_id','nombre','apellidos','usuario','correo','telefono','fecha_nacimiento','password','imagen','idioma','estado'
    ];

    protected $hidden = [
        'password',
    ];


    public function roles(){
        return $this->belongsToMany(Rol::class,'roles_usuarios','usuario_id','rol_id');
    }

    public function reservas(){
        return $this->hasMany(Reserva::class,'usuario_id');
    }

    public function comentarios(){
        return $this->hasMany(Reserva::class,'usuario_id');
    }

    public function calificaciones(){
        return $this->hasMany(Reserva::class,'usuario_id');
    }
}
