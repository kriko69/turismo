<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Image;
use Illuminate\Support\Facades\Storage;
use App\Models\Usuario;
use App\Models\Empresa;
use App\Models\Actividad;
use App\Helpers\JwtAuth;
use Intervention\Image\ImageManagerStatic as Image;
class ImagenController extends Controller
{
    public function index()
    {
        return view('image.index');
    }
    function subidafoto(Request $request)
    {

        $originalImage = $request->json("imagen");
        $image = str_replace('data:image/png;base64,', '', $originalImage);
        $image = str_replace('data:image/jpg;base64,', '', $originalImage);
        $image = str_replace(' ','+',$image);
        $thumbnailImage = Image::make($image);
        $originalPath = public_path().'/imagenes/usuarios/';
        $thumbnailImage->resize(200,200)->save($originalPath.'user.jpg');
        /*
        $image = $request->file('imagen');
        $nombre = $image->getClientOriginalName();
        Storage::disk('ftp')->putFileAs('/imagenes/usuarios',$image,$nombre);// EXITOOO*/
        $data = array(
            'estado'=>'exito',
            'descripcion'=>'respuesta generada con exito',
            'data' => 'dio'
    );
        return $data;
  }



 	function get_foto_usuario(Request $request)

    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        $id = $payload->sub;
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $user = Usuario::find($id);
            $nombre = $user->imagen;
            //Storage::disk('ftp')->get('/imagenes/Empresa/'.$nombre);
            $data = array(
                'estado'=>'exito',
                'descripcion'=>'respuesta generada con exito',
                'data' => '181.188.163.198/imagenes/usuarios/'.$nombre
        );
            return $data;
        }
        
    }

    
 	function get_foto_empresa(Request $request)
     {
         $token=$request->header('Authorization',null);
         $jwt = new JwtAuth();
         $payload=$jwt->verificarToken($token);
         $id = $payload->sub;
         if(!$payload)
         {
             $data=array(
                 'mensaje'=>'Token incorrecto'
             );
             return response()->json($data);
         }else{
             $emp = Empresa::find($id);
             $nombre = $emp->imagen;
             //Storage::disk('ftp')->get('/imagenes/Empresa/'.$nombre);
             $data = array(
                 'estado'=>'exito',
                 'descripcion'=>'respuesta generada con exito',
                 'data' => '181.188.163.198/imagenes/empresas/'.$nombre
         );
             return $data;
         }
         
     }
     
 	function get_foto_actividad(Request $request)
     {
         $token=$request->header('Authorization',null);
         $jwt = new JwtAuth();
         $payload=$jwt->verificarToken($token);
         $id = $payload->sub;
         if(!$payload)
         {
             $data=array(
                 'mensaje'=>'Token incorrecto'
             );
             return response()->json($data);
         }else{
             $act = Actividad::find($id);
             $nombre = $act->imagen;
             //Storage::disk('ftp')->get('/imagenes/Empresa/'.$nombre);
             $data = array(
                 'estado'=>'exito',
                 'descripcion'=>'respuesta generada con exito',
                 'data' => '181.188.163.198/imagenes/actividades/'.$nombre
         );
             return $data;
         }
         
     }
}
