<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Mail\MensajeRecibido;
use App\Mail\RechazarEmpresa;
use App\Models\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Bl\EmpresaBl;

class CorreoController extends Controller
{
    public function enviarCorreo(Request $request,$id)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        $id = (int) $id;
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'
            );
            return response()->json($data);
        }else{
            $empresa=Empresa::find($id);
            //return $empresa->correo;
            try{
                Mail::to($empresa->correo)->queue(new MensajeRecibido);
                $afiliacion = 1;
                $bl = new EmpresaBl();
                $data = $bl->actualizarAfiliacion($id,$afiliacion);
            }catch (\Exception $e)
            {
            }
            return response()->json($data);
        }

    }

    public function enviarCorreoRechazo(Request $request,$id)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        $id = (int) $id;
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'
            );
            return response()->json($data);
        }else{
            $empresa=Empresa::find($id);
            //return $empresa->correo;
            try
            {
                Mail::to($empresa->correo)->queue(new RechazarEmpresa);
                $afiliacion = 0;
                $bl = new EmpresaBl();
                $data = $bl->actualizarAfiliacion($id,$afiliacion);
            }catch (\Exception $e){
            }
            return response()->json($data);
        }

    }


}
