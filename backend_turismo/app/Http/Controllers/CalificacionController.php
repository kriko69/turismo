<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Http\Controllers\Bl\CalificacionBl;
use Illuminate\Http\Request;

class CalificacionController extends Controller
{
    public  function calificar(Request $request,$actividad_id)
    {
        $token=$request->header('Authorization',null);
        $calificacion=$request->json('calificacion',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{
            if(isset($payload->usuario)) //es token de usuario
            {
                $bl = new CalificacionBl();
                return $bl->calificar($payload->sub,$actividad_id,$calificacion);
            }
            else{
                $data=array(
                    'data'=>null,
                    'mensaje'=>'No puede calificar con un token de empresa',
                    "estado"=>'error'
                );
                return response()->json($data);
            }
        }
    }
}
