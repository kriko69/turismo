<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Http\Controllers\Bl\ComentarioBl;
use Illuminate\Http\Request;

class ComentarioController extends Controller
{
    public function comentarCalificar(Request $request,$actividad_id)
    {
        $token=$request->header('Authorization',null);
        $comentario=$request->json('comentario',null);
        $calificacion=$request->json('calificacion',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{
            if(isset($payload->usuario)) //es token de usuario
            {
                $bl = new ComentarioBl();
                return $bl->comentar($payload->sub,$actividad_id,$comentario,$calificacion);
            }
            else{
                $data=array(
                    'data'=>null,
                    'mensaje'=>'No puede comentar con un token de empresa',
                    "estado"=>'error'
                );
                return response()->json($data);
            }
        }
    }

    public function verComentarios(Request $request,$actividad_id)
    {
        $bl = new ComentarioBl();
        return $bl->verComentarios($actividad_id);
    }
}
