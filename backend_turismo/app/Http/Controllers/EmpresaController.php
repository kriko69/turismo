<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Bl\EmpresaBl;
use App\Helpers\JwtAuth;
use PharIo\Manifest\Email;

class EmpresaController extends Controller
{
    public function registrar_empresa(Request $request)
    {
        $nombre=$request->json("nombre");
        $pais=$request->json("pais");
        $ciudad=$request->json("ciudad");
        $descripcion=$request->json("descripcion");
        $telefono=$request->json("telefono");
        $correo=$request->json("correo");
        $password=$request->json("password");
        $imagen=$request->json("imagen");


        if(!is_null($nombre) && !is_null($pais) && !is_null($ciudad) && !is_null($descripcion) && !is_null($telefono) &&
        !is_null($correo) && !is_null($password) ){
            $bl = new EmpresaBl();
            $data=$bl->registro_empresa($nombre,$pais,$ciudad,$descripcion,$telefono,$correo,$password,$imagen);
        }
        else{
            $data=array(
                'estado'=>'fallo',
                'descripcion'=>'algun parametro en blanco',
                'data'=>null
            );
        }

        return response()->json($data,200);
    }

    public function login_empresa(Request $request )
    {
        $correo = $request->json("correo");
        $password = $request->json("password");
        
        $jwtAuth = new JwtAuth();

        if(!is_null($correo) && !is_null($password)  && isset($correo) && isset($password))
        {
            $bl = new EmpresaBl();
            $password=$password.'software-1';
            $password=hash('sha256',$password);
            $res = $bl->login_empresa($correo,$password);
            
            return $res;
        }else{
            $data=array(
                'estado'=>'fallo',
                'descripcion'=>'algun parametro en vacio',
                'data'=>null
            );

            return response()->json($data);
        }
    }

    public function get_empresas(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'estado'=>'fallo',
                'descripcion'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl = new EmpresaBl();
            $res = $bl->listarEmpresas();
            $data = array(
                'estado'=>'exito',
                'descripcion'=>'respuesta generada con exito',
                'data' =>$res
        );
            return $data;
        }
    }

    public function get_empresa(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        $id = $payload->sub;
        
        if(!$payload)
        {
            $data=array(
                'estado'=>'fallo',
                'descripcion'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl = new EmpresaBl();
            $res = $bl->listarEmpresa($id);
            $data = array(
                'estado'=>'exito',
                'descripcion'=>'respuesta generada con exito',
                'data' =>$res
        );
            return $data;
        }
    }

    public function perfil_empresa(Request $request,$id)
    {
        $id = (int) $id;
        
        $bl = new EmpresaBl();
        $res = $bl->listarEmpresa($id);
        $data = array(
            'estado'=>'exito',
            'descripcion'=>'respuesta generada con exito',
            'data' =>$res
        );
        return $data;
        
    }

    public function put_empresa(Request $request)
    {
        $token = $request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload = $jwt->verificarToken($token);
        $id = $payload->sub;
        if(!$payload)
        {
            $data=array(
                'estado'=>'fallo',
                'descripcion'=>'Token incorrecto'
            );
            return response()->json($data);
        }
        else{
            $nombre=$request->json("nombre");
            $pais=$request->json("pais");
            $ciudad=$request->json("ciudad");
            $descripcion=$request->json("descripcion");
            $telefono=$request->json("telefono");
            $correo=$request->json("correo");
            $imagen=$request->json("imagen");
            
            if(!is_null($nombre) && !is_null($pais) && !is_null($ciudad) && !is_null($descripcion) && !is_null($telefono) &&
            !is_null($correo) ){
            $bl = new EmpresaBl();
            $res=$bl->put_empresa($nombre,$pais,$ciudad,$descripcion,$telefono,$correo,$imagen,$id);

            }
            return $res;
        }
    }

    public function delete_empresa(Request $request,$id)
    {
        $token = $request->header('Authorization',null);
        $jwt = new JwtAuth();
        $id = (int) $id;
        $payload = $jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'estado'=>'fallo',
                'descripcion'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl = new EmpresaBl();
            $data = $bl->borrarEmpresa($id);
            return $data;
        }
    }

    public function put_password (Request $request){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        $id = $payload->sub;
            if(!$payload)
            {
                $data=array(
                    'estado'=>'fallo',
                    'descripcion'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $oldpass = $request->json("oldpass");
                $nuevo = $request->json("newpass");
                $newpass=$nuevo.'software-1';
                $newpass=hash('sha256',$newpass);
                $bl = new EmpresaBl();
                $data = $bl->actualizarPassword($id,$oldpass,$newpass);
                return $data;
                
            }
    }

    public function resetPassword(Request $request)
    {
        $email_empresa=$request->json('correo',null);
        $bl = new EmpresaBl();
        return $bl->resetPassword($email_empresa);


    }
}
