<?php

namespace App\Http\Controllers;


use App\Models\Actividad;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\JwtAuth;
use App\Http\Controllers\Bl\UsuarioBl;
use Illuminate\Support\Facades\DB;
use \Validator;
use Image;
use Illuminate\Support\Facades\Storage;

class UsuarioController extends Controller
{
    //chris
    public function registrar(Request $request)
    {

        /*$rules = [
            'nombre' => 'required',
            'apellido' => 'required',
            'usuario' => 'required',
            'correo' => 'required',
            'fecha_nacimiento' => 'required',
            'telefono' => 'required',
            'password' => 'required',
            'imagen' => 'required',
        ];
        $customMessages = [
            'required' => 'El atributo :attribute  es requerido.'
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/

        $nombre=$request->json("nombre");
        $apellidos=$request->json("apellidos");
        $usuario_login=$request->json("usuario");
        $correo=$request->json("correo");
        $nacimiento=$request->json("fecha_nacimiento");
        $telefono=$request->json("telefono");
        $password=$request->json("password"); 
        $imagen=$request->json("imagen");
        $idioma=$request->json("idioma");
        //$nombreimagen = $imagen->getClientOriginalName();

        //return $nombre.'-'.$apellidos.'-'.$password;

        if(!is_null($nombre) && !is_null($apellidos) && !is_null($correo) && !is_null($nacimiento)
            && !is_null($telefono) && !is_null($usuario_login)
            && !is_null($password) && !is_null($idioma)){
            $bl = new UsuarioBl();
            $data=$bl->registro($nombre, $apellidos, $usuario_login,$correo, $nacimiento, $telefono,$password,$imagen,$idioma);

        }else{

            $data=array(
                'mensaje'=>'usuario no creado',
                'descripcion'=>'algun parametro en null',
                'estado'=>'error'
            );

        }

        return response()->json($data,200);

        /*$usuario=Usuario::find(2);
        return $usuario->persona->nombre;*/

    }

    //chris
    public function login(Request $request)
    {
        $usuario=$request->json("usuario");
        $password=$request->json("password");
        /*$rules = [
            'ci' => 'required',
            'password' => 'required',

        ];
        $customMessages = [
            'required' => 'El atributo :attribute  es requerido.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/
        $jwtAuth = new JwtAuth();



        if (!is_null($usuario) && !is_null($password) && isset($usuario) && isset($password))
        {
            $password=$password.'turismo';
            $password=hash('sha256',$password);
            $signUp=$jwtAuth->signUp($usuario,$password);
            return response()->json($signUp,200);
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null',
                'estado'=>'error'
            );

            return response()->json($data);
        }

    }

    public function decodificarToken(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'

            );
            return response()->json($data);

        }else{

            return response()->json($payload);
        }
    }

    public function pruebaValidacion(Request $request)
    {

        $comentarios=DB::table('comentarios')
            ->join('usuarios','comentarios.usuario_id','=','usuarios.usuario_id')
            ->join('calificaciones','calificaciones.usuario_id','=','usuarios.usuario_id')
            ->select('comentarios.comentario_id','usuarios.nombre','usuarios.usuario_id','usuarios.apellidos','comentarios.comentario','comentarios.created_at as fecha','usuarios.imagen','calificaciones.calificacion')
            ->where('comentarios.estado','=',false)
            ->where('comentarios.actividad_id','=',1)
            ->where('calificaciones.actividad_id','=',1)
            ->orderBy('comentarios.created_at','desc')
            ->get();
        return $comentarios;



        $rules = [
            'nombre' => 'required|min:5',
            'apellidos' => 'required',

        ];
        $customMessages = [
            'required' => 'El atributo :attribute  es requerido.',
            'min'=> 'El atributo :attribute debe tener al menos 5 caracteres'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {

            $data=array(
                'errores'=>$validatorUsuario->errors(),
                "estado"=>'error'
            );
            return response()->json($data);
            //return response()->json($validatorUsuario->errors(),400);
        }else{
            return $request->json('nombre').' '.$request->json('apellidos');
        }
    }
    public function perfil(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->mostrarUsuario($payload->sub);
        }

    }

    public function actualizarPerfil(Request $request)
    {
        /*$rules = [
            'nombre' => 'required',
            'apellidos' => 'required',
            'carrera' => 'required',
            'fecha_nacimiento' => 'required',
            'email' => 'required',
            'telefono' => 'required',
            'celular' => 'required',
        ];
        $customMessages = [
            'required' => 'El atributo :attribute  es requerido.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/
        $nombre=$request->json("nombre");
        $apellidos=$request->json("apellidos");
        $usuario_login=$request->json("usuario");
        $correo=$request->json("correo");
        $nacimiento=$request->json("fecha_nacimiento");
        $telefono=$request->json("telefono");
        $imagen = $request->json("imagen");
        $token=$request->header('Authorization',null);
        if(!is_null($nombre) && !is_null($apellidos) && !is_null($correo) && !is_null($nacimiento)
            && !is_null($telefono) && !is_null($usuario_login)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new UsuarioBl();
                return $bl->actualizarPerfil($payload->sub,$nombre, $apellidos, $usuario_login,$correo, $nacimiento, $telefono,$imagen);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }


    }


    public function actualizarPassword(Request $request)
    {
        $token=$request->header('Authorization',null);
        $pass1=$request->json("actual_password");
        $pass2=$request->json("nueva_password");
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->actualizarPassword($payload->sub,$pass1,$pass2);
        }

    }

    public function actualizarIdioma(Request $request)
    {
        $token=$request->header('Authorization',null);
        $idioma=$request->json("idioma");
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->actualizarIdioma($payload->sub,$idioma);
        }

    }


    public function verMisReservas(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->verMisReservas($payload->sub);
        }

    }

    public function verMiCompra(Request $request,$reserva_id)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->verMiCompras($payload->sub,$reserva_id);
        }

    }

    public function resetPassword(Request $request)
    {
        $nombre_usuario=$request->json('usuario',null);
        $bl = new UsuarioBl();
        return $bl->resetPassword($nombre_usuario);


    }

    public function verificarCodigo(Request $request)
    {
        $codigo=$request->json('codigo',null);
        $bl = new UsuarioBl();
        return $bl->verificarCodigo($codigo);


    }

    public function ponerNuevaPassword(Request $request)
    {
        $id=$request->json('id',null);
        $tipo=$request->json('tipo',null);
        $password=$request->json('password',null);
        $bl = new UsuarioBl();
        return $bl->ponerNuevaPassword($id,$tipo,$password);


    }



}
