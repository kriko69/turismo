<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Http\Controllers\Dao\UsuarioDao;
use Illuminate\Http\Request;
use Twilio\Rest\Client;

class SmsController extends Controller
{
    public function enviarSms(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{
            $dao = new UsuarioDao();
            $telefono=$dao->obtenerTelefono($payload->sub);
            if ($telefono=="0")
            {
                $data=array(
                    'data'=>null,
                    'mensaje'=>'El usuario no tiene numero.',
                    "estado"=>'error'
                );
                return response()->json($data);
            }else{
                $telefono= "+591".$telefono;
                $tokenCompra=$this->generarToken();
                $body="Tu codigo de verificacion de compra en Turisteando Bolivia es: ".$tokenCompra.".";
                try
                {
                    $seGuardo=$dao->guardarCodigoCompra($payload->sub,$tokenCompra);
                    if($seGuardo)
                    {
                        $sid    = env( 'TWILIO_SID' );
                        $token  = env( 'TWILIO_TOKEN' );
                        $client = new Client( $sid, $token );
                        $client->messages->create(
                            $telefono,
                            [
                                'from' => env( 'TWILIO_FROM' ),
                                'body' => $body,
                            ]
                        );
                        $data=array(
                            'data'=>null,
                            'mensaje'=>"sms enviado a ".$telefono." con token ".$tokenCompra,
                            "estado"=>'exito'
                        );
                        return response()->json($data);
                    }else{
                        $data=array(
                            'data'=>null,
                            'mensaje'=>"Error al guardar token en DB",
                            "estado"=>'error'
                        );
                        return response()->json($data);
                    }

                }catch (\Exception $e)
                {
                    $data=array(
                        'data'=>null,
                        'mensaje'=>"error ".$e->getMessage(),
                        "estado"=>'error'
                    );
                    return response()->json($data);
                }
            }
        }
    }

    public function generarToken()
    {
        $token="";
        for($i=0;$i<6;$i++)
        {
            $token.=mt_rand(1,9);
        }
        return $token;
    }

    public function verificarTokenCompra(Request $request)
    {
        $token=$request->header('Authorization',null);
        $tokenCompra=$request->json('token');
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{
            $dao = new UsuarioDao();
            $verificacion=$dao->verificarCodigoCompra($payload->sub,$tokenCompra);
            if ($verificacion=="iguales")
            {
                $data=array(
                    'data'=>null,
                    'mensaje'=>'El token de compra es correcto.',
                    "estado"=>'exito'
                );
                return response()->json($data);
            }else{
                $data=array(
                    'data'=>null,
                    'mensaje'=>'El token de compra es incorrecto',
                    "estado"=>'error'
                );
                return response()->json($data);
            }

        }
    }
}
