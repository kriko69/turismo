<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Http\Controllers\Bl\ReservaBl;
use Illuminate\Http\Request;

class ReservaController extends Controller
{
    public function verificarDisponibilidad(Request $request,$actividad_id)
    {
        $fecha=$request->json('fecha',null);
        $cantidad_visitantes=$request->json('cantidad_visitantes',null);
        if(!is_null($fecha) || !is_null($cantidad_visitantes)){

            $bl = new ReservaBl();
            return $bl->verificarDisponibilidad($fecha,$cantidad_visitantes,$actividad_id);

        }
        else{
            $data=array(
                'data'=>null,
                'descripcion'=>'Datos incorrectos algun dato en null.',
                'estado'=>'error'
            );
            return response()->json($data);
        }

    }



    public function reservar(Request $request,$actividad_id)
    {
        $token=$request->header('Authorization',null); //token de empresa
        $cantidad_reservas=$request->json('cantidad_reservas',null);
        $precio_total=$request->json('precio_total',null);
        $fecha=$request->json('fecha',null);
        $jwt = new JwtAuth();
        $payload = $jwt->verificarToken($token);
        if (!$payload) {
            $data=array(
                'data'=>null,
                'descripcion'=>'Token incorrecto',
                'estado'=>'error'
            );
            return response()->json($data);

        } else {
            if(!is_null($fecha) || !is_null($cantidad_reservas) || !is_null($precio_total)){

                $bl = new ReservaBl();
                //return $cantidad_reservas." ".$precio_total." ".$fecha." ".$payload->sub." ".$actividad_id;
                return $bl->reservar($cantidad_reservas,$precio_total,$fecha,$payload->sub,$actividad_id);

            }
            else{
                $data=array(
                    'data'=>null,
                    'descripcion'=>'Datos incorrectos algun dato en null.',
                    'estado'=>'error'
                );
                return response()->json($data);
            }
        }
    }
}
