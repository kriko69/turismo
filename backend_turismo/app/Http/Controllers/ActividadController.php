<?php

namespace App\Http\Controllers;

use App\Models\Reserva;
use Illuminate\Http\Request;
use App\Http\Controllers\Bl\ActividadBl;
use App\Helpers\JwtAuth;
use \App\Models\Actividad;
use Illuminate\Support\Facades\DB;

class ActividadController extends Controller
{
    public function post_actividad(Request $request)
    {        
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if($payload){
            $nombre_esp=$request->json("nombre_esp");
            $nombre_eng=$request->json("nombre_eng");
            $ciudad=$request->json("ciudad");
            $pais=$request->json("pais");
            $horario=$request->json("horario");
            $precio=$request->json("precio");
            $descripcion_esp=$request->json("descripcion_esp");
            $descripcion_eng=$request->json("descripcion_eng");
            $imagen=$request->json("imagen");
            $empresa_id= $payload->sub;
            $cantidad =$request->json("cantidad_visitantes");
            $lugar=$request->json("lugar_encuentro");
            $fecha_inicio=$request->json("fecha_inicio");
            $repite=$request->json("se_repite");
            $dias=$request->json("dias");
            //$id = Usuario::find($id_usuario); te devuelve todo como select
            if(!is_null($nombre_esp) && !is_null($nombre_eng)  &&!is_null($ciudad) && !is_null($pais) && !is_null($horario) 
             && !is_null($precio) && !is_null($descripcion_esp) && !is_null($descripcion_eng) && !is_null($empresa_id) && !is_null($cantidad)
             && !is_null($lugar) && !is_null($fecha_inicio) && !is_null($repite)  ){
                $bl = new ActividadBl();
                $data = $bl->post_actividad($nombre_esp,$nombre_eng,$ciudad,$pais,$horario,$precio,$descripcion_esp,$descripcion_eng,
                                            $imagen,$empresa_id,$cantidad,$lugar,$fecha_inicio,$repite,$dias);
                return $data;
                
            }
            else{
                $data=array(
                    'estado'=>'fallo',
                    'mensaje'=>'Actividad no creada',
                    'descripcion'=>'No lleno todos los campos correctamente'
                );
                return response()->json($data);
            }
        }
        else{
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }
            
        
    }

    public function get_actividades(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'estado' => 'fallo',
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl = new ActividadBl();
            $res = $bl->listarActividades();
            $data = array(
                'estado'=>'exito',
                'descripcion'=>'respuesta generada con exito',
                'data' =>$res
            );
            return $data;
        }
    }

    public function get_sin_registro()
    {
            $bl = new ActividadBl();
            $res = $bl->listarSinRegistro();
            $data = array(
                'estado'=>'exito',
                'descripcion'=>'respuesta generada con exito',
                'data' =>$res
            );
            return $data;
        
    }

    public function get_actividades_empresa(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        $id = $payload->sub;
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl = new ActividadBl();
            $res = $bl->listarActividadesEmpresa($id);
            $data = array(
                'estado'=>'exito',
                'descripcion'=>'respuesta generada con exito',
                'data' =>$res
            );
            return $data;
        }

    }

    public function get_actividad(Request $request,$id)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl = new ActividadBl();
            $res = $bl->listarActividad($id);
            $data = array(
                'estado'=>'exito',
                'descripcion'=>'respuesta generada con exito',
                'data' =>$res
        );
            return $data;
        }
    }

    public function put_actividad(Request $request,$id)
    {
        $token = $request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload = $jwt->verificarToken($token);
        $id = (int) $id;
        $id_empresa = $payload->sub;
        if(!$payload)
        {
            $data=array(
                'estado'=>'fallo',
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }
        else{
            $nombre_esp=$request->json("nombre_esp");
            $nombre_eng=$request->json("nombre_eng");
            $ciudad=$request->json("ciudad");
            $pais=$request->json("pais");
            $horario=$request->json("horario");
            $precio=$request->json("precio");
            $descripcion_esp=$request->json("descripcion_esp");
            $descripcion_eng=$request->json("descripcion_eng");
            $imagen=$request->json("imagen");
            $empresa_id= $payload->sub;
            $cantidad =$request->json("cantidad_visitantes");
            $lugar=$request->json("lugar_encuentro");
            $fecha_inicio=$request->json("fecha_inicio");
            $repite=$request->json("se_repite");
            $dias=$request->json("dias");
            if(!is_null($nombre_esp) && !is_null($nombre_eng)  &&!is_null($ciudad) && !is_null($pais) && !is_null($horario) 
             && !is_null($precio) && !is_null($descripcion_esp) && !is_null($descripcion_eng) && !is_null($empresa_id) && !is_null($cantidad)
             && !is_null($lugar) && !is_null($fecha_inicio) && !is_null($repite)  ){
            
                $bl = new ActividadBl();
                $res=$bl->put_actividad($nombre_esp,$nombre_eng,$ciudad,$pais,$horario,$precio,$descripcion_esp,$descripcion_eng,
                $imagen,$empresa_id,$cantidad,$lugar,$fecha_inicio,$repite,$dias,$id,$id_empresa);
            }
            else{
                $data=array(
                'estado'=>'fallo',
                'mensaje'=>'Datos incorrectos algun dato en null'
                );
                return $data;
            }
            
            return $res;
        }
        
    }

    public function delete_actividad(Request $request, $id)
    {
        $token = $request->header('Authorization',null);
        $jwt = new JwtAuth();
        $id = (int) $id;
        $payload = $jwt->verificarToken($token);
        $id_empresa = $payload->sub;
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl = new ACtividadBl();
            $data = $bl->borrarACtividad($id,$id_empresa);
            return $data;
        }
    }

    public function mostrarReservas(Request $request,$id)
    {
        $token=$request->header('Authorization',null);

        $jwt = new JwtAuth();
        $payload = $jwt->verificarToken($token);
        if (!$payload) {
            $data = array(
                'mensaje' => 'Token incorrecto'
            );
            return response()->json($data);
        } else {
            $bl = new ActividadBl();
            return $bl->mostrarReservas($id,$payload);
        }
    }
}
