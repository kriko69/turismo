<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\JwtAuth;
use App\Http\Controllers\Bl\BuzonBl;

class BuzonController extends Controller
{
    public function buzon(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        $id = $payload->sub;
        if(!$payload)
        {
            $data=array(
                'estado'=>'fallo',
                'descripcion'=>'Token incorrecto'
            );
            return response()->json($data,200);
        }else{
            $bl = new BuzonBl();
            $calcom = $bl->buzonCalcom($id);
            $com = $bl->buzonCompra($id);
            if (!empty($calcom[0]) && !empty($com[0])){
                $data = array(
                    'estado'=>'exito',
                    'descripcion'=>'respuesta generada con exito',
                    'calificaciones_comentarios' =>$calcom,
                    'compras' => $com
                );
            }else {
                $data = array(
                    'estado'=>'exito',
                    'descripcion'=>'La empresa no tiene nada en el buzon',
                    'data' =>null
                );
            }
            return $data;
        }

    }  
}
