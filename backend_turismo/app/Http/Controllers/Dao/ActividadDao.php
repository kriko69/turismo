<?php

namespace App\Http\Controllers\Dao;

use Illuminate\Support\Facades\DB;
use App\Models\Actividad;

class ActividadDao
{
    function post_actividad($actividad)
    {
        DB::beginTransaction();
                try {
                    $actividad->save();
                    $data=array(
                        'mensaje'=>'Actividad creada con exito',
                        'descripcion'=>'exito',
                        'estado'=> 'exito'
                    );
                    DB::commit();
                } catch (Exception $e) {
                    $data=array(
                        'mensaje'=>'Error al realizar la transaccion',
                        'descripcion'=>'fallo'
                    );
                    DB::rollback();
                }
                return $data;
    }

    function listarActividades()
    {
        //$actividad = Actividad::all()->where('estado','<>',true);
        $act = DB::table('actividades')
                    ->select('actividad_id', 'nombre_esp', 'nombre_eng', 'ciudad', 'pais', 'horario',
                    'precio', 'descripcion_esp', 'descripcion_eng', 'empresa_id', 'imagen','cantidad_visitantes',
                    'lugar_encuentro','fecha_inicio','se_repite','dias','calificacion')
                    ->where('estado', '<>', true)
                    ->get();
        
        /*DB::select('select  actividad_id, nombre_esp, nombre_eng, ciudad, pais, horario, telefono,
        precio, descripcion_esp, descripcion_eng, empresa_id from actividades where estado != true', [1]);*/
        return $act;
    }

    function listarSinRegistro()
    {
        $date=getdate();
        $dia=$date['mday']-1; //sale la fecha con un dia demas
        $fecha=$date['year'].'-'.$date['mon'].'-'.$dia;
        $act = DB::table('actividades')
            ->join('empresas','actividades.empresa_id', '=', 'empresas.empresa_id')
            ->select('actividades.actividad_id','actividades.nombre_esp', 'actividades.nombre_eng', 'actividades.ciudad'
                    ,'actividades.pais','actividades.horario','actividades.precio','actividades.descripcion_esp', 
                    'actividades.descripcion_eng', 'actividades.empresa_id', 'actividades.imagen','actividades.cantidad_visitantes',
                    'actividades.lugar_encuentro','actividades.fecha_inicio','actividades.se_repite','actividades.dias',
                    'actividades.calificacion','empresas.nombre as nombre_empresa')
            ->where('actividades.estado', '<>', true)
            ->where('actividades.se_repite', '=', 0)
            ->get();
        $act2 = DB::table('actividades')
            ->select('actividad_id', 'nombre_esp', 'nombre_eng', 'ciudad', 'pais', 'horario',
                'precio', 'descripcion_esp', 'descripcion_eng', 'empresa_id', 'imagen','cantidad_visitantes',
                'lugar_encuentro','fecha_inicio','se_repite','dias')
            ->where('estado', '<>', true)
            ->where('se_repite', '<>', 0)
            ->where('fecha_inicio', '>=', $fecha)
            ->get();

        $actx=json_encode($act);
        $act2x=json_encode($act2);
        $fusion=array_merge(json_decode($actx),json_decode($act2x));
        return $fusion;
    }

    function listarActividadesEmpresa($id)
    {
        $act = DB::table('actividades')
                    ->select('actividad_id', 'nombre_esp', 'nombre_eng', 'ciudad', 'pais', 'horario',
                    'precio', 'descripcion_esp', 'descripcion_eng', 'empresa_id', 'imagen','cantidad_visitantes',
                    'lugar_encuentro','fecha_inicio','se_repite','dias','calificacion')
                    ->where([
                        ['estado', '<>', true],
                        ['empresa_id','=',$id]
                    ])
                    ->get();
        /*DB::select('select  actividad_id, nombre_esp, nombre_eng, ciudad, pais, horario, telefono,
        precio, descripcion_esp, descripcion_eng, empresa_id from actividades where estado != true and empresa_id ='.$id.'', [1]);*/
        return $act;
    }
    
    function listarActividad($id)
    {   
        //$actividad = Actividad::all()->where('estado','<>',true);
        $act = DB::table('actividades')
                    ->select('actividad_id', 'nombre_esp', 'nombre_eng', 'ciudad', 'pais', 'horario',
                    'precio', 'descripcion_esp', 'descripcion_eng', 'empresa_id', 'imagen','cantidad_visitantes',
                    'lugar_encuentro','fecha_inicio','se_repite','dias','calificacion')
                    ->where([
                        ['estado', '<>', true],
                        ['actividad_id','=',$id]
                    ])
                    ->first();
        return $act;
    
    }

    function put_actividad($nombre_esp,$nombre_eng,$ciudad,$pais,$horario,$precio,$descripcion_esp,$descripcion_eng,
    $imagen,$empresa_id,$cantidad,$lugar,$fecha_inicio,$repite,$dias,$id,$id_empresa)
    {
        $actividad=Actividad::find($id);
        $actividad->nombre_esp=$nombre_esp;
        $actividad->nombre_eng=$nombre_eng;
        $actividad->ciudad=$ciudad;
        $actividad->pais=$pais;
        $actividad->horario=$horario;
        $actividad->precio=$precio;
        $actividad->descripcion_esp=$descripcion_esp;
        $actividad->descripcion_eng=$descripcion_eng;
        if($imagen){
            $image = str_replace('data:image/png;base64,', '', $imagen);
            $image = str_replace('data:image/jpg;base64,', '', $imagen);
            $image = str_replace('data:image/jpeg;base64,', '', $imagen);
            $image = str_replace(' ','+',$image);
            $subidaimagen = Image::make($image);
            $path = public_path().'/imagenes/actividades/';
            $nombre = str_replace(' ','',$nombre_esp);
            $nombreimagen = $nombre.time().'.jpg';
            $subidaimagen->resize(500,500)->save($path.$nombreimagen);
            $actividad->imagen = $nombreimagen;
        }
        $aux = $actividad->empresa_id;
        $actividad->empresa_id=$empresa_id;
        $actividad->cantidad_visitantes=$cantidad;
        $actividad->lugar_encuentro=$lugar;
        $actividad->fecha_inicio=$fecha_inicio;
        $actividad->se_repite=$repite;
        $actividad->dias=$dias;

        DB::beginTransaction();
            try {
                if( $aux == $id_empresa){
                    $actividad->save();
                    $data=array(
                        'descripcion'=>'Actividad actualizada con exito',
                        'estado'=>'exito'
                    );
                    DB::commit();
                }
                else{
                    $data = array(
                        'estado'=>'fallo',
                        'descripcion'=>'No puede modificar una actividad que no le pertenece'
                    );
                    DB::rollback();
                }
            } catch (Exception $e) {
                $data=array(
                    'descripcion'=>'Error al realizar la transaccion',
                    'estado'=>'fallo'
                );
                DB::rollback();
            }
        
        return response()->json($data,200);
    
    }

    function borrarActividad($id,$id_empresa)
    {
        $act=Actividad::find($id);
        if(sizeof($act->reservas)==0) //no tiene reservas
        {
            $act->estado=true;
            DB::beginTransaction();
            try {
                $aux = $act->empresa_id;
                if($aux == $id_empresa){
                    $act->save();
                    $data=array(
                        'mensaje'=>'Actividad borrada con exito',
                        'descripcion'=>'exito'
                    );
                    DB::commit();
                }
                else {
                    $data = array(
                        'estado'=>'fallo',
                        'descripcion'=>'No puede borrar una actividad que no le pertenece'
                    );
                    DB::rollback();
                }
            } catch (Exception $e) {
                $data=array(
                    'mensaje'=>'Error al realizar la transaccion',
                    'descripcion'=>'fallo'
                );
                DB::rollback();
            }
            return $data;
        }else{
            $data=array(
                'data'=>null,
                'mensaje'=>'La actividad tiene reservas',
                'estado'=>'error'
            );
        }


    }

    function mostrarReservas($id)
    {
        DB::beginTransaction();
        try {
            $reservas=DB::table('reservas')
                ->join('usuarios','reservas.usuario_id','=','usuarios.usuario_id')
                ->join('actividades','reservas.actividad_id','=','actividades.actividad_id')
                ->select('usuarios.nombre','usuarios.apellidos','usuarios.telefono','reservas.cantidad_reservas','reservas.precio_total','reservas.fecha','usuarios.imagen','reservas.estado')
                ->where('reservas.estado','=',false)
                ->where('reservas.actividad_id','=',$id)
                ->orderByDesc('reservas.fecha')
                ->get();
            DB::commit();
        } catch (Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'estado'=>'error'
            );
            DB::rollback();
        }
        if (sizeof($reservas)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'Esta actividad no tiene reservas.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$reservas,
                'mensaje'=>'Exito al encontrar las reservas.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }
    }

}
?>
