<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 09-09-19
 * Time: 05:16 PM
 */
namespace App\Http\Controllers\Dao;
use App\Models\Empresa;
use Illuminate\Support\Facades\DB;
use App\Helpers\JwtAuth;

class EmpresaDao
{
    function verificarExistenciaEmpresa($correo)
    {
        $isset_usuario = Empresa::where('correo',"=", $correo)->first();
        if (!is_object($isset_usuario))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }
    
    function obtenerEmpresaParaToken($correo,$password)
    {
        $user = Empresa::where(
            array(
                'correo' => $correo,
                'password' => $password
            )
        )->first();
        return $user;
    }

    function registrar_empresa($empresa)
    {
        DB::beginTransaction();
                try {
                    $empresa->save();
                    $data=array(
                        'mensaje'=>'Empresa mando afiliación con exito',
                        'estado'=>'exito',
                        'empresa_id'=>$empresa->empresa_id
                    );
                    DB::commit();
                } catch (Exception $e) {
                    $data=array(
                        'estado'=>'fallo',
                        'mensaje'=>'Error al realizar la transaccion',
                        'descripcion'=>'fallo'
                    );
                    DB::rollback();
                }
        return $data;
    }

    
    function login_empresa($correo,$password)
    {
        $jwtAuth = new JwtAuth();
        $correo = (String) $correo;
        $afiliacion = DB::table('empresas')
                            ->select('afiliacion')
                            ->where([
                                ['estado', '<>', true],
                                ['afiliacion', '=', 'Aceptada']])
                            ->get();
        if($afiliacion != null){
            $signUp=$jwtAuth->signUpEmpresa($correo,$password); //Chris JWT
            
            return response()->json($signUp,200);
        }
        else{            
            $data = array(
                'estado'=>'fallo',
                'descripcion'=>'Empresa no se logeo con exito'
            );
            return response()->json($data,400);
        }
    }

    function listarEmpresas()
    {
        //$emp = Empresa::all()->where('estado','<>',true);
        $emp = DB::table('empresas')
                    ->select('empresa_id','nombre','pais', 'ciudad', 'descripcion', 'telefono', 'correo',
                    'afiliacion','imagen','calificacion')
                    ->where('estado', '<>', true)
                    ->get();
        /*$emp = DB::select('select empresa_id, nombre, pais, ciudad, descripcion, telefono, correo,
        afiliacion, imagen
        from empresas where estado != true', [1]);*/

        return $emp;
    }
     
    function listarEmpresa($id)
    {
        $emp = DB::table('empresas')
                    ->select('empresa_id','nombre','pais', 'ciudad', 'descripcion', 'telefono', 'correo',
                    'afiliacion','imagen','calificacion')
                    ->where([
                        ['estado', '<>', true],
                        ['afiliacion', '=', 'Aceptada'],
                        ['empresa_id','=',$id]])
                    ->first();
        
        return $emp;
    }
    
    function put_empresa($nombre,$pais,$ciudad,$descripcion,$telefono,$correo,$imagen,$id)
    {
        $emp=Empresa::find($id);
            $emp->nombre=$nombre;
            $emp->pais=$pais;
            $emp->ciudad=$ciudad;
            $emp->descripcion=$descripcion;
            $emp->telefono=$telefono;
            $emp->correo=$correo;
            if($imagen){
                $image = str_replace('data:image/png;base64,', '', $imagen);
                $image = str_replace('data:image/jpg;base64,', '', $imagen);
                $image = str_replace('data:image/jpeg;base64,', '', $imagen);
                $image = str_replace(' ','+',$image);
                $subidaimagen = Image::make($image);
                $path = public_path().'/imagenes/empresas/';
                $nombre = str_replace(' ','',$nombre);
                $nombreimagen = $nombre.time().'.jpg';
                $subidaimagen->resize(200,200)->save($path.$nombreimagen);
                $emp->imagen = $nombreimagen;
            }
            DB::beginTransaction();
            try {
                $emp->save();
                $data=array(
                    'descripcion'=>'Empresa actualizada con exito',
                    'estado'=>'exito'
                );
                DB::commit();
            } catch (Exception $e) {
                $data=array(
                    'descripcion'=>'Error al realizar la transaccion',
                    'estado'=>'fallo'
                );
                DB::rollback();
            }
        
        return response()->json($data,200);
    }

    function borrarempresa($id)
    {
        $emp = Empresa::find($id);

        $emp->estado = true;
        DB::beginTransaction();
        try {
            $emp->save();
            $data=array(
                'mensaje'=>'Empresa borrada con exito',
                'descripcion'=>'exito'
            );
            DB::commit();
        } catch (Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo'
            );
            DB::rollback();
        }
    
    return response()->json($data,200);
    }

    function actualizarPassword($id,$oldpass,$newpass)
    {
        $emp=Empresa::find($id);
        if($emp->password = $oldpass){
            $emp->password=$newpass;
            DB::beginTransaction();
            try {       
                $emp->save();
                $data=array(
                    'data'=>null,
                    'descripcion'=>'Password atualizado.',
                    'estado'=>'exito'
                );
                DB::commit();
            } catch (Exception $e) {
                $data=array(
                    'data'=>null,
                    'descripcion'=>'Error al actualizar password.',
                    'estado'=>'error',
                );
                DB::rollback();
            }
        }
        else{
            $data = array(
                'data'=>null,
                'descripcion'=>'Password no coicide con el anterior',
                'estado'=>'fallo'                
            );
        }
            
        
        return $data;

    }

    function actualizarAfiliacion($id,$afiliacion)
    {
        $emp=Empresa::find($id);
            $emp->afiliacion=$afiliacion;
            $emp->estado=false;
            DB::beginTransaction();
            try {       
                $emp->save();
                $data=array(
                    'data'=>null,
                    'descripcion'=>'Correo enviado con exito y cambio de estado de afiliacion.',
                    'estado'=>'exito'
                );
                DB::commit();
            } catch (Exception $e) {
                $data=array(
                    'data'=>null,
                    'descripcion'=>'Error al enviar el correo intente de nuevo.',
                    'estado'=>'error',
                );
                DB::rollback();
            }
        
        return $data;
    }

    public function verificarCorreoResetPassword($email_empresa)
    {
        $empresa = Empresa::where(
            array(
                'correo' => $email_empresa
            )
        )->first(['empresa_id','nombre','codigo_reset_password','correo']);

        return $empresa;
    }

    public function guardarCodigoResetPassword($codigo)
    {
        $empresa_id=substr($codigo,0,1);
        $empresa = Empresa::find($empresa_id);
        $empresa->codigo_reset_password=strtoupper($codigo);
        try
        {
            $empresa->save();
            //return 'se guardo '.strtoupper($codigo);
            return true;
        }catch (\Exception $e)
        {
            return false;
        }
    }

    public function obtenercodigo($empresa_id)
    {
        $empresa = Empresa::where(
            array(
                'empresa_id' => $empresa_id
            )
        )->first(['empresa_id as id','nombre as nombre_empresa','codigo_reset_password']);

        return $empresa;
    }

    public function ponerNuevaPassword($usuario_id,$password)
    {
        $empresa = Empresa::find($usuario_id);
        $empresa->password=$password;
        $empresa->codigo_reset_password="null";
        try
        {
            $empresa->save();
            //return 'se guardo '.strtoupper($codigo);
            return true;
        }catch (\Exception $e)
        {
            return false;
        }
    }
}

?>
