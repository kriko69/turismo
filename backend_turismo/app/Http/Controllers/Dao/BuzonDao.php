<?php

namespace App\Http\Controllers\Dao;

use Illuminate\Support\Facades\DB;
use App\Models\Actividad;


class BuzonDao 
{
    public function buzonCalcom($id)
    {
        $act_cal = DB::table('actividades')
            ->join('calificaciones', 'actividades.actividad_id', '=', 'calificaciones.actividad_id')
            ->join('usuarios', 'usuarios.usuario_id', '=', 'calificaciones.usuario_id')
            ->join('comentarios',function($join){
                $join->on('actividades.actividad_id', '=', 'comentarios.actividad_id');
                $join->on('usuarios.usuario_id', '=', 'comentarios.usuario_id');
            })
            ->select('actividades.actividad_id', 'actividades.nombre_esp', 'usuarios.usuario','calificaciones.calificacion','comentarios.comentario','usuarios.imagen','calificaciones.created_at as fecha')
            ->where('actividades.empresa_id','=',$id)
            ->orderBy('calificaciones.created_at', 'asc')
            ->get();

            return ($act_cal);
    }
    
    public function buzonCompra($id)
    {
        $act_com = DB::table('actividades')
            ->join('reservas', 'actividades.actividad_id', '=', 'reservas.actividad_id')
            ->join('usuarios', 'usuarios.usuario_id', '=', 'reservas.usuario_id')
            ->select('actividades.actividad_id', 'actividades.nombre_esp', 'usuarios.usuario','reservas.cantidad_reservas','reservas.precio_total','usuarios.imagen','reservas.created_at as fecha')
            ->where('actividades.empresa_id','=',$id)
            ->orderBy('reservas.created_at', 'asc')
            ->get();
            return $act_com;
    }

}
?>