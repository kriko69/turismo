<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 21-10-19
 * Time: 04:50 PM
 */
namespace App\Http\Controllers\Dao;


use Illuminate\Support\Facades\DB;

class CalificacionDao
{
    public function calificar($calificacion)
    {
        DB::beginTransaction();
        try {

            $calificacion->save();

            $data=array(
                'data'=>null,
                'mensaje'=>'Calificacion creada con exito',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }
}
