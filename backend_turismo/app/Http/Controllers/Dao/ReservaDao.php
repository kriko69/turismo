<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 16-09-19
 * Time: 06:43 PM
 */
namespace App\Http\Controllers\Dao;


use App\Models\Actividad;
use App\Models\CupoDia;
use Illuminate\Support\Facades\DB;

class ReservaDao
{
    function verificarSiEstaEnCupoDia($fecha,$actividad_id)
    {
        $cupo = CupoDia::where(
            array(
                'fecha_actividad' => $fecha,
                'actividad_id' => $actividad_id
            )
        )->first(['cupo_dia_id','cupos_disponibles']);
        return $cupo;
    }

    function verificarCantidadVisitantesActividad($actividad_id)
    {
        $cupo = Actividad::where(
            array(
                'actividad_id' => $actividad_id,
            )
        )->first(['cantidad_visitantes']);
        return $cupo;
    }

    function obtenerDiasActividad($actividad_id)
    {
        $cupo = Actividad::where(
            array(
                'actividad_id' => $actividad_id,
            )
        )->first(['dias']);
        return $cupo;
    }

    public function reservarConCupo($reserva,$cupo_dia)
    {
        DB::beginTransaction();
        try {

            $reserva->save();
            $cupo_dia->save();

            $data=array(
                'mensaje'=>'reserva y cupo creado con exito',
                'descripcion'=>'exito',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }


    public function mostrarCupo($cupo_dia_id)
    {
        return CupoDia::find($cupo_dia_id);
    }

    public function actividadEstaBorrada($actividad_id)
    {
        $actividad = Actividad::find($actividad_id,['estado']);
        return $actividad->estado;
    }
}
