<?php

namespace App\Http\Controllers\Dao;

use App\Models\Calificacion;
use Illuminate\Support\Facades\DB;

class ComentarioDao
{
    public function comentar($comentario,$calificacion)
    {
        DB::beginTransaction();
        try {

            $comentario->save();
            $calificacion->save();
            $data=array(
                'data'=>null,
                'mensaje'=>'Comentario y calificacion creado con exito',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }


    public function verComentarios($actividad_id)
    {
        //$comentarios=null;
        DB::beginTransaction();
        try {

            $comentarios=DB::table('comentarios')
                ->join('usuarios','comentarios.usuario_id','=','usuarios.usuario_id')
                ->join('calificaciones','calificaciones.usuario_id','=','usuarios.usuario_id')
                ->select('comentarios.comentario_id','usuarios.nombre','usuarios.usuario_id','usuarios.apellidos','usuarios.imagen as usuario_imagen','comentarios.comentario','comentarios.created_at as fecha','calificaciones.calificacion')
                ->where('comentarios.estado','=',false)
                ->where('comentarios.actividad_id','=',$actividad_id)
                ->where('calificaciones.actividad_id','=',$actividad_id)
                ->orderBy('comentarios.created_at','desc')
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        //return $comentarios;
        if (sizeof($comentarios)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'Esta actividad no tiene comentarios.',
                'estado'=>'exito'
            );
        }else{
            $data=array(
                'data'=>$comentarios,
                'mensaje'=>'Exito al encontrar los comentarios.',
                'estado'=>'exito'
            );
        }
        return $data;
    }

    public function obtenerCalificacion($usuario_id,$actividad_id)
    {
        return Calificacion::where(
            array(
                'usuario_id' => $usuario_id,
                'actividad_id' => $actividad_id
            )
        )->first(['calificacion']);
    }
}
