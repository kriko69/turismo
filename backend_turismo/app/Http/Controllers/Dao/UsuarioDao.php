<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 09-09-19
 * Time: 05:16 PM
 */
namespace App\Http\Controllers\Dao;
use App\Models\Calificacion;
use App\Models\Comentario;
use App\Models\Reserva;
use App\Models\Usuario;
use Illuminate\Support\Facades\DB;

class UsuarioDao
{
    function obtenerUsuarioParaToken($usuario,$password)
    {
        $user = Usuario::where(
            array(
                'usuario' => $usuario,
                'password' => $password
            )
        )->first();
        return $user;
    }
    function verificarExistenciaUsuario($usuario)
    {
        $isset_usuario = Usuario::where(
            array(
                'usuario' => $usuario
            )
        )->first();
        if (!is_object($isset_usuario))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }

    function registrar($usuario)
    {
        DB::beginTransaction();
        try {

            $usuario->save();
            $usuario->roles()->attach(2);
            
            //Storage::disk('ftp')->putFileAs('/imagenes/Empresa',$imagen,$nombreimagen);
            $data=array(
                'mensaje'=>'usuario creado con exito',
                'descripcion'=>'exito',
                'estado'=>'exito',
                'usuario_id'=>$usuario->usuario_id
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function mostrarPerfil($id)
    {
        return Usuario::find($id);
    }

    public function actualizarPerfil($usuarioNuevo)
    {
        DB::beginTransaction();
        try {

            $usuarioNuevo->save();
            $data=array(
                'data'=>null,
                'mensaje'=>'usuario se actualizo con exito',
                'descripcion'=>'exito',
                'estado'=>'exito',
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }


    public function actualizarPassword($usuario_id,$actual_password_encriptada,$nueva_password_encriptada)
    {
        $usuario=$this->mostrarPerfil($usuario_id);
        if(strcmp($usuario->password,$actual_password_encriptada)==0)
        {
            //pasword iguales
            $usuario->password=$nueva_password_encriptada;
            $mensaje=$this->actualizarPerfil($usuario);
            return response()->json($mensaje);
        }else{
            //password distintas
            $data=array(
                'data'=>null,
                'mensaje'=>'Error su actual password no es la correcta.',
                'estado'=>'error'
            );
            return response()->json($data);
        }
    }


    public function actualizarIdioma($usuario_id,$idioma)
    {
        $usuario= Usuario::find($usuario_id);
        $usuario->idioma = $idioma;
        DB::beginTransaction();
        try {
            $usuario->save();
            $data=array(
                'descripcion'=>'Idioma actualizado con exito',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (Exception $e) {
            $data=array(
                'descripcion'=>'Error al realizar la transaccion',
                'estado'=>'fallo'
            );
            DB::rollback();
        }
        return response()->json($data,200);
    }

    public function verMisReservas($usuario_id)
    {
        DB::beginTransaction();
        try {
            $reservas=DB::table('reservas')
                ->join('usuarios','reservas.usuario_id','=','usuarios.usuario_id')
                ->join('actividades','reservas.actividad_id','=','actividades.actividad_id')
                ->join('empresas','empresas.empresa_id','=','actividades.empresa_id')
                ->select('reservas.reserva_id','actividades.actividad_id',
                    'empresas.empresa_id','empresas.nombre as nombre_empresa',
                    'reservas.cantidad_reservas','reservas.precio_total',
                    'reservas.fecha as fecha_actividad','actividades.nombre_esp',
                    'actividades.nombre_eng','actividades.horario as horario_actividad',
                    'actividades.descripcion_esp','actividades.descripcion_eng',
                    'actividades.lugar_encuentro','actividades.imagen','empresas.nombre as nombre_empresa',
                    'actividades.calificacion as calificacion_actividad')
                ->where('reservas.estado','=',false)
                //->where('actividades.actividad_id','=','reservas.actividad_id')
                ->where('reservas.usuario_id','=',$usuario_id)
                ->orderByDesc('reservas.fecha')
                ->get(array('empresas.nombre as nombre_empresa'));
        } catch (Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'estado'=>'error'
            );
            DB::rollback();
        }

        if (sizeof($reservas)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'Este usuario no tiene reservas.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$reservas,
                'mensaje'=>'Exito al encontrar las reservas.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }
    }



    public function verMiCompra($usuario_id,$reserva_id)
    {
        $actividad_id=$this->obtenerActividadDeReserva($reserva_id);
        $calificacion=$this->obternerCalificacion($usuario_id,$actividad_id);
        $comentario=$this->obternerComentario($usuario_id,$actividad_id);

        $calificacion_usuario = [
            "calificacion_usuario" => $calificacion,
            "comentario_usuario" => $comentario
        ];
        DB::beginTransaction();
        try {
            $compra=DB::table('reservas')
                ->join('actividades','reservas.actividad_id','=','actividades.actividad_id')
                ->join('empresas','empresas.empresa_id','=','actividades.empresa_id')
                ->select('reservas.reserva_id','actividades.actividad_id','reservas.cantidad_reservas',
                'reservas.precio_total','reservas.fecha as fecha_actividad','actividades.nombre_esp',
                'actividades.nombre_eng','actividades.horario as horario_actividad','actividades.descripcion_esp',
                'actividades.descripcion_eng','actividades.lugar_encuentro','actividades.imagen',
                'empresas.nombre as nombre_empresa','actividades.calificacion as calificacion_actividad'
                ,'empresas.empresa_id')
                ->where('reservas.estado','=',false)
                ->where('reservas.reserva_id','=',$reserva_id)
                ->get();
            $actx=json_encode($calificacion_usuario);

            $act2x=json_encode($compra[0]);
            $fusion=array_merge((array)json_decode($actx),(array)json_decode($act2x));
            $data=array(
                'data'=>$fusion,
                'mensaje'=>'Exito al encontrar la compra.',
                'estado'=>'exito'
            );
            return response()->json($data);
        } catch (Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'estado'=>'error'
            );
            DB::rollback();
        }


    }

    public function obternerCalificacion($usuario_id,$actividad_id)
    {
        $califico = Calificacion::where(
            array(
                'usuario_id' => $usuario_id,
                'actividad_id' => $actividad_id
            )
        )->first(['calificacion']);

        if(is_object($califico)) //si califico
        {
            return $califico->calificacion;
        }else
        {
            return -1;
        }
    }
    public function obternerComentario($usuario_id,$actividad_id)
    {
        $califico = Comentario::where(
            array(
                'usuario_id' => $usuario_id,
                'actividad_id' => $actividad_id
            )
        )->first(['comentario']);

        if(is_object($califico)) //si califico
        {
            return $califico->comentario;
        }else
        {
            return null;
        }
    }


    public function obtenerActividadDeReserva($reserva_id)
    {
        $reserva=Reserva::find($reserva_id);
        return $reserva->actividad_id;

    }

    public function verificarNombreUsuarioResetPassword($nombre_usuario)
    {
        $usuario = Usuario::where(
            array(
                'usuario' => $nombre_usuario
            )
        )->first(['usuario_id','nombre','apellidos','codigo_reset_password','correo']);

        return $usuario;
    }

    public function guardarCodigoResetPassword($codigo)
    {
        $usuario_id=substr($codigo,0,1);
        $user = Usuario::find($usuario_id);
        $user->codigo_reset_password=strtoupper($codigo);
        try
        {
            $user->save();
            //return 'se guardo '.strtoupper($codigo);
            return true;
        }catch (\Exception $e)
        {
            return false;
        }
    }

    public function obtenerCodigo($usuario_id)
    {
        $usuario = Usuario::where(
            array(
                'usuario_id' => $usuario_id
            )
        )->first(['usuario_id as id','nombre','apellidos','codigo_reset_password']);
        return $usuario;
    }

    public function ponerNuevaPassword($usuario_id,$password)
    {
        $user = Usuario::find($usuario_id);
        $user->password=$password;
        $user->codigo_reset_password="null";
        try
        {
            $user->save();
            //return 'se guardo '.strtoupper($codigo);
            return true;
        }catch (\Exception $e)
        {
            return false;
        }
    }

    public function obtenerTelefono($usuario_id)
    {
        $user = Usuario::find($usuario_id);
        if(is_object($user))
        {
            return $user->telefono;
        }else{
            return "0";
        }
    }

    public function guardarCodigoCompra($usuario_id,$token)
    {
        $user = Usuario::find($usuario_id);
        $user->codigo_compra=$token;
        try
        {
            $user->save();
            //return 'se guardo '.strtoupper($codigo);
            return true;
        }catch (\Exception $e)
        {
            return false;
        }
    }

    public function verificarCodigoCompra($usuario_id,$token)
    {
        $codigo = Usuario::find($usuario_id,['codigo_compra']);
        if($codigo->codigo_compra==$token)
        {
            return "iguales";
        }else
        {
            return "no iguales";
        }
    }
}
