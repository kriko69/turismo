<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 16-09-19
 * Time: 06:43 PM
 */
namespace App\Http\Controllers\Bl;

use App\Http\Controllers\Dao\ReservaDao;
use App\Models\CupoDia;
use App\Models\Reserva;

class ReservaBl
{

    public function verificarDisponibilidad($fecha,$cantidad_visitantes,$actividad_id)
    {
        $reservaDao = new ReservaDao();
        $cupo=$reservaDao->verificarSiEstaEnCupoDia($fecha,$actividad_id);
        //return $cupo;
        if(is_object($cupo)) //existe el cupo
        {
            if($cupo->cupos_disponibles>=$cantidad_visitantes)
            {
                $data=array(
                    'data'=>null,
                    'descripcion'=>'si hay espacio',
                    'estado'=>'exito'
                );
                return response()->json($data);
            }else{
                //solo quedan $cupo->cupos_disponibles cupos disponibles
                $data=array(
                    'data'=>null,
                    'descripcion'=>'no hay espacio',
                    'estado'=>'error'
                );
                return response()->json($data);
            }
        }else{ //no hay cupo
            //verificar fecha

            $sePuedeEseDia=$this->fechaValida($fecha,$actividad_id);
            if($sePuedeEseDia)
            {
                $cupoActividad=$reservaDao->verificarCantidadVisitantesActividad($actividad_id);
                //return $cupoActividad;
                if($cupoActividad->cantidad_visitantes>=$cantidad_visitantes)
                {
                    $data=array(
                        'data'=>null,
                        'descripcion'=>'si hay espacio',
                        'estado'=>'exito'
                    );
                    return response()->json($data);
                }else{
                    //solo quedan $cupoActividad->cantidad_visitantes cupos disponibles
                    $data=array(
                        'data'=>null,
                        'descripcion'=>'no hay espacio',
                        'estado'=>'error'
                    );
                    return response()->json($data);
                }
            }else{
                $data=array(
                    'data'=>null,
                    'descripcion'=>'no se puede viajar ese dia',
                    'estado'=>'error'
                );
                return response()->json($data);
            }
        }
    }

    public function saber_dia($nombredia) {

        $dias = array('', 'L','Ma','Mi','J','V','S', 'D');

        $fecha = $dias[date('N', strtotime($nombredia))];

        return $fecha;

    }

    public function fechaValida($fecha,$actividad_id) {

        $reservaDao = new ReservaDao();
        $dias = $reservaDao->obtenerDiasActividad($actividad_id);
        $fechaLiteral=$this->saber_dia($fecha);

        if (strpos($dias, $fechaLiteral) !== false) {
            //la actividad permite ese dia
            return true;
        }else{
            return false;
        }

    }


    public function reservar($cantidad_reservas,$precio_total,$fecha,$usuario_id,$actividad_id)
    {
        $existeEspacio=$this->verificarDisponibilidad($fecha,$cantidad_reservas,$actividad_id);
        $data = $existeEspacio->getData();
        $descripcion = $data->descripcion;

        if ($descripcion=='si hay espacio')
        {
            $reservaDao = new ReservaDao();
            $borrada=$reservaDao->actividadEstaBorrada($actividad_id);
            if(!$borrada)
            {
                //procedo a reservar
                //verifico si fecha y actividad_id estan en cupos_dia
                $reservaDao = new ReservaDao();
                $cupo=$reservaDao->verificarSiEstaEnCupoDia($fecha,$actividad_id);
                if(is_object($cupo)) //existe el cupo
                {
                    //solo tengo que actualizar el cupo
                    //y crear reserva
                    //EN UN FUTURO COMPROBAR SI LA RESERVA ES IGUAL Y SOLO SUMAR A LA CANTIDAD DE RESERVAS
                    $reserva = new Reserva();
                    $reserva->cantidad_reservas=$cantidad_reservas;
                    $reserva->precio_total=$precio_total;
                    $reserva->fecha=$fecha;
                    $reserva->usuario_id=$usuario_id;
                    $reserva->actividad_id=$actividad_id;


                    //obtengo informacion del cupo
                    $cupoData=$reservaDao->mostrarCupo($cupo->cupo_dia_id);
                    $cupoData->cupos_disponibles = $cupoData->cupos_disponibles - $cantidad_reservas;

                    return $reservaDao->reservarConCupo($reserva,$cupoData);

                }else{

                    //no esta en cupos_dia entonces tengo que crearlo
                    $cupoActividad=$reservaDao->verificarCantidadVisitantesActividad($actividad_id);

                    $cupos_disponible=$cupoActividad->cantidad_visitantes-$cantidad_reservas;
                    $fecha_actividad=$fecha;
                    $dia_actividad=$this->saber_dia($fecha);

                    $reserva = new Reserva();
                    $reserva->cantidad_reservas=$cantidad_reservas;
                    $reserva->precio_total=$precio_total;
                    $reserva->fecha=$fecha;
                    $reserva->usuario_id=$usuario_id;
                    $reserva->actividad_id=$actividad_id;

                    $cupo_dia = new CupoDia();
                    $cupo_dia->cupos_disponibles=$cupos_disponible;
                    $cupo_dia->fecha_actividad=$fecha_actividad;
                    $cupo_dia->dia_actividad=$dia_actividad;
                    $cupo_dia->actividad_id=$actividad_id;
                    return $reservaDao->reservarConCupo($reserva,$cupo_dia);
                }
            }else{
                $data=array(
                    'data'=>null,
                    'descripcion'=>'la actividad esta borrada',
                    'estado'=>'error'
                );
                return response()->json($data);
            }

        }else{
            if ($descripcion=='no hay espacio')
            {
                $data=array(
                    'data'=>null,
                    'descripcion'=>'no hay espacio',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $data=array(
                    'data'=>null,
                    'descripcion'=>'no se puede viajar ese dia',
                    'estado'=>'error'
                );
                return response()->json($data);
            }
        }
    }
}
