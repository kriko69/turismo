<?php

namespace App\Http\Controllers\Bl;
use App\Mail\ResetPasswordEmpresa;
use \App\Models\Empresa;
use \App\Http\Controllers\Dao\EmpresaDao;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\ImageManagerStatic as Image;

class EmpresaBl 
{
    function registro_empresa($nombre,$pais,$ciudad,$descripcion,$telefono,$correo,$password,$imagen)
    {
        $empresa = new Empresa();
        $empresa->nombre=$nombre;
        $empresa->pais=$pais;
        $empresa->ciudad=$ciudad;
        $empresa->descripcion=$descripcion;
        $empresa->telefono=$telefono;
        $empresa->correo=$correo;
        $password=$password.'software-1'; //la sal
        $empresa->password=hash('sha256',$password);
        if(!$imagen){
            $empresa->imagen = 'defaultent.png';
        }
        else{
            $image = str_replace('data:image/png;base64,', '', $imagen);
            $image = str_replace('data:image/jpg;base64,', '', $imagen);
            $image = str_replace('data:image/jpeg;base64,', '', $imagen);
            $image = str_replace(' ','+',$image);
            $subidaimagen = Image::make($image);
            $path = public_path().'/imagenes/empresas/';
            $nombre = str_replace(' ','',$nombre);
            $nombreimagen = $nombre.time().'.jpg';
            $subidaimagen->resize(200,200)->save($path.$nombreimagen);
            $empresa->imagen = $nombreimagen;
        }
        $empresa->idioma = "espanol";
        $empresa->codigo_reset_password = "null";
        $empresa->estado=false;
        $empresa->afiliacion="Pendiente";

        $empresaDao = new EmpresaDao(); 
        $existe = $empresaDao->verificarExistenciaEmpresa($empresa->correo);
        if ($existe) {
            $data = array(
                'estado'=>'fallo',
                'mensaje' => 'la empresa ya está registrada',
                'descripcion' => 'hay empresa registrada con el correo'
            );
            return $data;
        } else {
            return $empresaDao->registrar_empresa($empresa);
        }

    }

    function login_empresa($correo,$password)
    {
        $dao = new EmpresaDao();
        return $dao->login_empresa($correo,$password);
    }

    function listarEmpresas(){  
        $dao = new EmpresaDao();
        return $dao->listarEmpresas();
    }

    function listarEmpresa($id){ 
        $dao = new EmpresaDao();
        return $dao->listarEmpresa($id);
    }

    function put_empresa($nombre,$pais,$ciudad,$descripcion,$telefono,$correo,$imagen,$id)
    {
        $empresaDao = new EmpresaDao();
        return $empresaDao->put_empresa($nombre,$pais,$ciudad,$descripcion,$telefono,$correo,$imagen,$id);
        
    }

    function borrarEmpresa($id)
    {
        $empresaDao = new EmpresaDao();
        return $empresaDao->borrarempresa($id);
    }

    function actualizarAfiliacion($id,$afiliacion)
    {
        $dao = new EmpresaDao();
        if($afiliacion == 1){
            $afiliacion = "Aceptada";
            return $dao->actualizarAfiliacion($id,$afiliacion);
        }else{
            $afiliacion = "Rechazada";
            return $dao->actualizarAfiliacion($id,$afiliacion);
        }
        
    }

    function actualizarPassword($id,$oldpass,$newpass)
    {
        $dao = new EmpresaDao();
        return $dao->actualizarPassword($id,$oldpass,$newpass);
        
        
    }


    public function resetPassword($email_empresa)
    {
        $dao = new EmpresaDao();
        $empresa= $dao->verificarCorreoResetPassword($email_empresa);
        // $usuario tiene => 'usuario_id','nombre','apellidos','codigo_reset_password'
        if(is_object($empresa))
        {
            $fecha=date("Y-m-d");
            $hora="".(date('H')-4).":".date('i').":".date('s');
            $cifrado = md5($empresa->empresa_id." ".$fecha." ".$hora);
            //              usuario_id o empresa_id fecha_actual hora_actual

            //id+tipo V visitante E empresa+key
            $key = $empresa->empresa_id."E".substr($cifrado,0,7);
            //el primero es el id para sacarlo y actualizar la password
            //la key va la debe para ser comparada
            //una vez actualizada la password se debe borrar esta key a null

            $seGuardo= $dao->guardarCodigoResetPassword($key);
            if ($seGuardo)
            {
                $reset = new ResetPasswordEmpresa($empresa->nombre,strtoupper($key));
                try
                {
                    Mail::to($empresa->correo)->queue($reset);
                    $data=array(
                        'data'=>null,
                        'mensaje'=>'Se envio correo y guardo en la DB el codigo de recuperacion con exito.',
                        'estado'=>'exito'
                    );
                    return $data;
                }catch (\Exception $e){
                    $data=array(
                        'data'=>null,
                        'mensaje'=>'No se pudo enviar el correo de recuperacion.',
                        'estado'=>'error'
                    );
                    return $data;
                }

            }else{
                $data=array(
                    'data'=>null,
                    'mensaje'=>'No se pudo guardar el codigo en la DB.',
                    'estado'=>'error'
                );
                return $data;
            }

        }else{
            $data=array(
                'data'=>null,
                'mensaje'=>'No existe empresa con ese correo.',
                'estado'=>'error'
            );
            return $data;
        }
    }

   
}

?>
