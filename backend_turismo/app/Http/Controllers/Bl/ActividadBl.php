<?php

namespace App\Http\Controllers\Bl;
use \App\Models\Actividad;
use \App\Http\Controllers\Dao\ActividadDao;
use Intervention\Image\ImageManagerStatic as Image;

class ActividadBl 
{
    function post_actividad($nombre_esp,$nombre_eng,$ciudad,$pais,$horario,$precio,$descripcion_esp,$descripcion_eng,
    $imagen,$empresa_id,$cantidad,$lugar,$fecha_inicio,$repite,$dias)
    { 
        $actividad = new Actividad();
        $actividad->nombre_esp=$nombre_esp;
        $actividad->nombre_eng=$nombre_eng;
        $actividad->ciudad=$ciudad;
        $actividad->pais=$pais;
        $actividad->horario=$horario;
        $actividad->precio=$precio;
        $actividad->descripcion_esp=$descripcion_esp;
        $actividad->descripcion_eng=$descripcion_eng;
        if(!$imagen){
            $actividad->imagen = 'defaultact.png';
        }
        else{
            $image = str_replace('data:image/png;base64,', '', $imagen);
            $image = str_replace('data:image/jpg;base64,', '', $imagen);
            $image = str_replace('data:image/jpeg;base64,', '', $imagen);
            $image = str_replace(' ','+',$image);
            $subidaimagen = Image::make($image);
            $path = public_path().'/imagenes/actividades/';
            $nombre = str_replace(' ','',$nombre_esp);
            $nombreimagen = $nombre.time().'.jpg';
            $subidaimagen->resize(500,500)->save($path.$nombreimagen);
            $actividad->imagen = $nombreimagen;
        }
        $actividad->empresa_id=$empresa_id;
        $actividad->cantidad_visitantes=$cantidad;
        $actividad->lugar_encuentro=$lugar;
        $actividad->fecha_inicio=$fecha_inicio;
        $actividad->se_repite=$repite;
        $actividad->dias=$dias;
        $actividad->estado=false;
        $dao = new ActividadDao();
        return $dao->post_actividad($actividad);
    }
    
    function listarActividades()
    {
        $dao = new ActividadDao();
        return $dao->listarActividades();
    }
    
    function listarSinRegistro()
    {
        $dao = new ActividadDao();
        return $dao->listarSinRegistro();
    }
    
    function listarActividadesEmpresa($id)
    {
        $dao = new ActividadDao();
        return $dao->listarActividadesEmpresa($id);
    }
    
    function listarActividad($id)
    {
        $dao = new ActividadDao();
        return $dao->listarActividad($id);
    }

    function put_actividad($nombre_esp,$nombre_eng,$ciudad,$pais,$horario,$precio,$descripcion_esp,$descripcion_eng,
    $imagen,$empresa_id,$cantidad,$lugar,$fecha_inicio,$repite,$dias,$id,$id_empresa)
    {
        $dao = new ActividadDao();
        return $dao->put_actividad($nombre_esp,$nombre_eng,$ciudad,$pais,$horario,$precio,$descripcion_esp,$descripcion_eng,
        $imagen,$empresa_id,$cantidad,$lugar,$fecha_inicio,$repite,$dias,$id,$id_empresa);
    }
    function borrarActividad($id,$id_empresa)
    {
        $dao = new ActividadDao();
        return $dao->borrarActividad($id,$id_empresa);
    }


    function mostrarReservas($id,$payload)
    {
        if($id==0)
        {
            $data = array(
                'mensaje' => 'Problema con el id de la actividad',
                'descripcion' => 'actividad_id no puede ser cero',
                'estado'=>'fallo'
            );
            return response()->json($data);
        }else{
            if (isset($payload->usuario))
            {
                $data = array(
                    'mensaje' => 'Problema con el token',
                    'descripcion' => 'No puede acceder con un token de visitante.',
                    'estado'=>'error'
                );
                return response()->json($data);
            }
            else{
                $dao = new ActividadDao();
                return $dao->mostrarReservas($id);
            }

        }

    }
}

?>
