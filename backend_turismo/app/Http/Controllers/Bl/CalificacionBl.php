<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 21-10-19
 * Time: 04:49 PM
 */
namespace App\Http\Controllers\Bl;



use App\Http\Controllers\Dao\CalificacionDao;
use App\Models\Calificacion;

class CalificacionBl
{
    public function calificar($usuario_id,$actividad_id,$calificacion)
    {
        $calificacion_usuario=new Calificacion();
        $calificacion_usuario->calificacion=(int)$calificacion;
        $calificacion_usuario->actividad_id=(int)$actividad_id;
        $calificacion_usuario->usuario_id=(int)$usuario_id;

        $dao = new CalificacionDao();
        return $dao->calificar($calificacion_usuario);

    }
}
