<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 09-09-19
 * Time: 05:26 PM
 */
namespace App\Http\Controllers\Bl;
use App\Http\Controllers\Dao\EmpresaDao;
use App\Mail\ResetPassword;
use \App\Models\Usuario;
use \App\Http\Controllers\Dao\UsuarioDao;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\ImageManagerStatic as Image;
class UsuarioBl
{


    function registro( $nombre, $apellidos, $usuario_login,$correo, $nacimiento, $telefono,$password,$imagen,$idioma)
    {
        $usuario = new Usuario();
        $usuario->nombre = $nombre;
        $usuario->apellidos = $apellidos;
        $usuario->usuario = $usuario_login;
        $usuario->correo = $correo;
        $usuario->fecha_nacimiento = $nacimiento;
        $usuario->telefono = $telefono;
        $password = $password . 'turismo'; //la sal
        $usuario->password = hash('sha256', $password);
        if(!$imagen){
            $usuario->imagen = 'defaultuser.png';
        }
        else{
            $image = str_replace('data:image/png;base64,', '', $imagen);
            $image = str_replace('data:image/jpg;base64,', '', $imagen);
            $image = str_replace('data:image/jpeg;base64,', '', $imagen);
            $image = str_replace(' ','+',$image);
            $subidaimagen = Image::make($image);
            $path = public_path().'/imagenes/usuarios/';
            $nombreimagen = $usuario_login.time().'.jpg';
            $subidaimagen->resize(200,200)->save($path.$nombreimagen);
            $usuario->imagen = $nombreimagen;
        }
        $usuario->idioma = $idioma;
        $usuario->codigo_reset_password = "null";
        //$usuario->imagen = $nombreimagen;
        $usuario->estado = false;

        $usuarioDao = new UsuarioDao();
        
        $existe = $usuarioDao->verificarExistenciaUsuario($usuario->usuario);

        if ($existe) {
            $data = array(
                'mensaje' => 'el usuario ya existe',
                'descripcion' => 'hay un usuario con ese nombre de usuario',
                'estado'=>'error'
            );
            return $data;
        } else {

            return $usuarioDao->registrar($usuario);
        }
    }

    public function mostrarUsuario($id)
    {
        if($id!=0)
        {
            $usuarioDao = new UsuarioDao();
            $user=$usuarioDao->mostrarPerfil($id);
            $object = (object) [
                "usuario_id"=> $user->usuario_id,
                "nombre"=> $user->nombre,
                "apellidos"=> $user->apellidos,
                "usuario"=> $user->usuario,
                "correo"=> $user->correo,
                "telefono"=> $user->telefono,
                "fecha_nacimiento"=> $user->fecha_nacimiento,
                "imagen"=>$user->imagen

            ];
            $data = array(
                'data'=> $object,
                'descripcion' => 'exito',
                'estado'=>'exito'
            );

        }else{
            $data = array(
                'data'=>null,
                'descripcion' => 'usuario con id igual a 0',
                'estado'=>'error'
            );

        }
        return response()->json($data);
    }

    public function actualizarPerfil($id,$nombre, $apellidos, $usuario_login,$correo, $nacimiento, $telefono,$imagen)
    {
        $usuarioDao=new UsuarioDao();
        $usuario=$usuarioDao->mostrarPerfil($id);
        $usuario->nombre = $nombre;
        $usuario->apellidos = $apellidos;
        $usuario->usuario = $usuario_login;
        $usuario->correo = $correo;
        $usuario->fecha_nacimiento = $nacimiento;
        $usuario->telefono = $telefono;
        if($imagen){
            $image = str_replace('data:image/png;base64,', '', $imagen);
            $image = str_replace('data:image/jpg;base64,', '', $imagen);
            $image = str_replace('data:image/jpeg;base64,', '', $imagen);
            $image = str_replace(' ','+',$image);
            $subidaimagen = Image::make($image);
            $path = public_path().'/imagenes/usuarios/';
            $nombreimagen = $usuario_login.time().'.jpg';
            $subidaimagen->resize(200,200)->save($path.$nombreimagen);
            $usuario->imagen = $nombreimagen;
        }
        return $usuarioDao->actualizarPerfil($usuario);
    }

    public function actualizarPassword($usuario_id,$actual_password,$nueva_password)
    {
        $password1 = $actual_password . 'turismo';
        $actual_password_encriptada = hash('sha256', $password1);
        $password2 = $nueva_password . 'turismo';
        $nueva_password_encriptada = hash('sha256', $password2);
        $usuarioDao=new UsuarioDao();
        return $usuarioDao->actualizarPassword($usuario_id,$actual_password_encriptada,$nueva_password_encriptada);
    }

    public function actualizarIdioma($usuario_id,$idioma)
    {
        $usuarioDao=new UsuarioDao();
        return $usuarioDao->actualizarIdioma($usuario_id,$idioma);
    }

    public function verMisReservas($usuario_id)
    {
        $usuarioDao=new UsuarioDao();
        return $usuarioDao->verMisReservas($usuario_id);
    }

    public function verMiCompras($usuario_id,$reserva_id)
    {
        $usuarioDao=new UsuarioDao();
        return $usuarioDao->verMiCompra($usuario_id,$reserva_id);
    }


    public function resetPassword($nombre_usuario)
    {
        $usuarioDao=new UsuarioDao();
        $usuario= $usuarioDao->verificarNombreUsuarioResetPassword($nombre_usuario);
        // $usuario tiene => 'usuario_id','nombre','apellidos','codigo_reset_password'
        if(is_object($usuario))
        {
            $fecha=date("Y-m-d");
            $hora="".(date('H')-4).":".date('i').":".date('s');
            $cifrado = md5($usuario->usuario_id." ".$fecha." ".$hora);
            //              usuario_id o empresa_id fecha_actual hora_actual

            //id+tipo V visitante E empresa+key
            $key = $usuario->usuario_id."V".substr($cifrado,0,7);
            //el primero es el id para sacarlo y actualizar la password
            //la key va la debe para ser comparada
            //una vez actualizada la password se debe borrar esta key a null

            $seGuardo= $usuarioDao->guardarCodigoResetPassword($key);
            if ($seGuardo)
            {
                $reset = new ResetPassword($usuario->nombre." ".$usuario->apellidos,strtoupper($key));
                try
                {
                    Mail::to($usuario->correo)->queue($reset);
                    $data=array(
                        'data'=>null,
                        'mensaje'=>'Se envio correo y guardo en la DB el codigo de recuperacion con exito.',
                        'estado'=>'exito'
                    );
                    return $data;
                }catch (\Exception $e){
                    $data=array(
                        'data'=>null,
                        'mensaje'=>'No se pudo enviar el correo de recuperacion.',
                        'estado'=>'error'
                    );
                    return $data;
                }

            }else{
                $data=array(
                    'data'=>null,
                    'mensaje'=>'No se pudo guardar el codigo en la DB.',
                    'estado'=>'error'
                );
                return $data;
            }

        }else{
            $data=array(
                'data'=>null,
                'mensaje'=>'No existe usuario con ese nombre de usuario.',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function obtenerTipo($codigo)
    {
        $longitud = strlen($codigo);
        $aux=0;

        $numeros=['V','E'];
        $tipo='';
        while ($aux<$longitud)
        {
            $letra = substr($codigo,$aux,1);
            if (in_array($letra,$numeros))
            {
                $tipo=$letra;
                break;
            }

            $aux = $aux+1;
        }
        return $tipo;

    }
    public function obtenerId($codigo)
    {
        $longitud = strlen($codigo);
        $aux=0;

        $numeros=['V','E'];
        $tipo='';
        while ($aux<$longitud)
        {
            $letra = substr($codigo,$aux,1);
            if (in_array($letra,$numeros))
            {
                $tipo=$letra;
                break;
            }

            $aux = $aux+1;
        }
        return substr($codigo,0,$aux);

    }
    public function verificarCodigo($codigo)
    {
        $id=(int)$this->obtenerId($codigo);
        $tipo=$this->obtenerTipo($codigo);
        //return $tipo." ".$id;
        $usuario_dao = new UsuarioDao();
        $empresa_dao = new EmpresaDao();
        if ($tipo=='V')
        {
            $usuario=$usuario_dao->obtenerCodigo($id);
            //return $usuario;
            if (is_object($usuario) && $usuario->codigo_reset_password==$codigo)
            {
                $data=array(
                    'data'=>$usuario,
                    'tipo'=>$tipo,
                    'mensaje'=>'El codigo ingresado es correcto.',
                    'estado'=>'exito'
                );
                return $data;
            }else{
                $data=array(
                    'data'=>null,
                    'mensaje'=>'El codigo no existe.',
                    'estado'=>'error'
                );
                return $data;
            }
        }else{
            if ($tipo=='E')
            {
                $empresa=$empresa_dao->obtenercodigo($id);
                if (is_object($empresa) && $empresa->codigo_reset_password==$codigo)
                {
                    $data=array(
                        'data'=>$empresa,
                        'tipo'=>$tipo,
                        'mensaje'=>'El codigo ingresado es correcto.',
                        'estado'=>'exito'
                    );
                    return $data;
                }else{
                    $data=array(
                        'data'=>null,
                        'mensaje'=>'El codigo no existe.',
                        'estado'=>'error'
                    );
                    return $data;
                }
            }else{
                $data=array(
                    'data'=>null,
                    'mensaje'=>'Codigo incorrecto.',
                    'estado'=>'error'
                );
                return $data;
            }
        }
    }

    public function ponerNuevaPassword($id,$tipo,$password)
    {
        $usuario_dao = new UsuarioDao();
        $empresa_dao = new EmpresaDao();
        if ($tipo=='V')
        {
            $password = $password . 'turismo'; //la sal
            $password = hash('sha256', $password);
            $seGuardo=$usuario_dao->ponerNuevaPassword($id,$password);
            if ($seGuardo)
            {
                $data=array(
                    'data'=>null,
                    'mensaje'=>'Se pudo actualizar la password.',
                    'estado'=>'exito'
                );
                return $data;
            }else{
                $data=array(
                    'data'=>null,
                    'mensaje'=>'No se pudo actualizar la password.',
                    'estado'=>'error'
                );
                return $data;
            }

        }else{
            if ($tipo=='E')
            {
                $password=$password.'software-1'; //la sal
                $password=hash('sha256',$password);
                $seGuardo=$empresa_dao->ponerNuevaPassword($id,$password);
                if ($seGuardo)
                {
                    $data=array(
                        'data'=>null,
                        'mensaje'=>'Se pudo actualizar la password.',
                        'estado'=>'exito'
                    );
                    return $data;
                }else{
                    $data=array(
                        'data'=>null,
                        'mensaje'=>'No se pudo actualizar la password.',
                        'estado'=>'error'
                    );
                    return $data;
                }

            }else{
                $data=array(
                    'data'=>null,
                    'mensaje'=>'Tipo es incorrecto.',
                    'estado'=>'error'
                );
                return $data;
            }
        }
    }

}
