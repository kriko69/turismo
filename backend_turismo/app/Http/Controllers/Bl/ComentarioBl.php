<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 21-10-19
 * Time: 03:47 PM
 */
namespace App\Http\Controllers\Bl;

use App\Http\Controllers\Dao\ComentarioDao;
use App\Models\Calificacion;
use App\Models\Comentario;

class ComentarioBl
{
    public function comentar($usuario_id,$actividad_id,$comentario,$calificacion)
    {

        $comentario_usuario = new Comentario();
        $comentario_usuario->comentario=$comentario;
        $comentario_usuario->usuario_id=$usuario_id;
        $comentario_usuario->actividad_id=(int)$actividad_id;
        //return response()->json($comentario_usuario);

        $calificacion_usuario=new Calificacion();
        $calificacion_usuario->calificacion=(int)$calificacion;
        $calificacion_usuario->actividad_id=(int)$actividad_id;
        $calificacion_usuario->usuario_id=(int)$usuario_id;

        $dao = new ComentarioDao();
        return $dao->comentar($comentario_usuario,$calificacion_usuario);

    }

    public function verComentarios($actividad_id)
    {


        $dao = new ComentarioDao();
        return $dao->verComentarios($actividad_id);

    }
}
