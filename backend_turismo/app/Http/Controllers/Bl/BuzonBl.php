<?php

namespace App\Http\Controllers\Bl;
use \App\Http\Controllers\Dao\BuzonDao;

class BuzonBl 
{
    public function buzonCalcom($id)
    {
        $dao = new BuzonDao();
        return $dao->buzonCalcom($id);
    }

    public function buzonCompra($id)
    {
        $dao = new BuzonDao();
        return $dao->buzonCompra($id);
    }

}

?>