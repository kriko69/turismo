<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles_usuarios', function (Blueprint $table) {
            $table->increments('rol_usuario_id');
            $table->integer('usuario_id')->unsigned();
            $table->integer('rol_id')->unsigned();

            $table->foreign( 'usuario_id')->references('usuario_id')->on('usuarios');
            $table->foreign('rol_id')->references('rol_id')->on('roles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles_usuarios');
    }
}
