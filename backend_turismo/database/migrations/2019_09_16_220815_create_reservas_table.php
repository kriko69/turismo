<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {
            $table->bigIncrements('reserva_id');
            $table->integer('cantidad_reservas');
            $table->double('precio_total');
            $table->date('fecha');
            $table->boolean('estado')->default(false);
            $table->integer('usuario_id')->unsigned();
            $table->integer('actividad_id')->unsigned();
            $table->foreign( 'usuario_id')->references('usuario_id')->on('usuarios');
            $table->foreign('actividad_id')->references('actividad_id')->on('actividades');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
    }
}
