<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('empresa_id');
            $table->string('nombre');
            $table->string('pais');
            $table->string('ciudad');
            $table->string('descripcion');
            $table->bigInteger('telefono');
            $table->string('correo');
            $table->string('password');
            $table->string('afiliacion');
            $table->string('codigo_reset_password')->default(null);
            $table->integer('calificacion')->default(-1);
            $table->string('imagen')->default('defaultent.png');
            $table->string('idioma')->default('espanol');
            $table->boolean('estado')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
