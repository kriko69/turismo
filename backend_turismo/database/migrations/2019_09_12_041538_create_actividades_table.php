<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActividadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividades', function (Blueprint $table) {
            $table->increments('actividad_id');
            $table->string('nombre_esp');
            $table->string('nombre_eng');
            $table->string('ciudad');
            $table->string('pais');
            $table->time('horario');
            $table->double('precio');
            $table->string('descripcion_esp');
            $table->string('descripcion_eng');
            $table->string('imagen');
            $table->integer('empresa_id')->unsigned();
            $table->integer('cantidad_visitantes');
            $table->string('lugar_encuentro');
            $table->date('fecha_inicio');
            $table->boolean('se_repite');
            $table->string('dias');
            $table->integer('calificacion')->default(-1);
            $table->boolean('estado')->default(false);
            $table->foreign('empresa_id')->references('empresa_id')->on('empresas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividades');
    }
}
