<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuposDiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cupos_dias', function (Blueprint $table) {
            $table->bigIncrements('cupo_dia_id');
            $table->integer('actividad_id')->unsigned();
            $table->integer('cupos_disponibles');
            $table->date('fecha_actividad');
            $table->string('dia_actividad');
            $table->foreign('actividad_id')->references('actividad_id')->on('actividades');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cupos_dias');
    }
}
