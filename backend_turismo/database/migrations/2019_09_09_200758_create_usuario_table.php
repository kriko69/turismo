<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('usuario_id');
            $table->string('nombre');
            $table->string('apellidos');
            $table->string('usuario');
            $table->string('correo');
            $table->bigInteger('telefono');
            $table->date('fecha_nacimiento');
            $table->string('password');
            $table->string('imagen')->default('defaultuser.png');
            $table->string('idioma');
            $table->string('codigo_reset_password')->default(null);
            $table->string('codigo_compra')->default(null);
            $table->boolean('estado')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
