<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios', function (Blueprint $table) {
            $table->bigIncrements('comentario_id');
            $table->string('comentario');
            $table->boolean('estado')->default(false);
            $table->integer('usuario_id')->unsigned();
            $table->integer('actividad_id')->unsigned();
            $table->foreign( 'usuario_id')->references('usuario_id')->on('usuarios');
            $table->foreign('actividad_id')->references('actividad_id')->on('actividades');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentarios');
    }
}
