<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Helpers\JwtAuth;

class BuzonTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    
    public function testBuzon(){
        
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenEmpresa();
        $response = $this->withHeaders([
            'Authorization'=>$token1,
            'Content-Type'=>'application/json'
        ])->get('/api/buzon');
        $response->assertStatus(200);
        $response->assertJson([ 
            'estado'=>'exito',
        ]);
    }
    public function testBuzonFallandoElToken(){

        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenEmpresa();
        $response = $this->withHeaders([
            'Authorization'=>$token1.'no',
            'Content-Type'=>'application/json'
        ])->get('/api/buzon');
        $response->assertStatus(200);
        $response->assertJson([ 
            'estado'=>'error',
            'mensaje'=>'el token es incorrecto'
        ]);
    }
}
