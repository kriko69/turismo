<?php

namespace Tests\Feature;

use App\Helpers\JwtAuth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CorreoTest extends TestCase
{

    /**
     * A basic feature test example.
     *
     * @return void
     * @test
     */
    public function testEnviarCorreoRechazo()
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenUser();
        $response = $this->withHeaders([
            'Authorization'=>$token1
        ])
            ->json('POST','api/empresa/1/rechazarSolicitud',[]);

        $response->assertStatus(200);
    }
    public function testEnviarCorreo()
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenUser();
        $response = $this->withHeaders([
            'Authorization'=>$token1
        ])
        ->json('POST','api/empresa/1/confirmarSolicitud',[]);

        $response->assertStatus(200);
    }

    /**
     * @test
     */

/*

    public function confirmarData()
    {
        return [
            "empresa1"=>[3],
            "empresa2"=>[2],
            "empresa3"=>[0],
            "empresa4"=>[1],
            "empresa5"=>[5]
        ];
    }
*//*
    public function rechazararData()
    {
        return [
            "empresa1"=>[3],
            "empresa2"=>[2],
            "empresa3"=>[0],
            "empresa4"=>[1],
            "empresa5"=>[5]
        ];
    }*/
}
