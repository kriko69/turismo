<?php

namespace Tests\Feature;

use App\Helpers\JwtAuth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ComentarioTest extends TestCase
{
    public $token='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQsInVzdWFyaW8iOiJwZXBlIiwicm9sZXMiOlsyXSwidHlwZSI6InZlcmlmaWNhY2lvbiIsImlhdCI6MTU3MTkzODgyMiwiZXhwIjoxNTcxOTQyNDIyfQ.DvBjd52-wHxl-ubDX0Xv-NpvawI_1-xf3jYNQz2onHo';


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testComentarCalificar()
    {
        //falta imagen
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenUser();
        $response = $this->withHeaders([
            'Content-Type'=>'application/json',
            'Authorization'=> $token1
        ])->json('POST','/api/actividad/1/comentar-calificar',[
            "comentario"=>"me gusto mucho",
            "calificacion"=>3
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'estado'=>'exito'
            ]);
    }

    public function testVerComentarios()
    {
        //falta imagen
        $response = $this->withHeaders([
            'Content-Type'=>'application/json'
        ])->json('GET','/api/actividad/1/comentarios');

        $response->assertStatus(200)
            ->assertJson([
                'estado'=>'exito'
            ]);
    }


}
