<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Helpers\JwtAuth;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmpresaTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoginEmpresaBien()
    {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('POST','/api/login_empresa',[
            'correo' => 'kevin.bonilla.kbs17@gmail.com',
            'password'=> '123abc'
        ]);
        $response->assertStatus(200)->assertJson(['estado'=>'Tokens creados con exito']);
    }

    public function testLoginEmpresaMal()
    {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('POST','/api/login_empresa',[
            'correo' => 'kevin.bonilla@gmail.com',
            'password'=> '123'
        ]);
        $response->assertJson(['estado'=>'error']);
    }

    public function testLoginEmpresaCampoVacio()
    {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('POST','/api/login_empresa',[
            'correo' => 'kevin.bonilla.kbs17@gmail.com',
            'password'=> ''
        ]);
        $response->assertJson(['estado'=>'fallo',
        'descripcion'=>'algun parametro en vacio']);
    }
/*
    public function testRegistroEmpresa()
    {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('POST','/api/registrar_empresa',[
            "nombre"=> "Prueba",
            "pais"=> "bolivia",
            "ciudad"=> "la paz",
            "descripcion"=> "buena",
            "telefono"=> "715203654",
            "correo"=> "kevin.bonilla@gmail.com",
            "password"=> "123abc",
            "imagen"=>""
        ]);
        $response->assertStatus(200);
        $response->assertJson(['mensaje'=>'Empresa mando afiliación con exito',
        'estado'=>'exito',]);
    }*/

    public function testRegistroEmpresaPreviamenteRegistrada()
    {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('POST','/api/registrar_empresa',[
            "nombre"=> "Prueba",
            "pais"=> "bolivia",
            "ciudad"=> "la paz",
            "descripcion"=> "buena",
            "telefono"=> "715203654",
            "correo"=> "kevin.bonilla.kbs17@gmail.com",
            "password"=> "123abc",
            "imagen"=>""
        ]);
        $response->assertStatus(200);
        $response->assertJson(['estado'=>'fallo',
        'mensaje' => 'la empresa ya está registrada',]);
    }

    public function testRegistroEmpresaCampoVacio()
    {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('POST','/api/registrar_empresa',[
            "nombre"=> "Prueba",
            "pais"=> "bolivia",
            "ciudad"=> "la paz",
            "descripcion"=> "",
            "telefono"=> "",
            "correo"=> "kevin.bonilla.kbs17@gmail.com",
            "password"=> "123abc",
            "imagen"=>""
        ]);
        $response->assertStatus(200);
        $response->assertJson([
            'estado'=>'fallo',
            'descripcion'=>'algun parametro en blanco',]);
    }

    public function testObtenerEmpresas()
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenEmpresa();
        $response = $this->withHeaders([
            'Authorization'=>$token1,
            'Content-Type'=>'application/json'
        ])->get('/api/get_empresas');
        $response->assertStatus(200);
        $response->assertJson([ 
            'estado'=>'exito',
            'descripcion'=>'respuesta generada con exito',
        ]);
    }

    public function testEditarEmpresa()
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenEmpresa();
        $response = $this->withHeaders([
            'Authorization'=>$token1,
            'Content-Type'=>'application/json'
        ])->json('PUT','/api/actualizar_empresa',[
            "nombre"=> "Real",
            "pais"=> "bolivia",
            "ciudad"=> "la paz",
            "descripcion"=> "buena",
            "telefono"=> "72030450",
            "correo"=> "kevin.bonilla.kbs17@gmail.com",
            "imagen"=>""
        ]);
        $response->assertStatus(200);
        $response->assertJson([
            "descripcion"=> "Empresa actualizada con exito",
            "estado"=> "exito"]);
    }

    
}
