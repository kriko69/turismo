<?php

namespace Tests\Feature;

use App\Helpers\JwtAuth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UsuarioTest extends TestCase
{

    public static $token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMsInVzdWFyaW8iOiJrYnMxNyIsInJvbGVzIjpbMSwyXSwidHlwZSI6InZlcmlmaWNhY2lvbiIsImlhdCI6MTU3MzE1Nzc4MSwiZXhwIjoxNTczMTYxMzgxfQ.Q117O-Qq5t1rzcG2n7ZYo9BCwCkfbrBePmZV6P3Hsxs";


    public function obtenerToken($usuario,$password)
    {
        $jwt = new JwtAuth();
        if ($usuario==null && $password==null)
        {
            $password='master69';
            $usuario='master';
            $password=$password.'turismo';
            $password=hash('sha256',$password);
            $data=$jwt->signUp($usuario,$password);
            $token=[$data=>'Token-Verificacion',];
            return $token;
        }else{

            $password=$password.'turismo';
            $password=hash('sha256',$password);
            $data=$jwt->signUp($usuario,$password);
            return 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQsInVzdWFyaW8iOiJwZXBlIiwicm9sZXMiOlsyXSwidHlwZSI6InZlcmlmaWNhY2lvbiIsImlhdCI6MTU3MTIyNjgxNSwiZXhwIjoxNTcxMjMwNDE1fQ.ihV86CDONodS69Hb3ECwJ4yQC__Hzu-tEt5MhtN2znc';

        }
    }
    /**
     * A basic feature test example.
     *
     * @return void
     *
     */
     /*
    public function testRegistro()//$nombre, $apellidos, $usuario_login,$correo, $nacimiento, $telefono,$password,$idioma
    {
        //falta imagen
        $response = $this->withHeaders([
            'Content-Type'=>'application/json'
        ])->json('POST','/api/registrar',[
            "nombre"=>"chris",
            "apellidos"=>"jimenez",
            "usuario"=>"user1005",
            "correo"=>"chris@gmail.com",
            "fecha_nacimiento"=>"1997-07-14",
            "telefono"=>2435036,
            "password"=>"masterPrueba6925",
            "idioma"=>"espanol"
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'estado'=>'exito'
            ]);

    }*/


    public function testLogin() //$usuario,$password
    {
        $response = $this->withHeaders([
            'Content-Type'=>'application/json'
        ])->json('POST','/api/login',[
            "usuario"=>'pepe',
            "password"=>'master69'
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'estado'=>'Tokens creados con exito'
            ]);
    }

    
    public function testPerfil()
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenUser();
        $response = $this->withHeaders([
            'Authorization'=>$token1
        ])->json('GET','/api/perfil');

        $response->assertStatus(200)
            ->assertJson([
                'estado'=>'exito'
            ]);
    }


    
    public function testActualizarPerfil() //$nombre, $apellidos, $usuario_login,$correo, $nacimiento, $telefono,$imagen
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenUser();
        $response = $this->withHeaders([
            'Authorization'=>$token1,
            'Content-Type'=>'application/json'
        ])->json('PUT','/api/perfil',[
            "nombre"=>"chris",
	"apellidos"=>"jimenez",
	"usuario"=>"pepe",
	"correo"=>"chris@gmail.com",
	"fecha_nacimiento"=>"1997-07-14",
	"telefono"=>2435036

        ]);

        $response->assertStatus(200)
            ->assertJson([
                'estado'=>'exito'
            ]);
    }


    
    public function testActualizarPassword() //$actual_password,$nueva_password
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenUser();
        $response = $this->withHeaders([
            'Authorization'=>$token1,
            'Content-Type'=>'application/json'
        ])->json('PUT','/api/perfil/password',[
            "actual_password"=>'master69',
	        "nueva_password"=>'master69'
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'estado'=>'exito'
            ]);
    }


    public function testActualizarIdioma() //$idioma
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenUser();
        $response = $this->withHeaders([
            'Authorization'=>$token1,
            'Content-Type'=>'application/json'
        ])->json('PUT','/api/perfil/idioma',[
            "idioma"=>'ingles'
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'estado'=>'exito'
            ]);
    }


    
    public function testVerMisReservas()
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenUser();
        $response = $this->withHeaders([
            'Authorization'=>$token1
        ])->json('GET','/api/usuario/reservas');

        $response->assertStatus(200)
            ->assertJson([
                'estado'=>'exito'
            ]);
    }

    //proveedores de datos


    public function registroData()
    {
        return [
            'usuario1'=>['chris','jimenez','chris123','chris@gmail.com','1998-10-02',24536586,'chris12345','espanol'],
            'usuario2'=>['luis','perez','luchexxx','luis@gmail.com','1998-10-02',42452452,'luis12345','espanol'],
            'usuario3'=>['pedro','bonilla','pepe999','pedro@gmail.com','1998-10-02',876786,'pedro12345','ingles'],
            'usuario4'=>['jimena','rodriguez','jimeme','jimena@gmail.com','1998-10-02',782782,'jimena12345','espanol'],
            'usuario5'=>['vania','fernandez','vanini','vania@gmail.com','1998-10-02',0,'cvania12345','ingles']
        ];
    }


    public function loginData()
    {
        return [
            'login1'=>['master','master69'],
            'login2'=>['pepe','pepe69'],
            'login3'=>['pedrido','pedro69'],
            'login4'=>['kevs','kev123'],
            'login5'=>['pepe','pepe69'],
        ];
    }


    public function actualizarPerfilData()
    {
        return [
            'actualizarUsuario1'=>['christian','torrez','chris','luis@gmail.com','1998-09-02',6969696,'default.jpg'],
            'actualizarUsuario2'=>['luisandro','saavedra','luch12','chris@gmail.com','1998-10-02',7575,'default.jpg'],
            'actualizarUsuario3'=>['pedreso','hurtado','pp123','jime@gmail.com','1998-12-02',11122,'default.jpg'],
            'actualizarUsuario4'=>['jime','jimenez','jimena','pedro@gmail.com','1998-11-02',9998887,'default.jpg'],
            'actualizarUsuario5'=>['vanina','perez','vani','vania@gmail.com','1998-10-02',1142424,'default.jpg']
        ];
    }

    public function actualizarPasswordData()
    {
        return [
            'password1'=>['master69','master14'],
            'password2'=>['kill123','kill666'],
            'password3'=>['123abc','lafayete'],
            'password4'=>['kev123','kev555'],
            'password5'=>['pepe69','pepeElCrack'],
        ];
    }


    public function actualizarIdiomaData()
    {
        return [
            'idioma1'=>['ingles'],
            'idioma2'=>['espanol'],
            'idioma3'=>['espanol'],
            'idioma4'=>['ingles'],
            'idioma5'=>['ingles']
        ];
    }




}
