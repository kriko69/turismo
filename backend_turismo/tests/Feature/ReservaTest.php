<?php

namespace Tests\Feature;

use App\Helpers\JwtAuth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReservaTest extends TestCase
{
    /*public function obtenerToken($usuario,$password)
    {
        $jwt = new JwtAuth();
        if ($usuario==null && $password==null)
        {
            $password='master69';
            $usuario='master';
            $password=$password.'turismo';
            $password=hash('sha256',$password);
            $data=$jwt->signUp($usuario,$password);
            return $data->Token-Verificacion;
        }else{
            $password=$password.'turismo';
            $password=hash('sha256',$password);
            $data=$jwt->signUp($usuario,$password);
            return $data->Token-Verificacion;

        }
    }*/

    public static $token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMsInVzdWFyaW8iOiJrYnMxNyIsInJvbGVzIjpbMSwyXSwidHlwZSI6InZlcmlmaWNhY2lvbiIsImlhdCI6MTU3MzE1Nzc4MSwiZXhwIjoxNTczMTYxMzgxfQ.Q117O-Qq5t1rzcG2n7ZYo9BCwCkfbrBePmZV6P3Hsxs";

    /**
     * A basic feature test example.
     *
     * @return void
     * @test
     */
    public function verificarDisponibilidad()
    {
        $response = $this
            ->json('POST','/api/actividad/1/verificar/reserva',[
                'cantidad_visitantes'=>1,
                'fecha'=>'2019-11-25'
            ]);

        $response->assertStatus(200)
        ->assertJson([
            'estado'=>'exito'
        ]);
    }


    /**
     * @test
     */

    public function reservar()
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenUser();
        $response = $this->withHeaders([
            'Content-Type'=>'application/json',
            'Authorization'=>$token1
        ])
            ->json('POST','/api/actividad/1/reservar',[
                "cantidad_reservas"=>1,
                "precio_total"=>550,
                "fecha"=>"2019-11-25"
            ]);

        $response->assertStatus(200)
            ->assertJson([
                'estado'=>'exito'
            ]);
    }

    public function testVerReserva()
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenUser();
        $response = $this->withHeaders([
            'Content-Type'=>'application/json',
            'Authorization'=>$token1
        ])
            ->json('GET','/api/usuario/reservas/1');

        $response->assertStatus(200)
            ->assertJson([
                'estado'=>'exito'
            ]);
    }

    public function reservarData()
    {
        return [
            'reserva1'=>[2,5,340,'2019-11-02'],
            'reserva2'=>[3,3,200,'2019-10-02'],
            'reserva3'=>[1,12,550,'2019-12-02'],
            'reserva4'=>[0,0,0,'2019-11-14'],
            'reserva5'=>[7,2,400,'2019-11-17']
        ];
    }
}
