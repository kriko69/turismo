<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Helpers\JwtAuth;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ActividadesTest extends TestCase
{
   
    /**
     * A basic test example.
     *
     * @return void
     */
    /*
     public function testRegistroActividad()
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenEmpresa();
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization'=> $token1,
        ])->json('POST','/api/actividad',[
            "nombre_esp"=>"Salto",
			"nombre_eng"=>"Jumping",
			"ciudad"=>"La Paz",
			"pais"=>"Bolivia",
			"horario"=>"11:00:00",
			"precio"=>"150",
			"descripcion_esp"=>"bien",
			"descripcion_eng"=>"fine",
			"imagen"=>"",
			"cantidad_visitantes"=>"100",
			"lugar_encuentro"=>"Prado",
			"fecha_inicio"=>"2019-10-30",
			"se_repite"=>"1",
			"dias"=>"M"
        ]);
        $response->assertStatus(200);
        $response->assertJson(['mensaje'=>'Actividad creada con exito',
        'descripcion'=>'exito',
        'estado'=> 'exito']);
    }*/

    public function testRegistroActividadCampoVacio()
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenEmpresa();
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization'=> $token1,
        ])->json('POST','/api/actividad',[
            "nombre_esp"=>"",
			"nombre_eng"=>"",
			"ciudad"=>"La Paz",
			"pais"=>"Bolivia",
			"horario"=>"11:00:00",
			"precio"=>"150",
			"descripcion_esp"=>"bien",
			"descripcion_eng"=>"fine",
			"imagen"=>"",
			"cantidad_visitantes"=>"100",
			"lugar_encuentro"=>"Prado",
			"fecha_inicio"=>"2019-10-30",
			"se_repite"=>"1",
			"dias"=>"martes"
        ]);
        $response->assertStatus(200);
        $response->assertJson([
             'estado' => 'fallo',
              'mensaje' => 'Actividad no creada',]);
    }

    public function testObtenerActividades()
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenEmpresa();
        $response = $this->withHeaders([
            'Authorization'=>$token1
        ])->get('/api/actividades');
        $response->assertStatus(200);
        $response->assertJson([
            'estado'=>'exito',
            'descripcion'=>'respuesta generada con exito',
        ]);
    }

    public function testObtenerActividadesSinRegistrarse()
    {
        $response = $this->get('/api/actividades/sin/registro');
        $response->assertStatus(200);
        $response->assertJson([
            'estado'=>'exito',
            'descripcion'=>'respuesta generada con exito',
        ]);
    }
    public function testEditarActividad()
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenEmpresa();
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization'=> $token1,
        ])->json('PUT','/api/actividad/2',[
            "nombre_esp"=>"Salto",
			"nombre_eng"=>"Jumping",
			"ciudad"=>"La Paz",
			"pais"=>"Bolivia",
			"horario"=>"11:00:00",
			"precio"=>"150",
			"descripcion_esp"=>"bien",
			"descripcion_eng"=>"fine",
			"imagen"=>"",
			"cantidad_visitantes"=>"100",
			"lugar_encuentro"=>"Prado",
			"fecha_inicio"=>"2019-10-30",
			"se_repite"=>"1",
			"dias"=>"M"
        ]);
        $response->assertStatus(200);
        $response->assertJson([
            'descripcion'=>'Actividad actualizada con exito',
            'estado'=>'exito']); 
    }

    public function testEditarActividadCampoVacio()
    {
        $jwt = new JwtAuth();
        $token1 = $jwt->getTokenEmpresa();
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization'=> $token1,
        ])->json('PUT','/api/actividad/4',[
            "nombre_esp"=>"Camino de la muerte",
			"nombre_eng"=>"Death Road",
			"ciudad"=>"",
			"pais"=>"",
			"horario"=>"11:00:00",
			"precio"=>"150",
			"descripcion_esp"=>"bien",
			"descripcion_eng"=>"fine",
			"imagen"=>"",
			"cantidad_visitantes"=>"100",
			"lugar_encuentro"=>"Centro",
			"fecha_inicio"=>"2019-10-30",
			"se_repite"=>"1",
			"dias"=>"lunes"
        ]);
        $response->assertOk();
        $response->assertJson([
            'estado'=>'fallo',
            'mensaje'=>'Datos incorrectos algun dato en null']);
    }
}
