<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::group(['middleware' => 'cors'], function(){
Route::get('/prueba',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@decodificarToken']);

Route::post('/validar','UsuarioController@pruebaValidacion');

    Route::post('/registrar','UsuarioController@registrar');
    Route::post('/login','UsuarioController@login');
    Route::get('/perfil',[
        'middleware'=>'JWT',
        'uses'=>'UsuarioController@perfil']);
    Route::put('/perfil',[
        'middleware'=>'JWT',
        'uses'=>'UsuarioController@actualizarPerfil']);
        Route::put('/perfil/password',[
            'middleware'=>'JWT',
            'uses'=>'UsuarioController@actualizarPassword']);
        Route::put('/perfil/idioma',[
            'middleware'=>'JWT',
            'uses'=>'UsuarioController@actualizarIdioma']);
    Route::get('/usuario/reservas',[
        'middleware'=>'JWT',
        'uses'=>'UsuarioController@verMisReservas']);

    Route::get('/usuario/reservas/{reserva_id}',[
        'middleware'=>'JWT',
        'uses'=>'UsuarioController@verMiCompra']);

    Route::post('/obtener/codigo/usuario','UsuarioController@resetPassword');

    Route::post('/verficar/codigo','UsuarioController@verificarCodigo');

    Route::post('/reset/password','UsuarioController@ponerNuevaPassword');

    //Registro de empresa
    Route::post('/registrar_empresa','EmpresaController@registrar_empresa');
    Route::post('/login_empresa','EmpresaController@login_empresa');




    Route::post('/empresa/{id}/confirmarSolicitud',[
        'middleware'=>'JWT',
        'uses'=>'CorreoController@enviarCorreo']);

    Route::post('/empresa/{id}/rechazarSolicitud',[
        'middleware'=>'JWT',
        'uses'=>'CorreoController@enviarCorreoRechazo']);




    //Listado empresa
    Route::get('/get_empresas',[
        'middleware'=>'JWT',
        'uses'=>'EmpresaController@get_empresas']
        
    );
    
    Route::get('/get_empresa',[
        'middleware'=>'JWT',
        'uses'=>'EmpresaController@get_empresa']
    );
    
    Route::get('/get_empresa/{id}','EmpresaController@perfil_empresa');

    // PUT DELETE Empresa

    Route::put('/actualizar_empresa',[
        'middleware'=>'JWT',
        'uses'=>'EmpresaController@put_empresa']
        
    );

    Route::delete('/borrar_empresa/{id}',[
        'middleware'=>'JWT',
        'uses'=>'EmpresaController@delete_empresa']
        
    );

    Route::put('/actualizar/password',[
        'middleware'=>'JWT',
        'uses'=>'EmpresaController@put_password']
        
    );


    Route::post('/obtener/codigo/empresa','EmpresaController@resetPassword');

    
    //Publicacion Actividades

    Route::post('/actividad',[
        'middleware'=>'JWT',
        'uses'=>'ActividadController@post_actividad']);

    Route::get('/actividad/{id}/reservas',[
        'middleware'=>'JWT',
        'uses'=>'ActividadController@mostrarReservas'
    ]);

    Route::get('/actividades',[
        'middleware'=>'JWT',
        'uses'=>'ActividadController@get_actividades']);

    Route::get('/actividades/sin/registro','ActividadController@get_sin_registro');
    
    Route::get('/actividad/{id}',[
        'middleware'=>'JWT',
        'uses'=>'ActividadController@get_actividad']);

    Route::get('/actividades_empresa',[
        'middleware'=>'JWT',
        'uses'=>'ActividadController@get_actividades_empresa']);

    Route::put('/actividad/{id}',[
        'middleware'=>'JWT',
        'uses'=>'ActividadController@put_actividad'
        ]);

    Route::delete('/actividad/{id}',[
        'middleware'=>'JWT',
        'uses'=>'ActividadController@delete_Actividad'
    ]);

    // IMAGENES
    Route::get('/imagen/usuario', [
        'middleware'=>'JWT',
        'uses'=>'ImagenController@get_foto_usuario']);

    Route::get('/imagen/empresa', [
        'middleware'=>'JWT',
        'uses'=>'ImagenController@get_foto_empresa']);

    Route::get('/imagen/acividad', [
        'middleware'=>'JWT',
        'uses'=>'ImagenController@get_foto_actividad']);

    Route::post('/imagen', 'ImagenController@subidaFoto');


    //reservas
    Route::post('/actividad/{actividad_id}/verificar/reserva',[
        'uses'=>'ReservaController@verificarDisponibilidad'
    ]);
    Route::post('/actividad/{actividad_id}/reservar',[
        'middleware'=>'JWT',
        'uses'=>'ReservaController@reservar'
    ]);

    //comentarios y calificar
    Route::post('/actividad/{actividad_id}/comentar-calificar',[
        'middleware'=>'JWT',
        'uses'=>'ComentarioController@comentarCalificar'
    ]);
    Route::get('/actividad/{actividad_id}/comentarios',[
        'uses'=>'ComentarioController@verComentarios'
    ]);

    //calificacion
    /*Route::post('/actividad/{actividad_id}/calificar',[
        'middleware'=>'JWT',
        'uses'=>'CalificacionController@calificar'
    ]);*/

    //Buzon
    Route::get('/buzon',[
        'middleware'=>'JWT',
        'uses'=>'BuzonController@buzon'
    ]);


    //SMS
    Route::post('/enviar/sms',[
        'middleware'=>'JWT',
        'uses'=>'SmsController@enviarSms'
    ]);

Route::post('/verificar/sms',[
    'middleware'=>'JWT',
    'uses'=>'SmsController@verificarTokenCompra'
]);


//});
?>
