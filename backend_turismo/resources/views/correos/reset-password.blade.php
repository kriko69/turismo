<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
<div class="cabecera">
    <img src="http://167.172.238.126/imagenes/icon.png" width="300px" height="200px" style="margin: 0px 0px 0px 50px;">
    <hr style="border:1px solid;margin: 0px 50px 0px 50px;">
</div>
<div class="cuerpo" >
    <h1 style="margin: 0px 100px 0px 300px;">RECUPERACION DE CONTRASEÑA USUARIO</h1>
    <h1 style="margin: 0px 100px 0px 340px;">PLATAFORMA <b>TURISTENADO</b></h1>
    <br>
    <img src="http://167.172.238.126/imagenes/banner1.jpeg" alt="icono" style="margin: 0px 0px 0px 50px;" width="1200px" height="300px">
    <br><br><br>
    <h4 style="margin: 0px 0px 0px 50px;">
        Hola {{$usuario}} gracias por el proceso de recuperacion de contraseña de la plataforma Turisteando
        mas abajo encontrara un codigo que debe ingresar en la aplicacion para poder cambiar la contraseña.
        Recuerde que este codigo es personal y es su responsabilidad si da este dato a otra persona que no sea.
        Si tiene problemas puede volver a intentar el proceso de recuperacion de contraseña desde la aplicacion
        o comunicarse con el area de soporte de al siguiente correo <b>turisteandobolivia2019@gmail.com</b> muchas gracias.
    </h4>
    <br>
    <br>
    <br>
    <h1 style="margin: 0px 0px 0px 550px;">
        {{$codigo}}
    </h1>
    <br><br>
</div>
<br>
<div class="footer" style="margin: 0px 50px 0px 50px;background-color: grey;">
    <h6 style="text-align:center;padding:15px 0px 15px 0px;color: white;">
        Turisteando 2019 Copyright &copy;
    </h6>
</div>
</body>
</html>
